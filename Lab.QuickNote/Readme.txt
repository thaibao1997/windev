- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com

-Các Chức năng đã làm:
		+ Thêm một note mới với phím tắt Ctrl + Space
		+ Xem nội dung của note
		+ Xoá note
		+ Cập nhật note
		+ Tìm kiếm note theo tiêu đề và nội dung
		+ Duyệt note theo tag 
		+ Sắp xếp các tag
		+ Vẽ biểu đồ tròn các tag theo số lượng note 
		+ Thu nhỏ chương trình dười thanh taskbar sử dụng notify icon
		+ Gợi ý các tags có sẵn trong CSDL để người dùng lựa chọn 

-Các Luồng sự kiên phụ:
		+ Người dùng không nhập tag hoặc tiêu đề hoặc nội dung cho note mới hoặc khi cập nhật note:
			=>thông báo lỗi yêu cầu nhập lại
		+ Người dùng chưa nhập nội dung tìm kiếm mà nhấn nút tìm kiếm:
			=>thông báo lỗi yêu cầu nhập lại

-Link Repo:		https://bitbucket.org/thaibao1997/windev/
-Link Video Demo: 	https://youtu.be/DaTGCtjz0cM