﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace QuickNote
{
    class NoteHelper
    {
        public static string ToFullDateNumber(int number)//return number like 0x or xx
        {
            if (number <= 9)
                return "0" + number.ToString();
            return number.ToString();
        }

        public static string GetCurrentDateTime()
        {
            string currentDatetime="";
            currentDatetime += ToFullDateNumber(DateTime.Now.Hour);
            currentDatetime += ":" + ToFullDateNumber(DateTime.Now.Minute);
            currentDatetime += ":" + ToFullDateNumber(DateTime.Now.Second);
            currentDatetime += " " + ToFullDateNumber(DateTime.Now.Day);
            currentDatetime += "/" + ToFullDateNumber(DateTime.Now.Month);
            currentDatetime += "/" + ToFullDateNumber(DateTime.Now.Year);
            return currentDatetime;
        }

        public static string ToTitleCase(string str)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }
    }
}
