﻿using System;
using Fluent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;
using System.Windows.Interop;
using System.Runtime.InteropServices;

using WinForms = System.Windows.Forms;


namespace QuickNote
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        private const string ID_SHOW_DIALOG = "SHOW_NOTE_WINDOW";

        DatabaseManager database;
        public NotesManager notesManager { get; set; }
        public List<TagModelView> TagsList
        {
            get
            {
                return notesManager.viewTagsList;
            }
        }

        public LinkedList<NoteModelView> NotesList
        {
            get
            {
                return notesManager.notesList;
            }
        }

        public string searchTextboxPlaceHolder = "Search Notes...\u00A0\uFEFF";//\u00A0\uFEFF special white spaces just in case user's text same as placeholder text
        private WinForms.NotifyIcon notifyIcon;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            database = new DatabaseManager();
            notesManager = new NotesManager(database);
            ConstructNotifyIcon();
            sortCombobox.ItemsSource = notesManager.SortByList;
            searchTextbox.Opacity = 0.5;
            searchTextbox.Text = searchTextboxPlaceHolder;
        }

        private void ConstructNotifyIcon()
        {
            notifyIcon = new WinForms.NotifyIcon();
            notifyIcon.Icon = new System.Drawing.Icon("icon.ico");
            notifyIcon.Visible = true;
            this.notifyIcon.MouseDown += new WinForms.MouseEventHandler(notifier_MouseDown);
            WinForms.ContextMenuStrip contextMenu = new WinForms.ContextMenuStrip();
            contextMenu.Items.Add("View Notes", System.Drawing.Image.FromFile("images/note.png"), notifyMenu_Notes);
            contextMenu.Items.Add("Add Note", System.Drawing.Image.FromFile("images/add.png"), notifyMenu_NewNode);
            contextMenu.Items.Add("View Chart", System.Drawing.Image.FromFile("images/chart.png"), notifyMenu_Chart);
            contextMenu.Items.Add("Exit", System.Drawing.Image.FromFile("images/exit.png"), notifyMenu_Exit);
            notifyIcon.ContextMenuStrip = contextMenu;

        }

        private void notifyMenu_NewNode(object sender, EventArgs e)
        {
            NoteWindow noteWindow = new NoteWindow(notesManager);
            noteWindow.Show();
        }

        private void notifyMenu_Notes(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = WindowState.Normal;
        }

        private void notifyMenu_Chart(object sender, EventArgs e)
        {
            statisticWindow sw = new statisticWindow(TagsList);
        }

        private void notifyMenu_Exit(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void notifier_MouseDown(object sender, WinForms.MouseEventArgs e)
        {
            if (e.Button == WinForms.MouseButtons.Left)
            {
                this.Show();
                this.WindowState = WindowState.Normal;
            }
        }

        private void RibbonWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //Application.Current.Shutdown();
            e.Cancel = true;
            this.Hide();
        }

        private void tagsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            notesManager.LoadNotesOfTag();
            searchTextbox.Opacity = 0.5;
            searchTextbox.Text = searchTextboxPlaceHolder;
        }

        private void notesBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            NoteModelView note = notesBox.SelectedItem as NoteModelView;
            if (note != null)
            {
                 NoteWindow noteWindow = new NoteWindow(notesManager,note);
                 noteWindow.Show();
                 notesBox.UnselectAll();
            }

            
        }

        private void allNotes_Click(object sender, RoutedEventArgs e)
        {
            tagsBox.UnselectAll();
            notesManager.LoadAllNotes();
            notesBox.Items.Refresh();
            searchTextbox.Opacity = 0.5;
            searchTextbox.Text = searchTextboxPlaceHolder;
        }



        private void newNote_Click(object sender, RoutedEventArgs e)
        {
            NoteWindow noteWindow = new NoteWindow(notesManager);
            noteWindow.Show();
        }

        private void statistic_Click(object sender, RoutedEventArgs e)
        {
            statisticWindow sw = new statisticWindow(TagsList);
        }

        private void delteNote_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button button = sender as System.Windows.Controls.Button;
            NoteModelView noteModelView = button.Tag as NoteModelView;
            MessageBoxResult boxResult=  MessageBox.Show("Are you sure you want to delete this note?", "Confirm", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (boxResult == MessageBoxResult.Yes)
            {
                notesManager.DeleteNote(noteModelView);
            }
        }


        private void sortCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            notesManager.SortTags(sortCombobox.SelectedIndex);
        }

        private void searchTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (searchTextbox.Text == "")
            {
                searchTextbox.Text = searchTextboxPlaceHolder;
                searchTextbox.Opacity = 0.5;
            }
        }

        private void searchTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (searchTextbox.Text == this.searchTextboxPlaceHolder)
            {
                searchTextbox.Text = "";
                searchTextbox.Opacity = 1;
            }
        }

        private void searchButton_Click(object sender, RoutedEventArgs e)
        {
            if (searchTextbox.Text == searchTextboxPlaceHolder || searchTextbox.Text == "")
            {
                MessageBox.Show("Please Enter Some Text To Search", "Incorrent Input", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (notesManager.SearchNotes(searchTextbox.Text) == 0)
                    MessageBox.Show("No Result", "Search Result", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void RibbonWindow_Loaded(object sender, RoutedEventArgs e)
        {
        }



        [DllImport("HotkeyDll.dll")]
        public static extern int StartKeyBoardHook(IntPtr hwnd);
        /// <summary>
        /// FOR HOOKING USING C++ DLL
        /// </summary>
        /// <param name="e"></param>
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            StartKeyBoardHook(source.Handle);
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // Handle messages...
            
            if (msg == 0x0400 + 1)
            {
                string wPaStr = Marshal.PtrToStringAnsi(wParam);
                if (wPaStr == ID_SHOW_DIALOG)
                {
                    NoteWindow noteWindow = new NoteWindow(notesManager);
                    noteWindow.Show();
                }
            }
            return IntPtr.Zero;
        }

    }
}
