﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace QuickNote
{
     public class NoteModelView
    {
        public Note model { get; set; }

        public NoteModelView(Note model)
        {
            this.model = model;
        }

        public string content
        {
            get
            {
                if(model.content.Length > 100)
                {
                    return model.content.Substring(0, 100) + "...";
                }
                return model.content;

            }
        }

        public string tags
        {
            get
            {
                if (model.database != null)
                {
                    string sql = "Select TA.name from TagsNotes TN join Tags TA on TN.tag_id = TA.id  where note_id = " + model.id.ToString();
                    string result = "Tags: ";
                    SQLiteDataReader reader = model.database.ExecuteQuery(sql);
                    while (reader.Read())
                    {
                        result += reader.GetString(0) + ",";
                    }
                    result =result.Remove(result.Length -1);
                    return result;
                }
                return "Tags: ";
            }
        }

        public string tagsList
        {
            get
            {
                if (model.database != null)
                {
                    string sql = "Select TA.name from TagsNotes TN join Tags TA on TN.tag_id = TA.id  where note_id = " + model.id.ToString();
                    string result = "";
                    SQLiteDataReader reader = model.database.ExecuteQuery(sql);
                    while (reader.Read())
                    {
                        result += reader.GetString(0) + ",";
                    }
                    result = result.Remove(result.Length - 1);
                    return result;
                }
                return "";
            }
        }
        public NoteModelView self
        {
            get
            {
                return this;
            }
        }
    }

}
