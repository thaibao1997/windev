﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace QuickNote
{

    public class DatabaseManager
    {
        private const string databaseFileName = "database.sqlite";
        private SQLiteConnection m_dbConnection;

        public DatabaseManager()
        {
            if (!File.Exists(Environment.CurrentDirectory+"\\"+databaseFileName))
            {
                CreateNewDatabase();
            }
            else
            {
                string connectString = "Data Source= " + Environment.CurrentDirectory + "\\" + databaseFileName + ";";
                m_dbConnection = new SQLiteConnection(connectString);
                m_dbConnection.Open();

                SQLiteCommand command = new SQLiteCommand("PRAGMA foreign_keys = ON", m_dbConnection);
                command.ExecuteNonQuery();
            }
        }

        private void CreateNewDatabase()
        {
          
            SQLiteConnection.CreateFile(databaseFileName);
            string connectString = "Data Source= " + Environment.CurrentDirectory + "\\" + databaseFileName + ";";
            m_dbConnection = new SQLiteConnection(connectString);
            m_dbConnection.Open();

            SQLiteCommand command = new SQLiteCommand("PRAGMA foreign_keys = ON", m_dbConnection);
            command.ExecuteNonQuery();

            string sql = "create table Tags  (id integer primary key autoincrement,name text unique)";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql= "create table Notes  (id integer primary key autoincrement,title text,content text,datetime text )";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "create table TagsNotes (tag_id integer ,note_id integer, ";
            sql+= "FOREIGN KEY (tag_id) REFERENCES tags(id) ON DELETE CASCADE, ";
            sql += "FOREIGN KEY (note_id) REFERENCES notes(id) ON DELETE CASCADE  ";
            sql += "PRIMARY KEY (tag_id, note_id))";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
        }

        public SQLiteDataReader ExecuteQuery(string query)
        {
            SQLiteCommand command = new SQLiteCommand(query, m_dbConnection);
            SQLiteDataReader reader = command.ExecuteReader();
            return reader;
        }

        public bool ExecuteNonQuery(string query)
        {
            SQLiteCommand command = new SQLiteCommand(query, m_dbConnection);
            return command.ExecuteNonQuery() != -1;

        }

        public long LastInsertID()
        {
            return m_dbConnection.LastInsertRowId;
        }
    }
}
