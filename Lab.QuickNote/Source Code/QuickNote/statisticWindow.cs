﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuickNote
{
    public partial class statisticWindow: Form
    {
        List<TagModelView> tags;
        private bool painted = false;
        string[] ColourValues = new string[] { //source: https://stackoverflow.com/questions/309149/generate-distinctly-different-rgb-colors-in-graphs
            "FF0000", "00FF00", "0000FF", "FFFF00", "FF00FF", "00FFFF", "000000",
            "800000", "008000", "000080", "808000", "800080", "008080", "808080",
            "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0",
            "400000", "004000", "000040", "404000", "400040", "004040", "404040",
            "200000", "002000", "000020", "202000", "200020", "002020", "202020",
            "600000", "006000", "000060", "606000", "600060", "006060", "606060",
            "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0",
            "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0",
        };

        public statisticWindow(List<TagModelView> tags)
        {
            this.Show();
            InitializeComponent();

            this.tags = tags;
            detailsGirdView.Columns.Add("Tag Name", "Tag Name");
            detailsGirdView.Columns.Add("Number Of Notes", "Number Of Notes");
            detailsGirdView.Columns.Add("Color", "Color");
            detailsGirdView.ReadOnly = true;
            detailsGirdView.SelectionChanged += DataGridView1_SelectionChanged;
            detailsGirdView.DefaultCellStyle.Font = new Font("Arial", 16, GraphicsUnit.Pixel);
        }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            detailsGirdView.ClearSelection();
        }

        private int totalNumberOfNotes()
        {
            int total =0;
            foreach(var noNotes in tags)
            {
                total += noNotes.model.numberOfNotes;
            }
            return total;
        }

        private void statisticWindow_Paint(object sender, PaintEventArgs e)
        {
            
            System.Drawing.Graphics grp = this.CreateGraphics();
            grp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            Random rnd = new Random();

            int total = totalNumberOfNotes();
            List<double> precents = new List<double>();
            foreach (var tag in tags)
            {
                int noNotes = tag.model.numberOfNotes;
                double precent = (noNotes * 1.0 / total) * 360;
                precents.Add(precent);
                if (!painted)
                {
                    detailsGirdView.Rows.Add(tag.model.name, tag.numberOfNotes, "");
                }

            }
            double startAngle = 270;
            int count = 0;
            Rectangle rect = new Rectangle(10, 10, 425, 425);
            foreach (var precent in precents)
            {
                Color brusnhColor;
                if (count < ColourValues.Length)
                {
                    int r = Convert.ToInt32(ColourValues[count].Substring(0, 2), 16);
                    int g = Convert.ToInt32(ColourValues[count].Substring(2, 2), 16);
                    int b = Convert.ToInt32(ColourValues[count].Substring(4, 2), 16);
                    brusnhColor = Color.FromArgb(r, g, b);
                }
                else
                {
                    brusnhColor= Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
                }
                SolidBrush brush = new SolidBrush(brusnhColor);
                grp.FillPie(brush, rect, (float)startAngle, (float)precent);
                startAngle += precent;
                detailsGirdView.Rows[count].Cells[2].Style.BackColor = brusnhColor;
                count++;


            }
            painted = true;

        }


        private void statisticWindow_Load(object sender, EventArgs e)
        {

        }

        private void statisticWindow_Shown(object sender, EventArgs e)
        {

        }
    }
}
