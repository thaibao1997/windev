﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;


namespace QuickNote
{
    public class Tag : Model
    {

        public string name { get; set; }
        public int numberOfNotes {
            get
            {
                if(database != null)
                {
                    string sql = "Select count(*)  from TagsNotes where tag_id = " + id.ToString();
                    SQLiteDataReader reader = database.ExecuteQuery(sql);
                    reader.Read();
                    return reader.GetInt32(0);

                }
                return 0;
            }   
         }

        override public void Delete()
        {
            if(database != null)
            {
                string sql = "delete from Tags where id = " + this.id.ToString();
                database.ExecuteNonQuery(sql);
            }
        }

        override public void Save()
        {
            if (database != null && id == 0)
            {
                string sql = "insert into Tags(name) values('" + this.name + "')";
                database.ExecuteNonQuery(sql);
                this.id = (int)database.LastInsertID();
            }
        }

        static public List<Tag> All(DatabaseManager database)//get all tag
        {
            List<Tag> tags = new List<Tag>();
            if (database == null) return tags;
            string sql = "select * from Tags";
            SQLiteDataReader reader = database.ExecuteQuery(sql);
            while (reader.Read())
            {
                Tag tag = new Tag();
                tag.database = database;
                tag.id = reader.GetInt32(0);
                tag.name = reader.GetString(1);
                tags.Add(tag);
            }
            return tags;
        }
    }
}
