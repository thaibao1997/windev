﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;


namespace QuickNote
{
    public class Note : Model
    {
        public string title { get; set; }
        public string content { get; set; }
        public string dateTime { get; set; }

        override public void Delete()
        {
            if(database != null)
            {
                string sql = "delete from Notes where id = " + this.id.ToString();
                database.ExecuteNonQuery(sql);
            }
        }

        override public void Save()
        {
            if (database == null) return;
            if( id == 0)
            {
                string currentDatetime = NoteHelper.GetCurrentDateTime();
                this.dateTime = currentDatetime;
                string sql = "Insert into Notes(title,content,datetime) values ('";
                sql += this.title + "','" + this.content + "','" + currentDatetime + "')";
                database.ExecuteNonQuery(sql);
                this.id = (int)database.LastInsertID();
            }
            else if(id != 0)
            {
                string sql = "update Notes set title = '" + this.title + "', content ='" + this.content + "' ";
                sql += "where id = " + this.id.ToString();
                database.ExecuteNonQuery(sql);
            }

        }

        public void SaveTagIDs(List<int> tagIDs)
        {
            if(this.database != null && this.id != 0)
            {
                List<int> knownTagIds = this.TagIDs();
                string sql = "Delete from TagsNotes where note_id = " + id.ToString();
                database.ExecuteNonQuery(sql);
                foreach (var tagId in tagIDs)
                {
                    sql = "Insert into TagsNotes values(" + tagId.ToString() + "," + this.id.ToString() + ")";
                    database.ExecuteNonQuery(sql);
                }
            }
        }

        public List<int> TagIDs()
        {
            List<int> tagIDs = new List<int>();
            string sql = "Select tag_id from TagsNotes where note_id =" + this.id.ToString();
            if(this.database != null && this.id != 0)
            {
                SQLiteDataReader reader= database.ExecuteQuery(sql);
                while (reader.Read())
                {
                    int tag_id = reader.GetInt32(0);
                    tagIDs.Add(tag_id);
                }
            }
            return tagIDs;
        }

        static public List<Note> All(DatabaseManager database)//get
        {
            List<Note> notes = new List<Note>();
            if (database == null) return notes;

            string sql = "select * from Notes order by id DESC";
            SQLiteDataReader reader = database.ExecuteQuery(sql);
            while (reader.Read())
            {
                Note note = new Note();
                note.database = database;
                note.id = reader.GetInt32(0);
                note.title = reader.GetString(1);
                note.content = reader.GetString(2);
                note.dateTime = reader.GetString(3);
                notes.Add(note);
            }
            return notes;
        }

        static public LinkedList<Note> Search(string content,DatabaseManager database)//get
        {
            LinkedList<Note> notes = new LinkedList<Note>();
            if (database == null) return notes;
            content = content.ToUpper();
            string sql = "select * from Notes  where upper(title) like '%" + content + "%' ";
            sql+= " or upper(content) like '%" + content + "%' order by id DESC";
            SQLiteDataReader reader = database.ExecuteQuery(sql);
            while (reader.Read())
            {
                Note note = new Note();
                note.database = database;
                note.id = reader.GetInt32(0);
                note.title = reader.GetString(1);
                note.content = reader.GetString(2);
                note.dateTime = reader.GetString(3);
                notes.AddLast(note);
            }
            return notes;
        }

        static public List<Note> AllOfTag(Tag tag, DatabaseManager database)
        {
            List<Note> notes = new List<Note>();
            if (database == null) return notes;

            string sql = "select * from Notes NO join TagsNotes TN on NO.id=TN.note_id ";
            sql += "where tag_id =" + tag.id.ToString() + " order by id DESC";
            SQLiteDataReader reader = database.ExecuteQuery(sql);

            while (reader.Read())
            {
                Note note = new Note();
                note.database = database;
                note.id = reader.GetInt32(0);
                note.title = reader.GetString(1);
                note.content = reader.GetString(2);
                note.dateTime = reader.GetString(3);
                notes.Add(note);
            }

            return notes;
        }


    }
}
