﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuickNote
{
    /// <summary>
    /// Interaction logic for NoteWindow.xaml
    /// </summary>
    public partial class NoteWindow : Window
    {
        public NotesManager notesManager { get; set; }
        private string titlePlaceHoler = "Title...\u00A0\uFEFF";
        private string contentPlaceHoler = "Content...\u00A0\uFEFF";
        private bool isNewMode = true;
        private Note viewNote;
        private int StartPos=-1,EndPos=-1;

        public NoteWindow(NotesManager notesManager)
        {
            InitializeComponent();
            NewNoteConstructor(notesManager);
            this.tagsBox.TextChanged += tagsBox_TextChanged;
        }

        public NoteWindow(NotesManager notesManager,NoteModelView note)
        {
            InitializeComponent();
            ViewNoteConstructor(notesManager,note);
            titleBox.Focus();
            this.tagsBox.TextChanged += tagsBox_TextChanged;
        }

        private void NewNoteConstructor(NotesManager notesManager)
        {
            titleBox.Opacity = 0.5;
            contentBox.Opacity = 0.5;
            titleBox.Text = titlePlaceHoler;
            contentBox.Text = contentPlaceHoler;
            this.notesManager = notesManager;
            tagsBox.Text="";
        }

        private void ViewNoteConstructor(NotesManager notesManager, NoteModelView note)
        {
            //saveButton.Visibility = Visibility.Hidden;
            this.notesManager = notesManager;
            this.viewNote = note.model;

            tagsBox.Text = note.tagsList;
            titleBox.Text = note.model.title;
            contentBox.Text = note.model.content;
            datetimeLabel.Content = note.model.dateTime;
            saveButton.Content = "Update";
            
            isNewMode = false;

        }

        private List<string> GetSuggestList(string content)
        {
            List<string> res = new List<string>();
            if (content == "") return res;
            LinkedList<TagModelView> tags = notesManager.tagsList;

            foreach (var tag in tags)
            {
                if (tag.model.name.ToLower().StartsWith(content.ToLower()))
                    res.Add(tag.model.name);
            }

            return res;
        }

        private void titleBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void titleBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (titleBox.Text == titlePlaceHoler)
            {
                titleBox.Text = "";
                titleBox.Opacity = 1;
            }
            suggestBox.Height = 0;
            this.saveButton.IsDefault = true;

        }

        private void titleBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if(titleBox.Text == "")
            {
                titleBox.Text = titlePlaceHoler;
                titleBox.Opacity = 0.5;
            }
        }

        private void contentBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (contentBox.Text == contentPlaceHoler)
            {
                contentBox.Text = "";
                contentBox.Opacity = 1;
            }
            suggestBox.Height = 0;
        }

        private void contentBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (contentBox.Text == "")
            {
                contentBox.Text = contentPlaceHoler;
                contentBox.Opacity = 0.5;
            }
            this.saveButton.IsDefault = true;
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            string tagText = tagsBox.Text;
            string title = titleBox.Text;
            string content = contentBox.Text;

            string err="";

            if (tagText == "")
                err += "Tags Không được trống\n";
            if (title == "" || title==titlePlaceHoler)
                err += "Title Không được trống\n";
            if (content == "" || content== contentPlaceHoler)
                err += "Nội dung Không được trống\n";

            if (err != "")
            {
                MessageBox.Show(err, "Lỗi", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                List<string> tags = new List<string>();
                tags = tagText.Split(',').ToList();
                if (isNewMode)
                {
                    Note note = new Note();
                    note.title = title;
                    note.content = content;
                    notesManager.SaveNewNote(tags, note);
                }
                else
                {
                    this.viewNote.title = title;
                    this.viewNote.content = content;
                    notesManager.UpdateNote(tags, this.viewNote);
                }
                this.Close();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void tagsBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string content = GetSuggestContent();
            List<String> list = GetSuggestList(content);
            this.suggestBox.Height = 25 * list.Count;
            this.suggestBox.ItemsSource = list;            
        }

        private void tagsBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                if (this.suggestBox.Height > 0)
                {
                    this.suggestBox.Focus();
                   // this.suggestBox.SelectedIndex = 0;
                }
            }
        }

        private void suggestBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ChangeTextTagBox();
            }
            else if (e.Key == Key.Up)
            {
                if (this.suggestBox.SelectedIndex == 0)
                {
                    this.tagsBox.Focus();
                    this.tagsBox.CaretIndex = this.tagsBox.Text.Length;
                }
            }
        }

        private void suggestBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.saveButton.IsDefault = false;
        }

        private void tagsBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.saveButton.IsDefault = false;
        }
   

        private void ChangeTextTagBox()
        {
            if (StartPos > EndPos)
            {
                string str = tagsBox.Text;
                string replace = (string)suggestBox.SelectedItem;
                string newStr = str.Substring(0, StartPos);
                newStr = newStr + replace;
                newStr = newStr + str.Substring(EndPos);
                this.tagsBox.TextChanged -= tagsBox_TextChanged;
                this.tagsBox.Text = newStr;
                this.tagsBox.Focus();
                this.tagsBox.CaretIndex = StartPos + replace.Length;
                this.tagsBox.TextChanged += tagsBox_TextChanged;
            }
        }

        private string GetSuggestContent()
        {
            int pos = tagsBox.Text.LastIndexOf(',');
            
            string content;
            int spos,epos;
            if (tagsBox.CaretIndex <= pos)//edit intside tag
            {
                spos = tagsBox.Text.LastIndexOf(',', tagsBox.CaretIndex);
                epos = tagsBox.Text.IndexOf(',', tagsBox.CaretIndex);
                if (spos == -1 && epos == -1)
                {
                    content = tagsBox.Text.Substring(0);
                    spos = 0;
                    epos = tagsBox.Text.Length;
                }
                else if (spos == -1 && epos != -1)
                {
                    content = tagsBox.Text.Substring(0, epos);
                    spos = 0;
                }
                else if (spos != -1 && epos != -1)
                {
                    if (spos == epos)
                    {
                        spos = tagsBox.Text.LastIndexOf(',', epos-1);
                        content = tagsBox.Text.Substring(++spos, epos - spos - 1);
                    }
                    else
                        content = tagsBox.Text.Substring(++spos, epos - spos - 1);

                }
                else
                {
                    content = tagsBox.Text.Substring(pos+1);
                    spos = pos + 1;
                    epos = tagsBox.Text.Length;
                }
            }
            else
            {
                content = tagsBox.Text.Substring(pos + 1);
                spos = pos + 1;
                epos = tagsBox.Text.Length;
            }
            StartPos = spos;
            EndPos = epos;
            return content;
        }
    }
}
