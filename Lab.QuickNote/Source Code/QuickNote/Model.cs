﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace QuickNote
{
    public abstract class Model
    {
        public DatabaseManager database { get; set; }
        public int id { get; protected set; }
        public abstract void Delete();
        public abstract void Save();


    }
}
