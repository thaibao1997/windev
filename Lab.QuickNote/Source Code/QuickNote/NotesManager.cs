﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Windows;
using System.Windows.Controls;

namespace QuickNote
{
    public class NotesManager //controller
    {
        DatabaseManager database;
     
        public List<TagModelView> viewTagsList { get;}// tags have notes > 0

        public LinkedList<TagModelView> tagsList { get; }
        public LinkedList<NoteModelView> notesList { get;  }
        public List<string> SortByList = new List<string> { "Name A-Z", "Name Z-A", "Notes(Ascending)", "Notes(Descending)" };
        public int currentSortType = -1;

        public NotesManager(DatabaseManager database)
        {
            
            this.database = database;

            tagsList = new LinkedList<TagModelView>();
            viewTagsList = new List<TagModelView>();
            notesList = new LinkedList<NoteModelView>();


            if (database != null)
            {
                LoadAllTags();
                LoadAllNotes();
            }
        }

        public List<int> GetAndSaveNewTag(List<string> tags)
        {
            List<int> tagIDs = new List<int>();
            //get tag id for notes
            foreach (var tagName in tags)
            {
                bool isNewTab = true;
                string trimTagName = tagName.Trim();
                foreach (var tag in tagsList)
                {
                    if (trimTagName == "")
                    {
                        isNewTab = false;
                        break;
                    }
                    if (tag.model.name.ToLower() == trimTagName.ToLower())
                    {
                        if (!tagIDs.Contains(tag.model.id))
                            tagIDs.Add(tag.model.id);
                        if (!viewTagsList.Contains(tag))
                        {
                            viewTagsList.Add(tag);
                        }
                        isNewTab = false;
                        break;
                    }
                }
                if (isNewTab)
                {
                    Tag newTab = new Tag();
                    newTab.database = database;
                    newTab.name = NoteHelper.ToTitleCase(trimTagName);
                    newTab.Save();
                    TagModelView tagModelView = new TagModelView(newTab);
                    tagsList.AddLast(tagModelView);
                    viewTagsList.Add(tagModelView);
                    tagIDs.Add(newTab.id);
                }
            }


            return tagIDs;
        }

        public bool SaveNewNote(List<string> tags,Note note)
        {
           
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            List<int> tagIDs = GetAndSaveNewTag(tags);

            note.database = database;
            note.Save();
            note.SaveTagIDs(tagIDs);

            LoadAllNotes();
            SortTags();
            mainWindow.notesBox.Items.Refresh();
            mainWindow.tagsBox.Items.Refresh();
            mainWindow.tagsBox.UnselectAll();
            return true;
        }

        public bool UpdateNote(List<string> tags, Note note)
        {

            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            //List<int> currentTagIDs = note.TagIDs();
            List<int> tagIDs = GetAndSaveNewTag(tags);
            note.database = database;
            note.Save();
            note.SaveTagIDs(tagIDs);
            viewTagsList.RemoveAll(item => item.model.numberOfNotes <= 0);
            SortTags();
            mainWindow.notesBox.Items.Refresh();
            mainWindow.tagsBox.Items.Refresh();
            return true;
        }

        public void LoadAllTags()
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            tagsList.Clear();
            List<Tag> tags = Tag.All(database);
            foreach(var tag in tags)
            {
                TagModelView tagModelView = new TagModelView(tag);
                tagsList.AddLast(tagModelView);
                if (tag.numberOfNotes > 0)
                    viewTagsList.Add(tagModelView);
            }
            mainWindow.tagsBox.Items.Refresh();
        }

        public void LoadAllNotes()
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            notesList.Clear();
            List<Note> notes = Note.All(database);
            foreach(var note in notes)
            {
                notesList.AddLast(new NoteModelView(note));
            }
            mainWindow.notesBox.Items.Refresh();
        }

        public void LoadNotesOfTag()
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            TagModelView selectedTag = mainWindow.tagsBox.SelectedItem as TagModelView;

            if (selectedTag != null)
            {
                notesList.Clear();
                List<Note> notes = Note.AllOfTag(selectedTag.model,database);
                foreach (var note in notes)
                {
                    notesList.AddLast(new NoteModelView(note));
                }
                mainWindow.notesBox.Items.Refresh();
            }
        }



        public void DeleteNote(NoteModelView note)
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            notesList.Remove(note);
            note.model.Delete();
            viewTagsList.RemoveAll(item => item.model.numberOfNotes <= 0);
            if(notesList.Count <= 0)
            {
                mainWindow.tagsBox.UnselectAll();
                LoadAllNotes();
                mainWindow.notesBox.Items.Refresh();
                mainWindow.tagsBox.Items.Refresh();
                mainWindow.searchTextbox.Opacity = 0.5;
                mainWindow.searchTextbox.Text = mainWindow.searchTextboxPlaceHolder;

            }
            mainWindow.notesBox.Items.Refresh();
            mainWindow.tagsBox.Items.Refresh();
            SortTags();
        }

        public void SortTags(int type = -1)
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);
            if(type != -1)
                currentSortType = type;
            switch (currentSortType)
            {
                case 0://Name A-Z
                    mainWindow.tagsBox.Items.SortDescriptions.Clear();
                    mainWindow.tagsBox.Items.SortDescriptions.Add(
                     new System.ComponentModel.SortDescription("model.name", System.ComponentModel.ListSortDirection.Ascending));
                    break;
                case 1://name Z-A
                    mainWindow.tagsBox.Items.SortDescriptions.Clear();
                    mainWindow.tagsBox.Items.SortDescriptions.Add(
                     new System.ComponentModel.SortDescription("model.name", System.ComponentModel.ListSortDirection.Descending));
                    break;
                case 2://Notes(Ascending)
                    mainWindow.tagsBox.Items.SortDescriptions.Clear();
                    mainWindow.tagsBox.Items.SortDescriptions.Add(
                     new System.ComponentModel.SortDescription("model.numberOfNotes", System.ComponentModel.ListSortDirection.Ascending));
                    break;
                case 3://Notes(Descending)
                    mainWindow.tagsBox.Items.SortDescriptions.Clear();
                    mainWindow.tagsBox.Items.SortDescriptions.Add(
                     new System.ComponentModel.SortDescription("model.numberOfNotes", System.ComponentModel.ListSortDirection.Descending));
                    break;
            }
            mainWindow.tagsBox.Items.Refresh();
        }

        public int SearchNotes(string content)
        {
            MainWindow mainWindow = ((MainWindow)System.Windows.Application.Current.MainWindow);

            LinkedList<Note> notes = Note.Search(content, database);
            if (notes.Count == 0)
                return 0;
            notesList.Clear();
            foreach (var note in notes)
            {
                notesList.AddLast(new NoteModelView(note));
            }
            mainWindow.notesBox.Items.Refresh();
            mainWindow.tagsBox.UnselectAll();
            return notes.Count();
        }

    }
}
