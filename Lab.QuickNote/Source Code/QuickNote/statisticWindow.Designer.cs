﻿namespace QuickNote
{
    partial class statisticWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(statisticWindow));
            this.detailsGirdView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.detailsGirdView)).BeginInit();
            this.SuspendLayout();
            // 
            // detailsGirdView
            // 
            this.detailsGirdView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.detailsGirdView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detailsGirdView.Location = new System.Drawing.Point(464, 12);
            this.detailsGirdView.Name = "detailsGirdView";
            this.detailsGirdView.Size = new System.Drawing.Size(346, 431);
            this.detailsGirdView.TabIndex = 0;
            // 
            // statisticWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(814, 455);
            this.Controls.Add(this.detailsGirdView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "statisticWindow";
            this.Text = "Notes Chart";
            this.Load += new System.EventHandler(this.statisticWindow_Load);
            this.Shown += new System.EventHandler(this.statisticWindow_Shown);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.statisticWindow_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.detailsGirdView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView detailsGirdView;
    }
}