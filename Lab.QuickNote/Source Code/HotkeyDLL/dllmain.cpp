// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

HWND hWnd;
HHOOK hHotKey;

HINSTANCE hInstance;
BOOL gIsHook = false;

#define HOOKAPI __declspec(dllexport)

BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	hInstance = hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}



//KEYBOARD HOOK
LRESULT CALLBACK KeyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		KBDLLHOOKSTRUCT * mhs = (KBDLLHOOKSTRUCT *)lParam;
		switch (wParam)
		{
		case WM_KEYDOWN:
			//MessageBox(0, 0, 0, 0);
			if (mhs->vkCode == VK_SPACE) {
				int ctrl = GetKeyState(VK_LCONTROL);
				if (ctrl & 0x8000) { // 1: DOWN, 0: UP
					char send[]="SHOW_NOTE_WINDOW\0";
					SendMessage(hWnd, WM_USER+1, (WPARAM)send, NULL);
					return 1;
				}
			}
			break;
		default:
			break;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}



extern "C" HOOKAPI  INT StartKeyBoardHook(HWND hwndYourWindow)
{
	hWnd = hwndYourWindow;
	hHotKey = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHookProc, hInstance, 0);
	if (hHotKey != NULL)
	{
		return 1;
	}
	return 0;
}

extern "C" HOOKAPI  INT StopKeyBoardHook()
{
	if (hHotKey != NULL)
	{
		UnhookWindowsHookEx(hHotKey);
		return 1;
	}
	return 0;
}
