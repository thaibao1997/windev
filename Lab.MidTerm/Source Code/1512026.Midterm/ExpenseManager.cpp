﻿#include "stdafx.h"
#include "ExpenseManager.h"
#include<fstream>
#include <commctrl.h>
#include <locale.h>


void ExpenseManager::CreateNewDatabase()
{
	std::vector<std::wstring> categories;
	categories.push_back(L"Ăn uống");
	categories.push_back(L"Di chuyển");
	categories.push_back(L"Nhà cửa");
	categories.push_back(L"Xe cộ");
	categories.push_back(L"Nhu yếu");
	categories.push_back(L"Dịch vụ");

	if (sqlite3_open_v2(DATABASE_FILE, &database, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, 0) == SQLITE_OK) {
		sqlite3_exec(database, "PRAGMA encoding = \"UTF-16\"", 0, 0, 0);
		sqlite3_exec(database, "PRAGMA synchronous = NORMAL", 0, 0, 0);
		sqlite3_exec(database, "PRAGMA journal_mode = WAL", 0, 0, 0);

		sqlite3_stmt *statement;

		wchar_t createQuery[] = L"CREATE TABLE Categories ( ID INTEGER PRIMARY KEY AUTOINCREMENT,NAME TEXT NOT NULL)";
		if (sqlite3_prepare16_v2(database, createQuery, -1, &statement, NULL) == SQLITE_OK) {
				sqlite3_step(statement);	
				sqlite3_finalize(statement);

				wchar_t insertQuery[] = L"INSERT INTO Categories(NAME) VALUES(?)";
				for (int i = 0; i < categories.size(); i++) {
					if (sqlite3_prepare16_v2(database, insertQuery, -1, &statement, NULL) == SQLITE_OK) {
						sqlite3_bind_text16(statement, 1, categories[i].c_str(), -1, NULL);
						sqlite3_step(statement);
						sqlite3_finalize(statement);

					}
				}
		}
		//
		wchar_t createQuery2[1024] = L"CREATE TABLE Expense";
		lstrcat(createQuery2, L"(ID INTEGER PRIMARY KEY AUTOINCREMENT,");
		lstrcat(createQuery2, L"INFO TEXT,MONEY INT,DATE TEXT,CATEGORY_ID INTEGER,");
		lstrcat(createQuery2, L"FOREIGN KEY(CATEGORY_ID) REFERENCES Categories(ID))");
		if (sqlite3_prepare16_v2(database, createQuery2, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_step(statement);
			sqlite3_finalize(statement);
		}

		sqlite3_exec(database, "CREATE INDEX index_expense_date ON Expense(DATE)", 0,0,0);
	}
	//sqlite3_close(db);

}

std::vector<std::wstring> ExpenseManager::GetCategoriesName()
{
	std::vector<std::wstring> res;
	sqlite3_stmt* statement;
	wchar_t query[512]=L"SELECT NAME FROM Categories";
	if (sqlite3_prepare16_v2(this->database, query, -1,&statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			wchar_t* buffer =(wchar_t*)sqlite3_column_text16(statement, 0);
			res.push_back(buffer);
		}
	}

	return res;
}

ExpenseManager::ExpenseManager()
{


}


ExpenseManager::~ExpenseManager()
{
	sqlite3_exec(database, "PRAGMA wal_checkpoint", 0, 0, 0);

	sqlite3_close_v2(database);
}

void ExpenseManager::LoadDatabase(HWND hWnd)
{
	if (!std::ifstream(DATABASE_FILE)) {//database file not found
		//create new database
		CreateNewDatabase();
	}
	else
	{
		if (!sqlite3_open_v2(DATABASE_FILE, &this->database, SQLITE_OPEN_READWRITE, 0) == SQLITE_OK) {
			MessageBox(hWnd, L"An error happen while connect to database!!!", L"ERROR", MB_OK);
		}
	} 


}

int ExpenseManager::AddExpense(wchar_t* date_, int cate, wchar_t * info_, wchar_t * money_)
{

	wchar_t query[1024] = L"INSERT INTO Expense(INFO,MONEY,DATE,CATEGORY_ID)";
	lstrcat(query, L"VALUES(?,?,?,?)");
	sqlite3_stmt *statement;
	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_text16(statement, 1, info_, -1, NULL);
		sqlite3_bind_int(statement, 2, _wtoi(money_));
		sqlite3_bind_text16(statement, 3, date_, -1, NULL);
		sqlite3_bind_int(statement, 4, cate + 1);
		sqlite3_step(statement);
		sqlite3_finalize(statement);

		moneyCate[cate] += _wtoi(money_);
	}
	return sqlite3_last_insert_rowid(database);
}

void ExpenseManager::GetCategoriesName(int id, wchar_t* buffer)
{
	wchar_t query[1024] = L"SELECT NAME FROM Categories WHERE ID = ?";
	sqlite3_stmt *statement;

	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_int(statement, 1,id);
		sqlite3_step(statement);
		lstrcpy(buffer,(wchar_t*)sqlite3_column_text16(statement, 0));
		sqlite3_finalize(statement);
	}
}

void ExpenseManager::ReloadMoneyFromDatabaseWhenDelete(std::vector<int> ids)
{
	sqlite3_stmt *statement;

	wchar_t query[1024] = L"SELECT CATEGORY_ID,MONEY FROM Expense EX WHERE ID = ?";
	for (int i = 0; i<ids.size(); i++)
	{
		if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_int(statement, 1, ids[i]);
			sqlite3_step(statement);

			int cate_id= sqlite3_column_int(statement, 0);
			int money = sqlite3_column_int(statement, 1);

			moneyCate[cate_id - 1] -= money;
			sqlite3_finalize(statement);
		}
	}
}

void ExpenseManager::AddExpenseToListView(ListView* listview, int id)
{
	sqlite3_stmt *statement;
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");
	lstrcat(query, L" WHERE EX.ID = ?");
	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
		sqlite3_bind_int(statement, 1, id);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement, 0);
			wchar_t* dateBuff = (wchar_t*)sqlite3_column_text16(statement, 1);
			wchar_t* cateBuff = (wchar_t*)sqlite3_column_text16(statement, 2);
			wchar_t* infoBuff = (wchar_t*)sqlite3_column_text16(statement, 3);
			wchar_t* moneyBuff = (wchar_t*)sqlite3_column_text16(statement, 4);
			int cate_id = sqlite3_column_int(statement, 5);
			int iTem = listview->InsertItem(dateBuff, 0, id);
			listview->SetItemText(cateBuff, iTem, 1);
			listview->SetItemText(infoBuff, iTem, 2);
			CurrencyFormat(moneyBuff);
			listview->SetItemText(moneyBuff, iTem, 3);
		}
		sqlite3_finalize(statement);
	}	
}

void ExpenseManager::LoadComboboxCategories(HWND cbbHwnd)
{
	
	wchar_t query[] = L"SELECT NAME FROM Categories";
	int count = LoadCombobox(cbbHwnd, query);
	if (count > 0) {
		SendMessage(cbbHwnd, CB_SETCURSEL, (WPARAM)0, NULL);
	}
	moneyCate.resize(count);
	for (int i = 0; i < count; i++)
		moneyCate[i] = 0;
}

void ExpenseManager::LoadComboboxPickMonth(HWND cbbHwnd)
{
	SendMessage(cbbHwnd, CB_RESETCONTENT, NULL, NULL);
	SendMessage(cbbHwnd, CB_ADDSTRING, NULL, (LPARAM)L"Tất Cả");
	wchar_t query[512] = L"SELECT DISTINCT strftime('%m/%Y',EX.DATE)  FROM Expense EX ";
	lstrcat(query, L" ORDER BY strftime('%Y',EX.DATE) ASC ,strftime('%m',EX.DATE) ASC");
	int count = LoadCombobox(cbbHwnd, query);
	SendMessage(cbbHwnd, CB_SETCURSEL, (WPARAM)0, NULL);
}

void ExpenseManager::DeleteExpenses(std::vector<int> deleteIDs){

	sqlite3_stmt *statement;
	wchar_t query[1024] = L"DELETE FROM Expense WHERE ID = ?";
	for (int i=0;i<deleteIDs.size();i++)
	{
		if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
			sqlite3_bind_int(statement, 1, deleteIDs[i]);
			sqlite3_step(statement);
			sqlite3_finalize(statement);
		}
	}
}

void ExpenseManager::ReloadMoneyFromDatabase()
{
	sqlite3_stmt *statement;
	wchar_t query[1024] = L"SELECT EX.MONEY,EX.CATEGORY_ID FROM Expense EX ";

	sqlite3_exec(database, "BEGIN;", 0, 0, 0);
	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
		for (int i = 0; i < moneyCate.size(); i++) {
			moneyCate[i] = 0;
		}
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement, 0);
			unsigned int money = sqlite3_column_int(statement, 0);
			unsigned int cate_id = sqlite3_column_int(statement, 1);
			moneyCate[cate_id - 1] += money;
		}
		sqlite3_finalize(statement);
	}
	sqlite3_exec(database, "END;", 0, 0, 0);
}


void ExpenseManager::ReloadTotalMoney(HWND gTotal)
{
	wchar_t buffer[128];
	unsigned int totalMoney = GetTotalMoney();
	wsprintf(buffer, L"%d", totalMoney);
	CurrencyFormat(buffer);
	lstrcat(buffer, L" VNĐ");
	SetWindowText(gTotal, buffer);
}

bool ExpenseManager::DrawPieChart(Graphics * grp, Rect drawRect)
{
	unsigned int totalMoney = GetTotalMoney();
	if (totalMoney == 0) {
		grp->DrawEllipse(&Pen(Color(0, 0, 0), 1), drawRect);
		
	}

	int size = moneyCate.size();

	std::vector<REAL> angle(size,0);
	for (int i = 0; i < size; i++) {
		if(totalMoney!=0)
		angle[i] = (moneyCate[i]*1.0 / totalMoney) * 360;
	}
	std::vector<Color> colors;
	colors.push_back(Color(255, 0, 0));
	colors.push_back(Color(0, 255, 0));
	colors.push_back(Color(0, 0, 255));
	colors.push_back(Color(255, 255, 0));
	colors.push_back(Color(255, 0, 255));
	colors.push_back(Color(0, 255, 255));

	SolidBrush brush(Color(0, 0, 0));
	SolidBrush textBrush(Color(0, 0, 0));

	Rect noteRect;
	noteRect.X = drawRect.X + drawRect.Width;
	noteRect.Y = drawRect.Y;
	noteRect.Width = 60;
	noteRect.Height = 60;
	REAL starAngle = 270;
	Font textFont(L"Arial", 13);

	std::vector<std::wstring> cateName = GetCategoriesName();

	for (int i = 0; i < size; i++) {
		brush.SetColor(colors[i]);
		grp->FillPie(&brush, drawRect, starAngle, angle[i]);
		starAngle += angle[i];
		//grp->FillRectangle(&brush, noteRect);
		grp->FillPie(&brush, noteRect, 270, 90);
		wchar_t buffer[512];
		lstrcpy(buffer, cateName[i].c_str());
		int precent1, precent2;
		precent1 = angle[i] / 3.6;
		precent2 = (angle[i] / 3.6 - precent1) * 100;
		wsprintf(buffer + lstrlen(buffer), L"\n%d.%d %%", precent1, precent2);
		grp->DrawString(buffer, lstrlen(buffer),
			&textFont, PointF(noteRect.X + noteRect.Width + 10, noteRect.Y), &textBrush);
		noteRect.Y += 55;
	}
	return true;
	
	
}

void ExpenseManager::CallReDraw(HWND hwnd)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	RECT rec1;
	rec1.left = 650;
	rec1.top = 10;
	rec1.right = 1200;
	rec1.bottom = 590;
	InvalidateRect(hwnd, &rec1, TRUE);
}

unsigned int ExpenseManager::GetTotalMoney()
{
	unsigned int res = 0;
	for (int i = 0; i < moneyCate.size(); i++) {
		res += moneyCate[i];
	}
	return res;
}

void ExpenseManager::CurrencyFormat(wchar_t * buffer)
{
	int len = lstrlen(buffer);
	int apendSize = (len-1) / 3;
	wchar_t  res[128];
	res[len+ apendSize] = L'\0';
	int count = 1;
	int j = len - 1;
	for (int i = len + apendSize-1; i >= 0; i--) {
		if (j==(len-3*count - 1)) {
			res[i] = L',';
			count++;
		}
		else {
			res[i] = buffer[j];
			j--;
		}

	}
	lstrcpy(buffer, res);
	
}

void ExpenseManager::RemoveExpense(ListView * listview)
{
	std::vector<int> deleteID;
	std::vector<int> deleteIitem;

	for (int itemInt = -1; (itemInt = SendMessage(listview->getHandle(), LVM_GETNEXTITEM, itemInt, LVNI_SELECTED)) != -1; )
	{
		wchar_t buffer[512];
		LVITEM lvi = listview->GetItem(itemInt, buffer);
		deleteID.push_back(lvi.lParam);
		deleteIitem.push_back(lvi.iItem);
	}

	for (int i = 0; i < deleteIitem.size(); i++) {
		listview->DeleteItem(deleteIitem[i] - i);
	}

	if (deleteID.size() > 0) {
		ReloadMoneyFromDatabaseWhenDelete(deleteID);
		DeleteExpenses(deleteID);
	}

}


void ExpenseManager::LoadListView(ListView * listview, wchar_t * query)
{
	sqlite3_stmt *statement;
	sqlite3_exec(database, "BEGIN;", 0, 0, 0);
	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {

		listview->Clear();
		for (int i = 0; i < moneyCate.size(); i++) {
			moneyCate[i] = 0;
		}

		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement, 0);
			wchar_t* dateBuff = (wchar_t*)sqlite3_column_text16(statement, 1);
			wchar_t* cateBuff = (wchar_t*)sqlite3_column_text16(statement, 2);
			wchar_t* infoBuff = (wchar_t*)sqlite3_column_text16(statement, 3);
			wchar_t* moneyBuff = (wchar_t*)sqlite3_column_text16(statement, 4);
			int cate_id = sqlite3_column_int(statement, 5);
			int iTem = listview->InsertItem(dateBuff, 0, id);
			listview->SetItemText(cateBuff, iTem, 1);
			listview->SetItemText(infoBuff, iTem, 2);


			unsigned int money = _wtoi(moneyBuff);
			moneyCate[cate_id - 1] += money;

			CurrencyFormat(moneyBuff);
			listview->SetItemText(moneyBuff, iTem, 3);
		}
		sqlite3_finalize(statement);
	}
	std::string err = sqlite3_errmsg(database);
	sqlite3_exec(database, "END;", 0,0,0);

}

int ExpenseManager::LoadCombobox(HWND cbbHwnd, wchar_t * query)
{
	sqlite3_stmt *statement;
	int count = 0;
	if (sqlite3_prepare16_v2(database, query, -1, &statement, NULL) == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			wchar_t* buff = (wchar_t*)sqlite3_column_text16(statement, 0);
			SendMessage(cbbHwnd, CB_ADDSTRING, NULL, (LPARAM)buff);
			count++;
		}
		sqlite3_finalize(statement);
	}
	return count;
}

void ExpenseManager::LoadListViewByDate(ListView * listview, wchar_t* bymonth, bool asc)
{
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");
	
	if (bymonth != NULL && lstrcmp(bymonth, L"Tất Cả") != 0) {
		lstrcat(query, L"WHERE strftime('%m/%Y',EX.DATE) LIKE '");
		lstrcat(query, bymonth);
		lstrcat(query, L"' ");
	}

	if(asc)
		lstrcat(query, L" ORDER BY Datetime(EX.DATE) ASC,EX.ID DESC");
	else
		lstrcat(query, L" ORDER BY Datetime(EX.DATE) DESC,EX.ID DESC");

	LoadListView(listview, query);

}

void ExpenseManager::LoadListViewByCategory(ListView * listview, wchar_t* bymonth, bool asc)
{
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");


	if (bymonth != NULL && lstrcmp(bymonth, L"Tất Cả") != 0) {
		lstrcat(query, L"WHERE strftime('%m/%Y',EX.DATE) LIKE '");
		lstrcat(query, bymonth);
		lstrcat(query, L"' ");

	}


	if (asc)
		lstrcat(query, L" ORDER BY CT.NAME ASC");
	else
		lstrcat(query, L" ORDER BY CT.NAME DESC");

	LoadListView(listview, query);

}

void ExpenseManager::LoadListViewByMoney(ListView * listview, wchar_t* bymonth, bool asc)
{
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");


	if (bymonth != NULL && lstrcmp(bymonth, L"Tất Cả") != 0) {
		lstrcat(query, L"WHERE strftime('%m/%Y',EX.DATE) LIKE '");
		lstrcat(query, bymonth);
		lstrcat(query, L"' ");

	}


	if (asc)
		lstrcat(query, L" ORDER BY EX.MONEY ASC");
	else
		lstrcat(query, L" ORDER BY EX.MONEY DESC");


	LoadListView(listview, query);

}

void ExpenseManager::LoadListViewByInfo(ListView * listview, wchar_t* bymonth, bool asc)
{
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");


	if (bymonth != NULL && lstrcmp(bymonth, L"Tất Cả") != 0) {
		lstrcat(query, L"WHERE strftime('%m/%Y',EX.DATE) LIKE '");
		lstrcat(query, bymonth);
		lstrcat(query, L"' ");
	}


	if (asc)
		lstrcat(query, L" ORDER BY EX.INFO ASC");
	else
		lstrcat(query, L" ORDER BY EX.INFO DESC");


	LoadListView(listview, query);

}

void ExpenseManager::LoadListViewMonth(ListView * listview, wchar_t * dateBuffer)
{
	if (lstrcmp(dateBuffer, L"Tất Cả") == 0) {
		LoadListViewByDate(listview,false);
		return;
	}
	wchar_t query[1024] = L"SELECT EX.ID,strftime('%d/%m/%Y',EX.DATE),CT.NAME,EX.INFO,EX.MONEY,EX.CATEGORY_ID ";
	lstrcat(query, L"FROM Expense EX JOIN Categories CT ON EX.CATEGORY_ID=CT.ID ");
	lstrcat(query, L"WHERE strftime('%m/%Y',EX.DATE) LIKE '");
	lstrcat(query, dateBuffer);
	lstrcat(query, L"' ORDER BY Datetime(EX.DATE) DESC");

	LoadListView(listview, query);

}
