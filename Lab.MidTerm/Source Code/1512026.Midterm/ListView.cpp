﻿#include "stdafx.h"
#include "ListView.h"
#pragma comment(lib, "comctl32.lib")



ListView::ListView(int x, int y, int width, int height, HMENU id, HWND parentHandle, HINSTANCE hInst)
{
	this->NumberOfColumn = 0;
	//this->NumberOfItem = 0;
	this->handle= CreateWindowEx(0, WC_LISTVIEW, L"", WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LVS_REPORT, x, y, width, height, parentHandle, id, hInst, NULL);
	ListView_SetExtendedListViewStyle(this->handle, LVS_EX_FULLROWSELECT  | LVS_EX_GRIDLINES);//| LVS_EX_CHECKBOXES
//	this->InitImageLists(hInst);
}

void ListView::AddColumn(LPWSTR name, int width)
{
	LVCOLUMN lvCol;
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = width;
	lvCol.pszText = name;
	int a= ListView_InsertColumn(this->getHandle(), this->NumberOfColumn, &lvCol);
	this->NumberOfColumn++;
}

int ListView::InsertItem(LPWSTR value, int image, LPARAM lparam )
{
	LVITEM litem;
	litem.mask = LVIF_TEXT|LVIF_IMAGE |LVIF_PARAM;
	litem.iItem = 0;
	litem.iSubItem = 0;
	litem.pszText = value;
	litem.iImage = image;
	litem.lParam = lparam;
	int a= ListView_InsertItem(this->getHandle(), &litem);
	//this->NumberOfItem++;
	return 0 ;
}

void ListView::SetItemText(LPWSTR value, int nItem, int column) {
	ListView_SetItemText(this->getHandle(),nItem, column, value);
}

int ListView::GetNumberOfItem()
{
	//return this->NumberOfItem;
	return 0;
}

LVITEM ListView::GetSelectedItem(TCHAR * buffer)
{
	LVITEM item;
	item.mask = LVIF_TEXT | LVIF_PARAM;
	item.pszText = buffer;
	item.cchTextMax = 128;

	int ipos = -1;
	ipos = ListView_GetNextItem(this->getHandle(), -1, LVNI_FOCUSED | LVNI_SELECTED);
	if (ipos != -1) {
		item.iItem = ipos;
		if (ListView_GetItem(this->getHandle(), &item))
			return item;
	}
	return LVITEM();
}
void ListView::DeleteCheckedItem()
{
	for (int itemInt = -1; (itemInt = ListView_GetNextItem(handle, itemInt, LVNI_SELECTED)) != -1; ) {
		if (ListView_GetCheckState(handle, itemInt)) {
			ListView_DeleteItem(handle, itemInt);
		}
	}
}
HWND ListView::getHandle()
{
	return this->handle;
}


ListView::~ListView()
{
}

void ListView::Clear()
{

	ListView_DeleteAllItems(this->handle);
	//this->NumberOfItem=0;
}

void ListView::DeleteItem(int item)
{
	ListView_DeleteItem(handle, item);
	//this->NumberOfItem--;
}


LVITEM ListView::GetItem(int index, TCHAR * buffer)
{
	LVITEM item;
	item.mask = LVIF_TEXT | LVIF_PARAM;
	item.pszText = buffer;
	item.cchTextMax = 128;
	item.iItem = index;
	if (ListView_GetItem(this->getHandle(), &item))
		return item;
	return LVITEM();
}