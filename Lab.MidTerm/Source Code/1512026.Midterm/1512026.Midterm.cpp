// 1512026.Midterm.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512026.Midterm.h"
#include"ListView.h"
#include<windowsx.h>
#include <commctrl.h>
#include"ExpenseManager.h"
/*#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")*/
using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#define MAX_LOADSTRING 100
#define MAX_BUFFER_SIZE 512

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND MainHWND;

ULONG_PTR gdiplusToken;
GdiplusStartupInput gdiplusStartupInput;
ExpenseManager gExpenseManger;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void OnSize(HWND hWnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);
void GetWindowText_(HWND hwnd, wchar_t* buffer);
bool NumberValidation(wchar_t* buffer);
int CurrencyFormat(wchar_t* buffer);
void CurrencyBufferToIntBuffer(wchar_t* buffer);
int CurrencyBufferToInt(wchar_t* buffer);
void DatetimeToBuffer(_SYSTEMTIME date, wchar_t* bufferDB, wchar_t* bufferCBB);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1512026MIDTERM, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512026MIDTERM));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg) && !IsDialogMessage(MainHWND, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY1512026MIDTERM);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   MainHWND = CreateWindowW(szWindowClass, szTitle, WS_SYSMENU| WS_OVERLAPPED | WS_MINIMIZEBOX,
      10, 10, 1150, 650, nullptr, nullptr, hInstance, nullptr);

   if (!MainHWND)
   {
      return FALSE;
   }

   ShowWindow(MainHWND, nCmdShow);
   UpdateWindow(MainHWND);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
	case WM_CTLCOLORSTATIC:
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnSize(HWND hWnd, UINT state, int cx, int cy) {

}

HFONT ghfont;
HWND gAddButton;
HWND gDeleteButton;
HWND gCbbCategory;
HWND gCbbMonth;
HWND gEditInfo;
HWND gEditMoney;
HWND gEditTotalMoney;
HWND gDatePicker;

ListView* gListView;

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {

	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	ghfont = CreateFont(19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");
	HWND hwind;



	hwind = CreateWindowEx(NULL, L"Button", L"Thêm Chi Tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,10, 20, 825, 100, hWnd, 0, hInst, 0);
	SetWindowFont(hwind, ghfont, TRUE);

	hwind = CreateWindowEx(NULL, L"static", L"Ngày:", WS_CHILD | WS_VISIBLE , 25, 50, 100, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwind, ghfont, TRUE);
	gDatePicker = CreateWindowEx(0, DATETIMEPICK_CLASS, L"", WS_CHILD | WS_VISIBLE | WS_TABSTOP | DTS_SHORTDATECENTURYFORMAT, 25, 75, 125, 27, hWnd, NULL, hInst, NULL);
	SetWindowFont(gDatePicker, ghfont, TRUE);
	wchar_t format[512]=L"dd/MM/yyyy";
	SendMessage(gDatePicker, DTM_SETFORMAT, NULL,(LPARAM)format);

	hwind = CreateWindowEx(NULL, L"static", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE, 155, 50, 100, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwind, ghfont, TRUE);
	gCbbCategory = CreateWindowEx(NULL, L"combobox", L"", WS_CHILD | WS_VISIBLE | WS_TABSTOP | CBS_DROPDOWNLIST, 155, 75, 110, 25, hWnd, NULL, hInst, NULL);
	SetWindowFont(gCbbCategory, ghfont, TRUE);

	

	hwind = CreateWindowEx(NULL, L"static", L"Nội dung", WS_CHILD | WS_VISIBLE, 270, 50, 100, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwind, ghfont, TRUE);
	gEditInfo = CreateWindowEx(NULL, L"edit", L"", WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_BORDER | ES_AUTOHSCROLL , 270, 75, 300, 27, hWnd, NULL, hInst, NULL);
	SetWindowFont(gEditInfo, ghfont, TRUE);
	SetFocus(gEditInfo);

	hwind = CreateWindowEx(NULL, L"static", L"Số tiền (VNĐ)", WS_CHILD | WS_VISIBLE, 575, 50, 150, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwind, ghfont, TRUE);
	gEditMoney = CreateWindowEx(NULL, L"edit", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | WS_TABSTOP | ES_NUMBER   , 575, 75, 120, 27, hWnd, (HMENU)ID_MONEY_EDIT, hInst, NULL);
	SetWindowFont(gEditMoney, ghfont, TRUE);

	


	gAddButton= CreateWindowEx(NULL, L"button", L"Thêm", WS_CHILD | WS_VISIBLE | WS_TABSTOP | BS_CENTER, 700, 75, 100, 27, hWnd, (HMENU)ID_ADD_BUTTON, hInst, NULL);
	SetWindowFont(gAddButton, ghfont, TRUE);
	//SendMessage(hWnd, DM_SETDEFID, (WPARAM)ID_ADD_BUTTON, NULL);

	gListView = new ListView(25, 190, 620, 360,(HMENU)ID_LISTVIEW, hWnd, hInst);
	SetWindowFont(gListView->getHandle(), ghfont, TRUE);
	gListView->AddColumn(L"Ngày", 120);
	gListView->AddColumn(L"Loại", 120);
	gListView->AddColumn(L"Nội Dung", 200);
	gListView->AddColumn(L"Số tiền", 200);


	gDeleteButton = CreateWindowEx(NULL, L"button", L"Xoá chi tiêu", WS_CHILD | WS_VISIBLE | WS_BORDER , 545, 160, 100, 27, hWnd, (HMENU)ID_DELETE_BUTTON, hInst, NULL);
	SetWindowFont(gDeleteButton, ghfont, TRUE);
//	HBITMAP hbitmap = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP1));
//	SendMessage(gDeleteButton, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hbitmap);

	gCbbMonth = CreateWindowEx(NULL, L"combobox", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 25, 160, 100, 25, hWnd, (HMENU)ID_CBB_MONTH, hInst, NULL);
	SetWindowFont(gCbbMonth, ghfont, TRUE);


	ghfont = CreateFont(21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");
	hwind = CreateWindowEx(NULL, L"static", L"Tổng cộng:", WS_CHILD | WS_VISIBLE , 373, 560, 180, 25, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwind, ghfont, TRUE);



	gEditTotalMoney = CreateWindowEx(NULL, L"edit", L"", WS_CHILD | WS_VISIBLE | ES_READONLY,465, 560, 150, 25, hWnd, NULL, hInst, NULL);
	SetWindowFont(gEditTotalMoney, ghfont, TRUE);


	ghfont = CreateFont(19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");
	hwind = CreateWindowEx(NULL, L"Button", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX, 10, 130, 1110, 455, hWnd, 0, hInst, 0);
	SetWindowFont(hwind, ghfont, TRUE);


	gExpenseManger.LoadDatabase(hWnd);
	gExpenseManger.LoadComboboxCategories(gCbbCategory);
	gExpenseManger.LoadComboboxPickMonth(gCbbMonth);
	gExpenseManger.LoadListViewByDate(gListView,NULL,true);
	gExpenseManger.ReloadTotalMoney(gEditTotalMoney);



	return TRUE;
}
// gMoneyBuffer[MAX_BUFFER_SIZE] = L"";
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	wchar_t datebufferDB[MAX_BUFFER_SIZE] = L"";
	wchar_t datebufferCBB[MAX_BUFFER_SIZE] = L"";
	wchar_t moneybuffer[MAX_BUFFER_SIZE] = L"";
	wchar_t infoBuffer[MAX_BUFFER_SIZE] = L"";
	wchar_t errBuffer[MAX_BUFFER_SIZE]=L"";
	wchar_t selBuffer[MAX_BUFFER_SIZE] = L"";
	BOOL err=FALSE;
	int category;
	
	std::vector<int> deleteID;
	RECT rect;
	POINT p;
	int pos;
	_SYSTEMTIME date;
	int index;
	switch (id)
	{
	case ID_ADD_BUTTON:		
		GetWindowText_(gEditInfo, infoBuffer);
		if (lstrcmp(infoBuffer, L"") == 0) {
			wsprintf(errBuffer, L"Nội dung không được rỗng\n");
			err = true;
		}

		GetWindowText_(gEditMoney, moneybuffer);
		CurrencyBufferToIntBuffer(moneybuffer);
		if (lstrcmp(moneybuffer, L"") == 0) {
			wsprintf(errBuffer + lstrlen(errBuffer), L"Số tiền phải là 1 số nguyên dương\n");
			err = true;
		}
		else if (_wtoi(moneybuffer) % 100 ) {
			wsprintf(errBuffer + lstrlen(errBuffer), L"Số tiền không phù hợp\n");
			err = true;
		}
		else if(_wtoi(moneybuffer) <= 0) {
			wsprintf(errBuffer + lstrlen(errBuffer), L"Số tiền không được là 0\n");
			err = true;
		}
		if (err) {
			SetWindowText(gEditMoney, L"");
			MessageBox(hWnd, errBuffer, L"Lỗi", MB_OK);
		}
		else {
			DateTime_GetSystemtime(gDatePicker, &date);
			DatetimeToBuffer(date, datebufferDB, datebufferCBB);

			category = SendMessage(gCbbCategory, CB_GETCURSEL, NULL, NULL);
			ComboBox_GetText(gCbbMonth, selBuffer, MAX_BUFFER_SIZE);

			int id= gExpenseManger.AddExpense(datebufferDB, category, infoBuffer, moneybuffer);
			gExpenseManger.LoadComboboxPickMonth(gCbbMonth);

				if (lstrcmp(selBuffer, L"Tất Cả") == 0) {
					gExpenseManger.AddExpenseToListView(gListView, id);
				}
				else {
					index = ComboBox_SelectString(gCbbMonth, -1, datebufferCBB);
					if (index != CB_ERR) {
						if (lstrcmp(datebufferCBB, selBuffer) == 0) {
							gExpenseManger.AddExpenseToListView(gListView, id);
						}
						else
							gExpenseManger.LoadListViewMonth(gListView, datebufferCBB);
					}
					else {
						gExpenseManger.LoadListViewByDate(gListView, false);
						index = 0;
					}
				}
			ComboBox_SetCurSel(gCbbMonth, index);
			gExpenseManger.ReloadTotalMoney(gEditTotalMoney);
			gExpenseManger.CallReDraw(hWnd);

			SetWindowText(gEditMoney, L"");
			SetWindowText(gEditInfo, L"");
		
		}
		break;
	case ID_MONEY_EDIT:
		if (codeNotify == EN_CHANGE ) {
			GetWindowText_(gEditMoney, moneybuffer);
			pos= CurrencyFormat(moneybuffer);
			SetWindowText(gEditMoney, moneybuffer);
			SendMessage(gEditMoney, EM_SETSEL, pos, pos);
		}
		break;
	case ID_DELETE_BUTTON:
			gExpenseManger.RemoveExpense(gListView);
			ComboBox_GetText(gCbbMonth, selBuffer, MAX_BUFFER_SIZE);
			gExpenseManger.LoadComboboxPickMonth(gCbbMonth);
			index = ComboBox_SelectString(gCbbMonth, -1, selBuffer);
			if (index == CB_ERR) {
					gExpenseManger.LoadListViewByDate(gListView, false);
					index = 0;
			}
			ComboBox_SetCurSel(gCbbMonth, index);
			gExpenseManger.ReloadTotalMoney(gEditTotalMoney);
			gExpenseManger.CallReDraw(hWnd);
		break;
	case ID_CBB_MONTH:
		if (codeNotify == CBN_SELCHANGE) {
			GetWindowText_(gCbbMonth, datebufferDB);
			gExpenseManger.LoadListViewMonth(gListView, datebufferDB);
			gExpenseManger.ReloadTotalMoney(gEditTotalMoney);
			gExpenseManger.CallReDraw(hWnd);
		}
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	RECT winRect, drawRect;
	HDC          hdcMem;
	HBITMAP      hbmMem;
	HANDLE       hOld;
	int width, height;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	GetClientRect(hWnd, &winRect);
	width = winRect.right;
	height = winRect.bottom;

	HRGN  drawRegion = CreateRectRgn(650,10,1200,590);
	SelectClipRgn(hdc, drawRegion);
	GetRgnBox(drawRegion, &drawRect);

	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, width, height);
	hOld = SelectObject(hdcMem, hbmMem);

	FillRect(hdcMem, &winRect, (HBRUSH)(COLOR_WINDOW + 1));//tô trắng vùng
	Graphics* grp = new Graphics(hdcMem);
	grp->SetSmoothingMode(SmoothingModeHighQuality);

	Rect EllipseRect;
	EllipseRect.X = 660;
	EllipseRect.Y = 210;
	EllipseRect.Width = EllipseRect.Height = 300;


	gExpenseManger.DrawPieChart(grp,EllipseRect);


	BitBlt(hdc, 0, 0, width, height, hdcMem, 0, 0, SRCCOPY);
	delete grp;
	DeleteObject(drawRegion);
	SelectObject(hdcMem, hOld);
	DeleteObject(hbmMem);

	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd){
	GdiplusShutdown(gdiplusToken);
	PostQuitMessage(0);
}

BOOL gSortState[4];

LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm) {
	NMLISTVIEW* nmlist = (NMLISTVIEW*)pnm;
	LPNMHDR hdr = (LPNMHDR)pnm;
	int nCurSelIndex;

	switch (pnm->code)
	{
	case LVN_ITEMCHANGED:
		if (idFrom == ID_LISTVIEW) {
			if (nmlist->uNewState & LVIS_STATEIMAGEMASK){
				//checkbox has been changed => select/unselect the row
				BOOL checked = ListView_GetCheckState(gListView->getHandle(), nmlist->iItem);
				ListView_SetItemState(gListView->getHandle(), nmlist->iItem,
					checked ? LVIS_SELECTED : 0, LVIS_SELECTED);
			}

		}

		break;

	case LVN_COLUMNCLICK:
		if (idFrom == ID_LISTVIEW) {
			int selectedCol = nmlist->iSubItem;
			gSortState[selectedCol]= !gSortState[selectedCol];
			wchar_t datebufferCBB[MAX_BUFFER_SIZE] = L"";
			ComboBox_GetText(gCbbMonth, datebufferCBB, MAX_BUFFER_SIZE);

			if (selectedCol == 0) {
				gExpenseManger.LoadListViewByDate(gListView, datebufferCBB,gSortState[selectedCol]);
			}
			else if (selectedCol == 1) {
				gExpenseManger.LoadListViewByCategory(gListView, datebufferCBB,gSortState[selectedCol]);
			}else if (selectedCol == 2) {
				gExpenseManger.LoadListViewByInfo(gListView, datebufferCBB,gSortState[selectedCol]);
			}
			else if (selectedCol == 3) {
				gExpenseManger.LoadListViewByMoney(gListView, datebufferCBB,gSortState[selectedCol]);
			}

		}
		break;
	case DTN_DATETIMECHANGE:

		break;
	
	default:
		break;
	}
	return 0;

}

void GetWindowText_(HWND hwnd, wchar_t * buffer)
{
	int size= GetWindowTextLength(hwnd);
	GetWindowText(hwnd, buffer, size+1);
}

bool NumberValidation(wchar_t* buffer)
{
	int len = lstrlen(buffer);
	for (int i = 0; i < len; i++)
		if (buffer[i] > '9' || buffer[i] < '0')
			return FALSE;
	return TRUE;
}

int CurrencyFormat(wchar_t * buffer)
{

	CurrencyBufferToIntBuffer(buffer);
	int len = lstrlen(buffer);
	int apendSize = (len - 1) / 3;
	wchar_t  res[128];
	res[len + apendSize] = L'\0';
	int count = 1;
	int j = len - 1;
	for (int i = len + apendSize - 1; i >= 0; i--) {
		if ((buffer[j] >= L'0' && buffer[j] <= '9')) {
			if (j == (len - 3 * count - 1)) {
				res[i] = L',';
				count++;
				
			}
			else {
				res[i] = buffer[j];
				j--;
			}
		}

	}
	lstrcpy(buffer, res);
	return lstrlen(buffer);
}

void CurrencyBufferToIntBuffer(wchar_t * buffer)
{
	int len = lstrlen(buffer);
	wchar_t interger[128];
	int j = 0;
	for (int i = 0; i < len; i++) {
		if (buffer[i] < L'0' || buffer[i]>'9') continue;
		interger[j] = buffer[i];
		j++;
	}
	interger[j] = '\0';
	lstrcpy(buffer, interger);
}

int CurrencyBufferToInt(wchar_t * buffer)
{
	CurrencyBufferToIntBuffer(buffer);
	return _wtoi(buffer);
}

void DatetimeToBuffer(_SYSTEMTIME date, wchar_t * bufferDB, wchar_t * bufferCBB)
{
	wsprintf(bufferDB, L"%d-", date.wYear);
	if (date.wMonth < 10)
		wsprintf(bufferDB + lstrlen(bufferDB), L"0%d-", date.wMonth);
	else
		wsprintf(bufferDB + lstrlen(bufferDB), L"%d-", date.wMonth);

	if (date.wDay<10)
		wsprintf(bufferDB + lstrlen(bufferDB), L"0%d", date.wDay);
	else
		wsprintf(bufferDB + lstrlen(bufferDB), L"%d", date.wDay);

	if (date.wMonth < 10)
		wsprintf(bufferCBB + lstrlen(bufferCBB), L"0%d/", date.wMonth);
	else
		wsprintf(bufferCBB + lstrlen(bufferCBB), L"%d/", date.wMonth);
	wsprintf(bufferCBB + lstrlen(bufferCBB), L"%d", date.wYear);
}
