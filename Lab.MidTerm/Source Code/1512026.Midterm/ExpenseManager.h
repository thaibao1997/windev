﻿#pragma once
#include<vector>
#include<string>
#include<windowsx.h>
#include"sqlite\\sqlite3.h"
#include"ListView.h"
#include <ObjIdl.h>
#include <gdiplus.h>

#pragma comment(lib, "gdiplus.lib")

using namespace Gdiplus;

#if defined(_WIN64)
#pragma comment(lib, "sqlite\\x64\\sqlite3.lib")
#else
#pragma comment(lib, "sqlite\\x86\\sqlite3.lib")
#endif


#define DATABASE_FILE "app.database"



class ExpenseManager
{
private:
	sqlite3 *database;
	void CreateNewDatabase();

	std::vector<unsigned int> moneyCate;//số tiền của từng loại chi tiêu
	
	std::vector<std::wstring> GetCategoriesName();
	void LoadListView(ListView* listview, wchar_t* query);
	int LoadCombobox(HWND cbbHwnd, wchar_t* query);
	void DeleteExpenses(std::vector<int> deleteIDs);
	void ReloadMoneyFromDatabase();
	void GetCategoriesName(int id, wchar_t* buffer);
	void ReloadMoneyFromDatabaseWhenDelete(std::vector<int> ids);
public:
	ExpenseManager();
	~ExpenseManager();

	void LoadDatabase(HWND hWnd);


	void LoadComboboxCategories(HWND cbbHwnd);
	void LoadComboboxPickMonth(HWND cbbHwnd);

	void LoadListViewByDate(ListView* listview, wchar_t* bymonth = NULL,bool asc=true);//asc tăng dần
	void LoadListViewByCategory(ListView* listview, wchar_t* bymonth = NULL, bool asc = true);//asc tăng dần
	void LoadListViewByMoney(ListView* listview, wchar_t* bymonth = NULL, bool asc = true);//asc tăng dần
	void LoadListViewByInfo(ListView* listview, wchar_t* bymonth = NULL, bool asc = true);//asc tăng dần
	void LoadListViewMonth(ListView* listview, wchar_t* dateBuffer);//asc tăng dần


	void ReloadTotalMoney(HWND gTotal);

	bool DrawPieChart(Graphics* grp,Rect drawRect);
	void CallReDraw(HWND hwnd);
	unsigned int GetTotalMoney();

	void CurrencyFormat(wchar_t* buffer);

	void RemoveExpense(ListView* listview);
	int AddExpense(wchar_t* date_, int cate, wchar_t* info_, wchar_t * money_);//thêm 1 chi tiêu
	void AddExpenseToListView(ListView* listview,int id);
};

