//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512026.Midterm.rc
//
#define IDC_MYICON                      2
#define IDD_MY1512026MIDTERM_DIALOG     102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_MY1512026MIDTERM            109
#define ID_ADD_BUTTON                   110
#define ID_LISTVIEW                     111
#define ID_MONEY_EDIT                   112
#define ID_DELETE_BUTTON                113
#define ID_STATISTIC_BUTTON             114
#define ID_CBB_MONTH                    115
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     129
#define IDI_ICON1                       131
#define IDB_BITMAP1                     133
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           116
#endif
#endif
