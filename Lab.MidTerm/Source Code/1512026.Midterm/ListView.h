﻿#pragma once
#include <Commctrl.h>
#include <string>
#include "1512026.Midterm.h"

class ListView
{
private:
	HWND handle;
	int NumberOfColumn=0; //lưu số lượng colummn
public:
	ListView(int x,int y,int width,int height,HMENU id,HWND parentHandle,HINSTANCE hInst);
	void AddColumn(LPWSTR name,int width);
	int InsertItem(LPWSTR value,int image=0,LPARAM lparam=NULL);
	void SetItemText(LPWSTR value,int nItem, int column);
	int GetNumberOfItem();
	LVITEM GetSelectedItem(TCHAR* buffer);//buffer để lưu text
	LVITEM GetItem(int index, TCHAR * buffer);

	void DeleteCheckedItem();
	void Clear();

	void DeleteItem(int item);


	HWND getHandle();
	~ListView();
};

