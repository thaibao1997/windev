// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"

#define ID_HOOK_OK 110
#define ID_STOPHOOK_OK 111

HWND hWnd;
HHOOK hWinHookMouse;
HHOOK hWinHookKeyB;

HINSTANCE hInstance;
BOOL gIsHook = false;

#define HOOKAPI __declspec(dllexport)

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
	hInstance = hModule;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}



INT StartMouseHook();
INT StopMouseHook();

//KEYBOARD HOOK
LRESULT CALLBACK KeyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		KBDLLHOOKSTRUCT * mhs = (KBDLLHOOKSTRUCT *)lParam;
		switch (wParam)
		{
		case WM_KEYDOWN:
			//MessageBox(0, 0, 0, 0);
			if (mhs->vkCode == VK_SPACE) {
				int ctrl = GetKeyState(VK_LCONTROL);
				if (ctrl & 0x8000) { // 1: DOWN, 0: UP
					gIsHook = !gIsHook;
					if (gIsHook) {
						if (StartMouseHook())
							SendMessage(hWnd, WM_COMMAND, ID_HOOK_OK, NULL);
					}
					else {
						if (StopMouseHook())
							SendMessage(hWnd, WM_COMMAND, ID_STOPHOOK_OK, NULL);
					}
				}
			}
			break;
		default:
			break;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}



extern "C" HOOKAPI  INT StartKeyBoardHook(HWND hwndYourWindow)
{
	hWnd = hwndYourWindow;
	hWinHookKeyB = SetWindowsHookEx(WH_KEYBOARD_LL, KeyboardHookProc, hInstance, 0);
	if (hWinHookKeyB != NULL)
	{
		return 1;
	}
	return 0;
}

extern "C" HOOKAPI  INT StopKeyBoardHook()
{
	if (hWinHookMouse != NULL)
	{
		UnhookWindowsHookEx(hWinHookKeyB);
		return 1;
	}
	return 0;
}

//MOUSE HOOK

LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode == HC_ACTION)
	{
		POINT p;
		GetCursorPos(&p);
		MOUSEHOOKSTRUCT* mhs = (MOUSEHOOKSTRUCT*)lParam;
		switch (wParam)
		{
		case  WM_LBUTTONUP:
		case WM_LBUTTONDBLCLK:
		case WM_LBUTTONDOWN:
			PostMessage(mhs->hwnd, WM_RBUTTONDOWN, NULL, MAKELPARAM(p.x, p.y));
			PostMessage(mhs->hwnd, WM_RBUTTONUP, NULL, MAKELPARAM(p.x, p.y));
			return 1;
		case  WM_RBUTTONUP:
		case WM_RBUTTONDBLCLK:
		case WM_RBUTTONDOWN:
			//PostMessage(mhs->hwnd, WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(p.x, p.y));
			//PostMessage(mhs->hwnd, WM_LBUTTONUP, NULL, MAKELPARAM(p.x, p.y));
		{
			INPUT input;
			input.type = INPUT_MOUSE;
			input.mi.dx = 0;
			input.mi.dy = 0;
			input.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE | MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP);
			input.mi.mouseData = 0;
			input.mi.dwExtraInfo = NULL;
			input.mi.time = 0;
			SendInput(1, &input, sizeof(INPUT));
		}
			return 1;
		default:
			break;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}

INT StartMouseHook()
{
	hWinHookMouse = SetWindowsHookEx(WH_MOUSE, MouseHookProc, hInstance, 0);
	if (hWinHookMouse != NULL)
	{
		return 1;
	}
	return 0;
}

INT StopMouseHook()
{
	if (hWinHookMouse != NULL)
	{
		UnhookWindowsHookEx(hWinHookMouse);
		return 1;
	}
	return 0;
}