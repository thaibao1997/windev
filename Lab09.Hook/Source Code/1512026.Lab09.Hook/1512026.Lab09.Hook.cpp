// 1512026.Lab09.Hook.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512026.Lab09.Hook.h"
#include <windowsx.h>
#include<shellapi.h>
#define MAX_LOADSTRING 100

#pragma (lib,"Shell32.lib");
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define APPWM_ICONNOTIFY (WM_APP + 1)


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);


BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
void OnSize(HWND hwnd, UINT state, int cx, int cy);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MY1512026LAB09HOOK, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512026LAB09HOOK));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1512026LAB09HOOK));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MY1512026LAB09HOOK);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX,
      CW_USEDEFAULT, 0, 340, 140, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);

	case APPWM_ICONNOTIFY:
	{
		switch (lParam)
		{
		case WM_LBUTTONUP:
			ShowWindow(hWnd, SW_RESTORE);
			break;
		case WM_RBUTTONUP:
		{
			POINT curPoint;
			GetCursorPos(&curPoint);
			HMENU hMenuPopup = CreatePopupMenu();
			//InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_STOPHOOK_OK, L"");
			InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, IDM_EXIT, L"Exit");

			//ClientToScreen(hWnd, &curPoint);
			TrackPopupMenu(hMenuPopup, TPM_TOPALIGN | TPM_LEFTALIGN, curPoint.x, curPoint.y, 0, hWnd, NULL);
		}
			break;
		}

		return 0;
	}
	case WM_SYSCOMMAND:
		switch (wParam & 0xfff0)
		{
		case SC_CLOSE:
			ShowWindow(hWnd, SW_HIDE);
			break;
		default:
			DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_ERASEBKGND:
			break;


	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }

    return (INT_PTR)FALSE;
}

HINSTANCE ghDLL;
HWND gLabel;
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {
	ghDLL = LoadLibrary(L"HookDLL.dll");
	bool err=false;
	typedef  int(*proc1) (HWND);
	if (ghDLL != NULL)
	{
		proc1 installHookproc = (proc1)GetProcAddress(ghDLL, "StartKeyBoardHook");
		if (installHookproc) {
			int rs = installHookproc(hWnd);
			if (rs != 1) {
				err = true;
			}
		}
		else {
			err = true;
		}
	}
	else {
		err = true;
	}
	if (err) {
		MessageBox(hWnd, L"Failed To Load DLL. Exit Appication...", L"Error", MB_OK);
		PostMessage(hWnd, WM_DESTROY, NULL, NULL);
	}
	HFONT hfont = CreateFont(19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");
	gLabel = CreateWindowEx(NULL, L"static", L"Press left control + space to start hooking.", WS_CHILD | WS_VISIBLE| SS_CENTER, 10, 10, 300, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(gLabel, hfont, TRUE);

	NOTIFYICONDATA niData;
	ZeroMemory(&niData, sizeof(NOTIFYICONDATA));
	niData.cbSize = sizeof(NOTIFYICONDATA);
	niData.uID = 112;
	niData.uFlags = NIF_ICON | NIF_MESSAGE ;
	niData.hIcon =
		(HICON)LoadImage(hInst,
			MAKEINTRESOURCE(IDI_MY1512026LAB09HOOK),
			IMAGE_ICON,
			GetSystemMetrics(SM_CXSMICON),
			GetSystemMetrics(SM_CYSMICON),
			LR_DEFAULTCOLOR);
	niData.hWnd = hWnd;
	niData.uCallbackMessage = APPWM_ICONNOTIFY;
	Shell_NotifyIcon(NIM_ADD, &niData);


	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	case ID_HOOK_OK:
		SetWindowText(gLabel, L"Press left control + space to stop hooking.");
		break;
	case ID_STOPHOOK_OK:
		SetWindowText(gLabel, L"Press left control + space to start hooking.");
		break;
	}

}

void OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetBkMode(hdc, TRANSPARENT);

	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd) {
	FreeLibrary(ghDLL);
	PostQuitMessage(0);
}

void OnSize(HWND hwnd, UINT state, int cx, int cy)
{
	
}
