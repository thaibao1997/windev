﻿- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com
-Các Chức năng đã làm:
	-Luồng sự kiện chính
		+ Khởi động chương trình
		+ Nhấn phím tắt LControl + space để bắt đầu vô hiệu hoá chuột phải
		+ Nhấn phím tắt  LControl + space để kích hoạt lại chuột phải
		+ Khi nhấn nút close chương trình sẽ thu nhỏ dưới taskbar
		+ Muốn tắt chương trình click chuột phải vào icon dưới taskbar chọn exit
-Link Repo: https://bitbucket.org/thaibao1997/windev/
-Link Video Demo: https://youtu.be/F_-mNOExs4A
