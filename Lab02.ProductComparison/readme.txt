- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com
- What you have done?
	+ Viết chương trình so sánh giá giữa Nhật và Singapore xuất ra 
	  màn hình nên mua ipad ở đâu dưới dạng 1 label (static text) 
	  màu đỏ.
	+ Xử lý các lỗi trong quả trình nhập giá tiền.
- Main flow:
	+ Nguời dùng nhập đúng input giá tiền(số) và nhấn nút so sánh sẽ
	  xuất ra màn hình nên mua ipad ở đâu dưới dạng 1 label(static text) 
	  màu đỏ.
- Additional flow:
	+ Người dùng không nhập gì khi nhấn nút so sánh sẽ xuất hiện 1
	  message box thông báo "Giá tiền ở ... phải là một số"
	+ Người dùng nhập vào không phải số khi nhấn nút so sánh sẽ xuất 
	  hiện 1 message box thông báo "Giá tiền ở ... phải là một số"
	  vào xoá input người dùng vừa nhập vào edit box đó.
	+ Người dùng nhập vào 1 số âm khi nhấn nút so sánh sẽ xuất 
	  hiện 1 message box thông báo "Giá tiền ở ... phải là một số Dương"
	  vào xoá input người dùng vừa nhập vào edit box đó.
	+ Khi người dùng đã nhập đúng và đã xuất ra kết quả ở lần nhập trước
	  tuy nhiên lần nhập sau người dùng nhập sai sẽ xuất thông báo lỗi
	  và xoá text ở label kết quả của lần nhập trước


