#include "stdafx.h"
#include "Validation.h"
#include <iostream>
#include <string>
#include <regex>
using namespace std;

Validation::Validation()
{
}


Validation::~Validation()
{
}

bool Validation::isNumber(const wstring& str)
{
	string str1 = string(str.begin(), str.end());
	return regex_match(str1,regex("(\\+|-)?([[:digit:]])+(\\.{1}[[:digit:]]+)?"));
}
