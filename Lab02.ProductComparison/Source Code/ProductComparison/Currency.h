#pragma once

enum CurrencyType
{
	SGD, //Singapore dollar
	VND, // VN dong
	JPY,// Yen
};

#define SGD_TO_VND 16884
#define JPY_TO_VND 204

class Currency
{
private:
	double value;
	CurrencyType type;
	double toVND() const;
public:
	Currency();
	Currency(double val,CurrencyType ct);
	~Currency();
	int compare(const Currency& c2) const;
};

