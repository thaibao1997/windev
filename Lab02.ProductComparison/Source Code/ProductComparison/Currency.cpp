#include "stdafx.h"
#include "Currency.h"
#include <iostream>
using namespace std;


Currency::Currency()
{
}

Currency::Currency(double val, CurrencyType ct)
{
	
	this->value = val;
	this->type = ct;
}




Currency::~Currency()
{
}



double Currency::toVND() const
{
	switch (this->type)
	{
	case JPY:
		return this->value*JPY_TO_VND;
	case SGD:
		return this->value*SGD_TO_VND;
	default:
		break;
	}
}


/*
	reuturn 0 if equal 1 if this > c2 -1 if this < c2
*/
int Currency::compare(const Currency & c2) const {
	if (this->toVND() > c2.toVND())
		return 1;
	if (this->toVND() < c2.toVND())
		return -1;
	return 0;
}
