﻿// ProductComparison.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "ProductComparison.h"
#include "Currency.h"
#include "windowsX.h";
#include "windows.h";
#include<string>
#include"Validation.h";

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100



// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PRODUCTCOMPARISON, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PRODUCTCOMPARISON));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW +1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PRODUCTCOMPARISON);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON2));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"Mua Ở Đâu", WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX,
      380,200, 400, 320, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}






//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HDC hdc;
	HDC hdcStatic;
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_CTLCOLORSTATIC:

		if (GetDlgCtrlID((HWND)lParam) == ID_LABELKQ)
		{
			hdcStatic = (HDC)wParam; 
			SetBkMode(hdcStatic, TRANSPARENT);
			SetTextColor(hdcStatic, RGB(255, 0, 0)); 
			return (BOOL)GetSysColorBrush(COLOR_MENU);
		}
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }


    return 0;
}





// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}



HWND Input1;
HWND Input2;
HWND labelKQ;

#define CreateEditBox(text, left, top, width, height) CreateWindowEx(0, L"EDIT",text, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, left, top, width, height, hWnd, NULL, hInst, NULL);
#define createLabel(text, left, top, width, height)  CreateWindowEx(0, L"STATIC",text, WS_CHILD | WS_VISIBLE | SS_LEFT,left, top, width, height, hWnd, NULL, hInst, NULL);

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {
	HWND hwnd;

	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(21, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);



	hwnd = createLabel(L"Nhập Giá ở Nhật:", 10, 60, 200, 50);
	SetWindowFont(hwnd, hFont, TRUE);
	hwnd = createLabel(L"Nhập Giá ở Singapore:", 10, 120, 200, 50);
	SetWindowFont(hwnd, hFont, TRUE);

	Input1 = CreateEditBox(L"", 10, 85, 250, 25);
	SetWindowFont(Input1, hFont, TRUE);
	Input2 = CreateEditBox(L"", 10, 145, 250, 25);
	SetWindowFont(Input2, hFont, TRUE);
	
	hwnd = CreateWindowEx(0, L"BUTTON", L"So Sánh", WS_CHILD | WS_VISIBLE, 280, 75, 80, 95, hWnd, (HMENU)ID_BUTTON_KQ, hInst, NULL);
	SetWindowFont(hwnd, hFont, TRUE);

	hwnd = createLabel(L"Kết Quả:", 10, 185, 200, 50);
	SetWindowFont(hwnd, hFont, TRUE);


	hFont = CreateFont(30, 0,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	labelKQ = CreateWindowEx(0, L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_CENTER | WS_BORDER, 10, 210,350, 40, hWnd, (HMENU)ID_LABELKQ, hInst, NULL);
	SetWindowFont(labelKQ, hFont, TRUE);
	hwnd = CreateWindowEx(0, L"STATIC", L"Mua Ipad Ở Đâu", WS_CHILD | WS_VISIBLE | SS_CENTER , 60, 10, 250, 50, hWnd, NULL, hInst, NULL);
	SetWindowFont(hwnd, hFont, TRUE);

	return TRUE;
}




void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify){
	// Parse the menu selections:
	WCHAR* buffer1 = NULL;
	WCHAR* buffer2 = NULL;

	WCHAR* bufferKQ = NULL; //buffer lưu kết quả
	WCHAR* bufferERR = NULL;//buffer lưu các lỗi

	int size1, size2;
	double giaNhat, giaSingapore;

	Currency* cGiaNhat;
	Currency* cGaSingapore;

	int compareResult;
	bool error = false;

	switch (id)
	{
	case ID_BUTTON_KQ:

		
	
		size1 = GetWindowTextLength(Input1);
		size2 = GetWindowTextLength(Input2);

		buffer1 = new WCHAR[size1 + 1];
		buffer2 = new WCHAR[size2 + 1];

		GetWindowText(Input1, buffer1, size1 + 1);
		GetWindowText(Input2, buffer2, size2 + 1);


		bufferERR = new WCHAR[255];
		wsprintf(bufferERR, L"");

		if (!Validation::isNumber(wstring(buffer1))) {
			wsprintf(bufferERR + lstrlen(bufferERR), L"Giá Tiền Ở Nhật Phải Là Một Số\n");
			SetWindowText(Input1, L"");
			error = true;
		}
		else{
			giaNhat = _wtof(buffer1);
			if (giaNhat < 0) {
				wsprintf(bufferERR + lstrlen(bufferERR), L"Giá Tiền Ở Nhật Phải Là Một Số Dương \n");
				SetWindowText(Input1, L"");
				error = true;
			}
		}

		if (!Validation::isNumber(wstring(buffer2))) {
			wsprintf(bufferERR  +lstrlen(bufferERR), L"Giá Tiền Ở Singapore Phải Là Một Số \n");
			SetWindowText(Input2, L"");
			error = true;
		}
		else {
			giaSingapore = _wtof(buffer2);
			if (giaSingapore < 0) {
				wsprintf(bufferERR + lstrlen(bufferERR), L"Giá Tiền Ở Singapore Phải Là Một Số Dương \n");
				SetWindowText(Input2, L"");
				error = true;
			}
		}
	
		if (error) { //kiểm tra lỗi
			SetWindowText(labelKQ, L"");
			MessageBox(hWnd, bufferERR, L"Lỗi Input", MB_OK);
			break;
		}

		cGiaNhat = new Currency(giaNhat, CurrencyType::JPY);
		cGaSingapore = new Currency(giaSingapore, CurrencyType::SGD);

		bufferERR = new WCHAR[255];
		bufferKQ = new WCHAR[255];

		compareResult = cGiaNhat->compare(*cGaSingapore);
		if (compareResult > 0)
			wsprintf(bufferKQ, L"Bạn Nên Mua Ở Singapore");
		else if (compareResult < 0)
			wsprintf(bufferKQ, L"Bạn Nên Mua Ở Nhật");
		else
			wsprintf(bufferKQ, L"Bạn Mua đâu cũng được");


		SetWindowText(labelKQ, bufferKQ);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}

	if (!buffer1)
		delete[] buffer1;
	if (!buffer2)
		delete[] buffer2;
	if (!bufferKQ)
		delete[] bufferKQ;
	if (!bufferERR)
		delete[] bufferERR;

}

void OnPaint(HWND hWnd){
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SetBkMode(hdc, TRANSPARENT);

	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd) {
	PostQuitMessage(0);
}