﻿#pragma once
#include <Commctrl.h>
#include <string>
#include "1512026Lab04Explorer.h"

class TreeView
{
private:
	HWND handle;

public:
	TreeView(int x, int y, int width, int height, HMENU id, HWND parentHandle, HINSTANCE hInst);
	HTREEITEM InsertRootNode(LPWSTR value, int image = 0,int selectedImage=0 ,LPARAM lparam=NULL);
	HTREEITEM InsertNode(LPWSTR value,HTREEITEM parent ,int image = 0, int selectedImage = 0, LPARAM lparam = NULL,HTREEITEM InsetPos= TVI_LAST);
	HTREEITEM InsertDummy(HTREEITEM parent);//chèn 1 node dummy để có expand button
	void DeleteDummy(HTREEITEM parent);//delete node dummy khi load node thật
	void DeleteAllChild(HTREEITEM parent);//delete tất cả node con của parent

	TVITEM  GetSelectedItem(TCHAR* buffer);//buffer lưu text của TVITEM
	HTREEITEM GetSelectedItem();
	HWND getHandle();
	~TreeView();
};

