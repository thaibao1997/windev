﻿// 1512026Lab04Explorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512026Lab04Explorer.h"
#include "windowsx.h"
#include <commctrl.h>
#include "ListView.h"
#include "TreeView.h"
#include <string>
#include"Drive.h"
#include <shlwapi.h>
#include "ToolBar.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND editlDir; //thanh địa chỉ
HWND buttonGo; //nút go
ToolBar* g_ToolBar;
ListView* g_ListView; 
TreeView* g_TreeView;
HFONT hfont;


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);




int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512026LAB04EXPLORER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512026LAB04EXPLORER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON4));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512026LAB04EXPLORER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON4));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX,
      100, 50, 900, 600, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}




//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}



void OnSize(HWND hWnd, UINT state, int cx, int cy) {


}



BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {
	hfont = CreateFont(18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");

	g_TreeView = new TreeView(10, 40, 200, 490, (HMENU)ID_TREEVIEW, hWnd, hInst);

	g_ListView = new ListView(215, 40, 660, 490, (HMENU)ID_LISTVIEW, hWnd, hInst);

	g_ToolBar = new ToolBar(hWnd, hInst,hfont);
	g_ToolBar->SetEnableBackButton();
	g_ToolBar->SetEnableForwardButton();

	Drive::StartUpInital(g_ListView, g_TreeView,hInst);

	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	wchar_t buffer[512];
	wchar_t buffer2[512];

	switch (id)
	{
	case ID_GO_BUTTON:
		g_ToolBar->GetAddressBarText(buffer);
		if (!StrCmp(buffer, L"This PC")) {
			Drive::ViewDriveList(g_ListView);
		}
		else if (!Drive::TryOpenFile(buffer)) {
			if (!Drive::ViewFileList(g_ListView, buffer)) {
				MessageBox(hWnd, L"Đường Dẫn Không Tồn Tại", L"Lỗi", MB_OK | MB_ICONERROR);
				g_ToolBar->RestoreAddress();
			}else if (StrCmp(g_ToolBar->GetCurrentAddress().c_str(), buffer) != 0) {
					g_ToolBar->StoreCurrentAddress(FALSE);
					g_ToolBar->StoreNewAddress();
			}
		}
		else {
			g_ToolBar->RestoreAddress();
		}
		break;
	case ID_BACK_BUTTON:
		g_ToolBar->StroreForwardAddress();
		g_ToolBar->GetBackAddress(buffer);
		if (StrCmp(buffer,L"This PC")==0) {
			Drive::ViewDriveList(g_ListView);
		}
		else {
			Drive::ViewFileList(g_ListView, buffer);
		}
		g_ToolBar->SetAddressBarText(buffer);
		g_ToolBar->StoreNewAddress(FALSE);
		break;
	case ID_FORWARD_BUTTON:
		g_ToolBar->StoreCurrentAddress();
		g_ToolBar->GetForwardAddress(buffer);
		if (StrCmp(buffer, L"This PC") == 0) {
			Drive::ViewDriveList(g_ListView);
		}
		else {
			Drive::ViewFileList(g_ListView, buffer);
		}
		g_ToolBar->SetAddressBarText(buffer);
		g_ToolBar->StoreNewAddress(FALSE);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;

	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
//	g_ToolBar->SetEnableBackButton();
}

void OnPaint(HWND hWnd) {

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd) {
	if (g_ListView != NULL)
		delete g_ListView;
	if (g_TreeView != NULL)
		delete g_TreeView;
	if(g_ToolBar!=NULL)
		delete g_ToolBar;
	Drive::FreeMemory();
	PostQuitMessage(0);

}

LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm) {
	LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;
	int nCurSelIndex;
	LPNMTOOLBAR lpnmToolBar = (LPNMTOOLBAR)pnm;


	HTREEITEM  hti;
	TCHAR buffer[128];
	HTREEITEM hSelectedItem;
	TVITEM item;


	switch (pnm->code)
	{
	case TVN_ITEMEXPANDING:
		item = lpnmTree->itemNew;
		Drive::LoadExpanding(g_TreeView, item);
		break;
	case TVN_SELCHANGED:
		item = g_TreeView->GetSelectedItem(buffer);
		g_ToolBar->StoreCurrentAddress();
		if (item.lParam == (LPARAM)L"THIS_PC_ROOT") {
			Drive::ViewDriveList(g_ListView);
			g_ToolBar->SetAddressBarText(L"This PC");
		}
		else {
			Drive::ViewFileList(g_ListView, (LPWSTR)item.lParam);
			g_ToolBar->SetAddressBarText((LPWSTR)item.lParam);
		}
		g_ToolBar->StoreNewAddress();
		break;
	case NM_DBLCLK:
		if (idFrom == ID_LISTVIEW) {
			LVITEM item = g_ListView->GetSelectedItem(buffer);
			if (!Drive::TryOpenFile((LPWSTR)item.lParam)) {
				g_ToolBar->StoreCurrentAddress();
				Drive::ViewFileList(g_ListView, (LPWSTR)item.lParam);
				g_ToolBar->SetAddressBarText((LPWSTR)item.lParam);
				g_ToolBar->StoreNewAddress();

			}
		}


	default:
		break;
	}


	return 0;

}