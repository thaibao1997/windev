//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512026Lab04Explorer.rc
//
#define IDC_MYICON                      2
#define IDD_1512026LAB04EXPLORER_DIALOG 102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512026LAB04EXPLORER        107
#define IDI_SMALL                       108
#define IDC_1512026LAB04EXPLORER        109
#define ID_LISTVIEW                     110
#define ID_TREEVIEW                     111
#define ID_GO_BUTTON                    112
#define ID_BACK_BUTTON                  113
#define ID_FORWARD_BUTTON               114
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       132
#define IDI_ICON2                       133
#define IDI_ICON3                       134
#define IDI_ICON4                       138
#define IDB_BITMAP1                     143
#define IDB_BITMAP2                     144
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           115
#endif
#endif
