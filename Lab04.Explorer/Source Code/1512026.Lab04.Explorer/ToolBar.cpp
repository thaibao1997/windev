#include "stdafx.h"
#include "ToolBar.h"
#include "windowsx.h"
#include <shlwapi.h>


ToolBar::ToolBar(HWND parentHandle, HINSTANCE hInst, HFONT hfont)
{
	this->addressHandle = CreateWindowEx(0, WC_EDIT, L"This PC", WS_VISIBLE | WS_CHILD |ES_AUTOHSCROLL |ES_LEFT | WS_BORDER , 80, 10, 760, 25, parentHandle, NULL, hInst, NULL);
	this->goButtonHandle = CreateWindowEx(0, WC_BUTTON, L">", WS_VISIBLE | WS_CHILD, 840, 10, 35, 25, parentHandle, (HMENU)ID_GO_BUTTON, hInst, NULL);
	this->backButtonHandle= CreateWindowEx(0, WC_BUTTON, L"<-", WS_VISIBLE | WS_CHILD  | BS_BITMAP, 10, 10, 35, 25, parentHandle, (HMENU)ID_BACK_BUTTON, hInst, NULL);
	this->forwardButtonHandle= CreateWindowEx(0, WC_BUTTON, L"->", WS_VISIBLE | WS_CHILD | BS_BITMAP, 44, 10, 35, 25, parentHandle, (HMENU)ID_FORWARD_BUTTON, hInst, NULL);
	SetWindowFont(this->addressHandle, hfont, TRUE);

	//HBITMAP hbmpItem;
	//hbmpItem = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP1)); //back
	//SetBitmapDimensionEx(hbmpItem, 6, 6, NULL);
	//SendMessage(this->backButtonHandle, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hbmpItem);
	//hbmpItem = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_BITMAP2)); //back
	//SendMessage(this->forwardButtonHandle, BM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hbmpItem);

	
	this->historyAddress.clear();
	this->forwardAddress.clear();
	this->currentAddress = L"This PC";
}

HWND ToolBar::GetAddressBarHandle()
{
	return this->addressHandle;
}

void ToolBar::GetAddressBarText(wchar_t * buffer)
{
	GetWindowText(this->GetAddressBarHandle(), buffer, 512);

}

void ToolBar::SetAddressBarText(wchar_t * buffer)
{
	SetWindowText(this->GetAddressBarHandle(), buffer);

}

void ToolBar::StoreCurrentAddress(BOOL onAddressBar)
{
	if (onAddressBar) {
		wchar_t* buffer = new wchar_t[512];
		this->GetAddressBarText(buffer);
		this->historyAddress.push_back(std::wstring(buffer));
	}
	else {
		this->historyAddress.push_back(this->currentAddress);
	}
	this->SetEnableBackButton();

}

void ToolBar::StoreNewAddress(BOOL clearForward)
{
	wchar_t buffer[512];
	this->GetAddressBarText(buffer);
	this->currentAddress = std::wstring(buffer);

	if(clearForward)
		this->forwardAddress.clear();
	this->SetEnableForwardButton();
}

void ToolBar::StroreForwardAddress()
{
	wchar_t buffer[512];
	this->GetAddressBarText(buffer);
	this->forwardAddress.push_back(std::wstring(buffer));
	this->SetEnableForwardButton();

}

void ToolBar::GetBackAddress(wchar_t* buffer)
{
	if (this->historyAddress.empty()) return;
	std::wstring tmp = this->historyAddress.back();
	this->historyAddress.pop_back();
	StrCpy(buffer, tmp.c_str());
	this->SetEnableBackButton();
	//delete tmp;
}

void ToolBar::GetForwardAddress(wchar_t * buffer)
{
	if (this->forwardAddress.empty()) return;
	std::wstring tmp = this->forwardAddress.back();
	this->forwardAddress.pop_back();
	StrCpy(buffer, tmp.c_str());
	this->SetEnableForwardButton();
}

void ToolBar::RestoreAddress()
{
	wchar_t buffer[512];
	StrCpy(buffer, this->currentAddress.c_str());
	this->SetAddressBarText(buffer);
}

std::wstring ToolBar::GetCurrentAddress()
{
	return this->currentAddress;
}

void ToolBar::SetEnableBackButton()
{
	if (this->historyAddress.empty())
		EnableWindow(this->backButtonHandle, FALSE);
	else
		EnableWindow(this->backButtonHandle, TRUE);

}

void ToolBar::SetEnableForwardButton()
{
	if (this->forwardAddress.empty())
		EnableWindow(this->forwardButtonHandle, FALSE);
	else
		EnableWindow(this->forwardButtonHandle, TRUE);
}


ToolBar::~ToolBar()
{
}
