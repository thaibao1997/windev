﻿#include "stdafx.h"
#include "Drive.h"
#//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#include<iostream>
#include<Shellapi.h>
#include <string>     // std::wstring, std::to_wstring
#include <algorithm>

#pragma comment(lib, "shlwapi.lib")



#define MAX_BUFFER_SIZE 1024
#define DRIVE_ICON_INDEX 0
#define FOLDER_ICON_INDEX 1
#define FILE_ICON_INDEX 2
#define PC_ICON_INDEX 3



wchar_t* Drive::drivesBuffer = new wchar_t[MAX_BUFFER_SIZE];
std::vector<wchar_t*> Drive::bufferAddress = std::vector<wchar_t*>(0);


std::vector<wchar_t*> Drive::GetDrives()
{
	wchar_t* buffer = drivesBuffer;
	GetLogicalDriveStrings(255, buffer);
	std::vector<wchar_t*> vec;

	while (buffer && lstrlen(buffer) > 1) {
		vec.push_back(buffer);
		buffer += lstrlen(buffer) + 1;
	}

	return vec;
}

void Drive::StartUpInital(ListView * lw, TreeView * tw, HINSTANCE hInst)
{
	HTREEITEM root = tw->InsertRootNode(L"This PC", PC_ICON_INDEX, PC_ICON_INDEX, (LPARAM)L"THIS_PC_ROOT");
	lw->Clear();

	lw->AddColumn(L"Tên", 200);  //tạo sẵn các column
	for (int i = 0; i < 3; i++)
		lw->AddColumn(L"",100);

	Drive::ShowDriveColumn(lw);


	std::vector<wchar_t*> drives = GetDrives();
	for (int i = 0; i < drives.size(); i++) {
		//lw->InsertItem(drives[i], 0, (LPARAM)drives[i]);
		HTREEITEM justInsert = tw->InsertNode(drives[i], root, 0, 0, (LPARAM)drives[i]);
		if(HasSubDirectory(drives[i]))
			tw->InsertDummy(justInsert);
	}
	TreeView_Expand(tw->getHandle(), root, TVE_EXPAND);

	ViewDriveList(lw);

	HICON hiconItem;     // Icon for view items.
	HIMAGELIST hImgList;   // Image list for icon view.
	hImgList = ImageList_Create(16, 16, ILC_COLOR32 | ILC_MASK, 3, 3);
	hiconItem = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1)); //disk
	ImageList_AddIcon(hImgList, hiconItem);
	hiconItem = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON3)); //folder
	ImageList_AddIcon(hImgList, hiconItem);
	hiconItem = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON2)); //file
	ImageList_AddIcon(hImgList, hiconItem);
	hiconItem = LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON4)); //PC
	ImageList_AddIcon(hImgList, hiconItem);
	DestroyIcon(hiconItem);
	TreeView_SetImageList(tw->getHandle(), hImgList, TVSIL_NORMAL);
	ListView_SetImageList(lw->getHandle(), hImgList, LVSIL_SMALL);

}

void Drive::ViewDriveList(ListView * lw)
{

	std::vector<wchar_t*> drives=GetDrives();
	lw->Clear();
	Drive::ShowDriveColumn(lw);
	for (int i = 0; i < drives.size(); i++) {
		int iItem=lw->InsertItem(drives[i], 0, (LPARAM)drives[i]);
		wchar_t infoBuffer[30];


		_GetDiskLabel(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 1);
		_GetDiskTotalSize(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 2);
		_GetDiskFreeSpace(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 3);
	}
}


BOOL Drive::ViewFileList(ListView * lw, wchar_t * path)
{

	wchar_t Path[MAX_BUFFER_SIZE];
	StrCpy(Path, path);
	StrCat(Path,L"\\*");
	
	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(Path, &fd);



	if (hFile == INVALID_HANDLE_VALUE)
		return FALSE;
	lw->Clear();
	Drive::ShowFileColumn(lw);
	std::vector<WIN32_FIND_DATA> File;
	std::vector<WIN32_FIND_DATA> Folder;

	BOOL bFound = 1;
	TCHAR * folderPath;
	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
				Folder.push_back(fd);
			else
				File.push_back(fd);
		}
		bFound = FindNextFileW(hFile, &fd);
	}//while
	for (int i = 0; i < Folder.size(); i++) {
		wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];
		StrCpy(buffer, path);
		StrCat(buffer, Folder[i].cFileName);
		StrCat(buffer, L"\\");
	
		int  iItem = lw->InsertItem(Folder[i].cFileName, FOLDER_ICON_INDEX, (LPARAM)buffer);
		
		bufferAddress.push_back(buffer);

		lw->SetItemText(L"Thư Mục", lw->GetNumberOfItem() - 1, 2);
		wchar_t infoBuffer[30];
		GetLastWriteTime(infoBuffer, Folder[i]);
		lw->SetItemText(infoBuffer, iItem, 3);
	}

	for (int i = 0; i < File.size(); i++) {
		wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];
		StrCpy(buffer, path);
		StrCat(buffer, File[i].cFileName);
		int  iItem=lw->InsertItem(File[i].cFileName, FILE_ICON_INDEX, (LPARAM)buffer);

		bufferAddress.push_back(buffer);
		
		wchar_t infoBuffer[30];
		GetFileSize(infoBuffer, File[i]);
		lw->SetItemText(infoBuffer, iItem, 1);
		GetFileExtension(infoBuffer, File[i]);
		lw->SetItemText(infoBuffer, iItem, 2);
		GetLastWriteTime(infoBuffer, File[i]);
		lw->SetItemText(infoBuffer, iItem, 3);

		

	}
	return TRUE;
	
	
}

BOOL Drive::TryOpenFile(wchar_t * path)
{
	if (!IsExistPath(path) || IsFolderPath(path)) return FALSE;
	SHELLEXECUTEINFO ShExecInfo = { 0 };
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
	ShExecInfo.lpFile = path;
	ShExecInfo.lpParameters = _T("");
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_SHOW;
	ShExecInfo.hInstApp = NULL;
	ShellExecuteEx(&ShExecInfo);
	//WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
	return TRUE;
}

void Drive::LoadExpanding(TreeView* tw, TVITEM parent)
{
	if (parent.hItem == TreeView_GetRoot(tw->getHandle()))
		return;

	tw->DeleteAllChild(parent.hItem);


	wchar_t path[MAX_BUFFER_SIZE];

	StrCpy(path, (PWSTR)parent.lParam);
	StrCat(path, L"*");

	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(path, &fd);

	BOOL bFound = 1;

	if (hFile == INVALID_HANDLE_VALUE)
		bFound = FALSE;

	TCHAR * folderPath;
	while (bFound)
	{
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(fd.cFileName, _T(".")) != 0) && (StrCmp(fd.cFileName, _T("..")) != 0))
		{
			wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];
			StrCpy(buffer, (PWSTR)parent.lParam);
			StrCat(buffer, fd.cFileName);
			StrCat(buffer, L"\\");
			HTREEITEM hti = tw->InsertNode(fd.cFileName, parent.hItem, FOLDER_ICON_INDEX, FOLDER_ICON_INDEX, (LPARAM)buffer, TVI_SORT);
			if (HasSubDirectory(buffer))
				tw->InsertDummy(hti);
			bufferAddress.push_back(buffer);
		}

		bFound = FindNextFileW(hFile, &fd);
	}//while


}

BOOL Drive::isEmptyDir(wchar_t * path)
{
	wchar_t buffer_tmp[MAX_BUFFER_SIZE];
	WIN32_FIND_DATA found;
	HANDLE hFind;
	StrCpy(buffer_tmp, path);
	StrCat(buffer_tmp, L"*");
	hFind = FindFirstFile(buffer_tmp, &found);
	if (hFind == INVALID_HANDLE_VALUE)
		return TRUE;
	BOOL bFound = 1;
	while (bFound) {
		if (((found.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((found.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(found.cFileName, _T(".")) != 0) && (StrCmp(found.cFileName, _T("..")) != 0))
			return FALSE;
		bFound = FindNextFileW(hFind, &found);
	}
	return TRUE;
}

BOOL Drive::HasSubDirectory(wchar_t * path)
{
	wchar_t buffer_tmp[MAX_BUFFER_SIZE];
	WIN32_FIND_DATA found;
	HANDLE hFind;
	StrCpy(buffer_tmp, path);
	StrCat(buffer_tmp, L"*");
	hFind = FindFirstFile(buffer_tmp, &found);
	if (hFind == INVALID_HANDLE_VALUE)
		return FALSE;
	BOOL bFound = 1;
	while (bFound) {
		if (((found.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
			&& ((found.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN)
			&& ((found.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM)
			&& (StrCmp(found.cFileName, _T(".")) != 0) && (StrCmp(found.cFileName, _T("..")) != 0))
				return TRUE;
		bFound = FindNextFileW(hFind, &found);
	}
	return FALSE;
}

BOOL Drive::IsExistPath(wchar_t * path)
{
	WIN32_FIND_DATA fd;
	HANDLE hFile = FindFirstFileW(path, &fd);

	return !(hFile == INVALID_HANDLE_VALUE);
}

BOOL Drive::IsFolderPath(wchar_t * path)
{
	DWORD attr = GetFileAttributes(path);
	return ((attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY);
}

void Drive::ShowFileColumn(ListView* lw)
{
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_WIDTH;
	lvCol.cx = 180;
	ListView_SetColumn(lw->getHandle(), 0, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_FMT;
	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.pszText = L"Kích thước";
	ListView_SetColumn(lw->getHandle(), 1, &lvCol);


	lvCol.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 130;
	lvCol.pszText = L"Loại tập tin";
	ListView_SetColumn(lw->getHandle(), 2, &lvCol);

	lvCol.pszText = L"Ngày chỉnh sửa";
	ListView_SetColumn(lw->getHandle(), 3, &lvCol);
}

UINT64 Drive::GetFileSize(const WIN32_FIND_DATA & file)
{
	_LARGE_INTEGER   size;
	size.HighPart = file.nFileSizeHigh;
	size.LowPart = file.nFileSizeLow;
	return size.QuadPart;
}


void Drive::GetFileSize(wchar_t * buffer, const WIN32_FIND_DATA & file)
{
	UINT64 size = GetFileSize(file);
	SizeConverter(buffer, size);


}

void Drive::GetLastWriteTime(wchar_t * buffer, const WIN32_FIND_DATA & file)
{
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&file.ftLastWriteTime, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	wsprintf(buffer, L"%02d/%02d/%04d %02d:%02d %s",
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour > 12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour > 12) ? (L"PM") : (L"AM"));
}

void Drive::GetFileExtension(wchar_t * buffer, const WIN32_FIND_DATA& file)
{
	std::wstring filename(file.cFileName);
	int pos = filename.rfind(L"."); //lấy phần extension
	filename = filename.substr(pos + 1, filename.size() - pos);
	std::transform(filename.begin(), filename.end(), filename.begin(), ::toupper); //chuyển extension thành chữ hoa

	StrCpy(buffer, L"File ");
	//StrCat(buffer, filename.c_str());
	if (!StrCmp(filename.c_str(), L"TXT")) {
		StrCat(buffer, L"Text ");
	}
	else if (!StrCmp(filename.c_str(), L"EXE") || !StrCmp(filename.c_str(), L"MSI")) {
		StrCat(buffer, L"Thực Thi ");
	}
	else if (!StrCmp(filename.c_str(), L"RAR") || !StrCmp(filename.c_str(), L"ZIP") || !StrCmp(filename.c_str(), L"7Z")) {
		StrCat(buffer, L"Nén ");
	}
	else if (!StrCmp(filename.c_str(), L"MP4") || !StrCmp(filename.c_str(), L"FLV") || !StrCmp(filename.c_str(), L"AVI")) {
		StrCat(buffer, L"Video ");
	}
	else if (!StrCmp(filename.c_str(), L"MP3") || !StrCmp(filename.c_str(), L"AAC")) {
		StrCat(buffer, L"Nhạc ");
	}
	else if (!StrCmp(filename.c_str(), L"DOC") || !StrCmp(filename.c_str(), L"DOCX")) {
		StrCat(buffer, L"Microsoft Word ");
	}
	else if (!StrCmp(filename.c_str(), L"XLSX") || !StrCmp(filename.c_str(), L"XLS")) {
		StrCat(buffer, L"Microsoft Exel ");
	}
	else if (!StrCmp(filename.c_str(), L"PPT") || !StrCmp(filename.c_str(), L"PPTX")) {
		StrCat(buffer, L"Microsoft Powerpoint ");
	}
	else if (!StrCmp(filename.c_str(), L"JPG") || !StrCmp(filename.c_str(), L"JPEG") || !StrCmp(filename.c_str(), L"PNG") || !StrCmp(filename.c_str(), L"BMP")) {
		StrCat(buffer, L"Ảnh ");
	}
	StrCat(buffer, filename.c_str());



}

void Drive::FreeMemory()
{
	for (int i = 0; i < bufferAddress.size(); i++){
		delete[] bufferAddress[i];
	}
	delete[] drivesBuffer;

}

void Drive::ShowDriveColumn(ListView* lw)
{
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_TEXT | LVCF_FMT  | LVCF_WIDTH;

	lvCol.fmt = LVCFMT_LEFT ;
	lvCol.cx = 100;
	lvCol.pszText = L"Label";
	ListView_SetColumn(lw->getHandle(), 1, &lvCol);

	lvCol.fmt = LVCFMT_RIGHT ;
	lvCol.cx = 150;
	lvCol.pszText = L"Tổng dung lượng";
	ListView_SetColumn(lw->getHandle(), 2, &lvCol);

	lvCol.cx = 150;
	lvCol.pszText = L"Dung lượng trống";
	ListView_SetColumn(lw->getHandle(), 3, &lvCol);
}

void Drive::SizeConverter(wchar_t* buffer, UINT64 size) {
	wchar_t* DV[] = { L" bytes",L" KB",L" MB", L" GB" };
	int count = 0;
	double sizeD = size;
	while ((size / 1024) != 0 && count < 4) {
		sizeD /= 1024;
		size /= 1024;
		count++;
	}
	wsprintf(buffer, L"%d.%d", int(sizeD), int((sizeD - int(sizeD)) * 100));//in ra 2 số sau dấu thập phân
	StrCat(buffer, DV[count]);
}

void Drive::_GetDiskTotalSize(wchar_t * buffer, wchar_t * drivename)
{
	ULARGE_INTEGER totalsize;
	SHGetDiskFreeSpaceEx(drivename, NULL,&totalsize, NULL);
	UINT64 size = totalsize.QuadPart;
	SizeConverter(buffer, size);

}

void Drive::_GetDiskFreeSpace(wchar_t * buffer, wchar_t * drivename)
{
	ULARGE_INTEGER freeSpace;
	SHGetDiskFreeSpaceEx(drivename, NULL, NULL, &freeSpace);
	UINT64 size = freeSpace.QuadPart;
	SizeConverter(buffer, size);
}

void Drive::_GetDiskLabel(wchar_t * buffer, wchar_t * drivename)
{
	GetVolumeInformation(drivename, buffer, 30, NULL, NULL, NULL, NULL, NULL);
}




Drive::Drive()
{
}


Drive::~Drive()
{
}
