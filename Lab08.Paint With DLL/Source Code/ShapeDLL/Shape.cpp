#include "stdafx.h"
#include "Shape.h"

Shape::Shape()
{
	pen = NULL;
	
}

Shape::~Shape()
{
	delete pen;
}

Shape::Shape(Point lefttop, Point rightbottom)
{
	this->leftTop = lefttop;
	this->rightBottom = rightbottom;
}

void Shape::ReDraw(Graphics * grp)
{
	this->Draw(grp, pen, leftTop, rightBottom);
}

void Shape::SetPoints(Point lefttop, Point rightbottom)
{
	this->leftTop = lefttop;
	this->rightBottom = rightbottom;
}

void Shape::SetPen(Pen * _pen)
{
	if (_pen == NULL || _pen == pen ) return;
	if (pen != NULL)
		delete pen;
	pen = _pen->Clone();
}

Point Shape::GetLeftTop()
{
	return this->leftTop;
}

Point Shape::GetRightBottom()
{
	return this->rightBottom;
}
