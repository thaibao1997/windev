#include "stdafx.h"
#include "Ellipse.h"


Ellipse_::Ellipse_()
{
}


Ellipse_::~Ellipse_()
{
}



void Ellipse_::Draw(Graphics* grp, Pen* _pen, Point lefttop, Point rightbottom)
{
	this->SetPoints(lefttop, rightbottom);
	this->SetPen(_pen);

	int width = abs(lefttop.X - rightbottom.X);
	int height= abs(lefttop.Y - rightbottom.Y);
	int Sx = min(lefttop.X, rightbottom.X);
	int Sy = min(lefttop.Y, rightbottom.Y);

	grp->DrawEllipse(pen, Sx, Sy, width, height);
	
}

Shape * Ellipse_::Clone()
{
	Ellipse_* New = new Ellipse_();
	New->SetPoints(this->GetLeftTop(), this->GetRightBottom());
	New->SetPen(this->pen->Clone());
	return New;
}


