﻿#pragma once
#include "Shape.h"
#ifdef SHAPELIBRARY_EXPORTS
#define SHAPEAPI __declspec(dllexport)
#else
#define SHAPEAPI __declspec(dllimport)
#endif

struct IDrawShape
{
	virtual void SetIsSpecialShape(BOOL val)=0;
	virtual void SetIsDrawingPreview(BOOL val)=0;

	virtual BOOL IsDrawingPreview()=0;
	virtual void ReDrawAll(Graphics* grp)=0;
	virtual void AddShape(Shape* shape)=0;
	virtual Point DrawPreview(Graphics* grp, Pen* pen, Point leftTop, Point rightBottom)=0;
	virtual void SavePreview(Point leftTop, Point rightBottom)=0;
	virtual void CallReDraw(HWND hwnd)=0;
	virtual void SetShapeType(int type) = 0;
	virtual int GetShapeType() = 0;
	virtual int GetNumOfShapes() = 0;
	virtual void Clear() = 0;
	virtual void Release() = 0;
	
};

extern "C" SHAPEAPI IDrawShape*  _stdcall  GetDrawShape();
