#include "stdafx.h"
#include "Rectangle.h"


Rectangle_::Rectangle_()
{
}


Rectangle_::~Rectangle_()
{
}


void Rectangle_::Draw(Graphics* grp, Pen* _pen, Point lefttop, Point rightbottom)
{
	this->SetPoints(lefttop, rightbottom);
	this->SetPen(_pen);
	int width = abs(lefttop.X - rightbottom.X);
	int height = abs(lefttop.Y - rightbottom.Y);
	int Sx = min(lefttop.X, rightbottom.X);
	int Sy = min(lefttop.Y, rightbottom.Y);
	grp->DrawRectangle(pen, Sx, Sy, width, height);
}


Shape * Rectangle_::Clone()
{
	Rectangle_* New = new Rectangle_();
	New->SetPoints(this->GetLeftTop(), this->GetRightBottom());
	New->SetPen(this->pen->Clone());
	return New;
}

