#include "stdafx.h"
#include "StatusBar.h"
#include<CommCtrl.h>

StatusBar::StatusBar(HWND parentHandle, HINSTANCE hInst)
{

	handle = CreateWindowEx(NULL, STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0,parentHandle, NULL, hInst, NULL);
}


StatusBar::~StatusBar()
{
}

void StatusBar::ChangeSize()
{
	if(handle != NULL)
	SendMessage(handle, WM_SIZE, 0, 0);
}

void StatusBar::SetParts(int no, int*  size)
{
	noParts = no;
	SendMessage(handle, SB_SETPARTS, (WPARAM)no, (LPARAM)size);
}

void StatusBar::SetText(int index, wchar_t * text)
{
	SendMessage(handle, SB_SETTEXT, index,(LPARAM)text);

}
