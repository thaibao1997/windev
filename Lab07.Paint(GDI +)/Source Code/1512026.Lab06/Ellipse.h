#pragma once
#include"Shape.h"

class Ellipse_ : public Shape
{
public:
	Ellipse_();
	~Ellipse_();
	void Draw(Graphics* grp, Pen* _pen, Point lefttop, Point rightbottom);
	Shape* Clone();
};

