﻿#pragma once
#include"Line.h"
#include"Ellipse.h"
#include"Rectangle.h"
#include<vector>
using namespace std;

#define RIBBON_HEIGHT 147

#define LINE_PROTOTYPE_INDEX 0
#define ELLIPSE_PROTOTYPE_INDEX 1
#define RECTANGLE_PROTOTYPE_INDEX 2


class DrawShape
{
private:
	vector<Shape*> prototypes;
	vector<Shape*> shapes;
	BOOL isDrawingPreview;
	int shapeType;
	BOOL isSpecialShape;
public:

	DrawShape();
	~DrawShape();
	//void ToggleDrawingPreview();
	//void ToggleDrawingSpecialShape();
	void SetIsSpecialShape(BOOL val);
	void SetIsDrawingPreview(BOOL val);

	BOOL IsDrawingPreview();
	void ReDrawAll(Graphics* grp);
	void AddShape(Shape* shape);
	Point DrawPreview(Graphics* grp, Pen* pen, Point leftTop, Point rightBottom);
	void SavePreview(Point leftTop, Point rightBottom);
	void CallReDraw(HWND hwnd);
	void SetShapeType(int type);
	int GetShapeType();
	int GetNumOfShapes();//lấy số lượng shape có thể vẽ
	void Clear();//xoá hình đã vẽ
};

