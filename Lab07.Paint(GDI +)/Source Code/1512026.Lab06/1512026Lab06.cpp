﻿// 1512026Lab06.cpp : Defines the entry point for the application.
//
#pragma once

#include "stdafx.h"
#include "1512026Lab06.h"
#include<windowsx.h>
#include"StatusBar.h"
#include"DrawShape.h"
#include<fstream>
#include <algorithm>
#include <Commdlg.h>
#include<string>
#include "RibbonFramework.h"
#include "RibbonIDs.h"
#include <Objbase.h>
#include<UIRibbonPropertyHelpers.h>

#pragma comment(lib, "Ole32.lib")

using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#define MAX_LOADSTRING 100
// Global Variables:
const int gRibbonHeight = 150;
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
Point gLeftTop, gRightBottom;
DrawShape gDrawShape;
StatusBar* gStatusBar;
int currentCheckedMenu;//id menu dang được check
BOOL gIsSaved = TRUE;
ULONG_PTR gdiplusToken;
GdiplusStartupInput gdiplusStartupInput;
std::wstring gOpenImagePath;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnRButtonUp(HWND hwnd, int x, int y, UINT flags);
void OnClose(HWND hwnd);

void CheckMenu(HMENU hMenu, int ID);
void UncheckMenu(HMENU hMenu, int ID);
void UnCheckedAllMenu(HMENU hMenu);
void SaveImage(HWND hwnd,wstring path);

std::wstring SaveFileDialog(HWND hWnd);
std::wstring OpenFileDialog(HWND hWnd);

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);//https://msdn.microsoft.com/en-us/library/windows/desktop/ms533843(v=vs.85).aspx
std::wstring GetImageType(std::wstring path);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr)) {
		return FALSE;
	}
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512026LAB06, szWindowClass, MAX_LOADSTRING);
	

    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512026LAB06));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
	CoUninitialize();
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0;//CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512026LAB06);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 800, 600, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_KEYDOWN, OnKeyDown);
		HANDLE_MSG(hWnd, WM_KEYUP, OnKeyUp);
		HANDLE_MSG(hWnd, WM_RBUTTONUP, OnRButtonUp);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
	case WS_CLIPCHILDREN:
		break;
	case WM_ERASEBKGND:
		return 1;

	case	WM_MOUSELEAVE:
		MessageBox(0, 0, 0, 0);
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	gStatusBar->ChangeSize();
	//InitializeFramework(hWnd);
}
//HWND gPaintWindow;
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	//gPaintWindow = CreateWindowEx(NULL, L"static", L"", WS_CHILD | WS_VISIBLE, 0, RIBBON_HEIGHT + 5, 800, 600, hWnd, NULL, hInst, NULL);

	gStatusBar = new StatusBar(hWnd, hInst);
	int SBsize[] = { 100, 250 };
	gStatusBar->SetParts(3, SBsize);


	bool init = InitializeFramework(hWnd);
	if (!InitializeFramework(hWnd)) {
		return -1;
	}

	currentCheckedMenu = ID_SHAPE_LINE;
	HMENU hMenu = GetMenu(hWnd);
	CheckMenu(hMenu, currentCheckedMenu);
	gStatusBar->SetText(1, L"Line");





	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	HMENU hMenu = GetMenu(hWnd);
	wstring path;
	switch (id)
	{
	case ID_SHAPE_LINE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_LINE);
		currentCheckedMenu = ID_SHAPE_LINE;
		gDrawShape.SetShapeType(LINE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Line");

		break;
	case ID_SHAPE_ELLIPSE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_ELLIPSE);
		currentCheckedMenu = ID_SHAPE_ELLIPSE;
		gDrawShape.SetShapeType(ELLIPSE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Ellipse");
		break; 
	case ID_SHAPE_RECTANGLE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_RECTANGLE);
		currentCheckedMenu = ID_SHAPE_RECTANGLE;
		gDrawShape.SetShapeType(RECTANGLE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Rectangle");
		break;
	case IDM_ABOUT:
		//OpenImage(hWnd,L"a.bmp");
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case ID_FILE_SAVE:
		//path=SaveFileDialog(hWnd);
		if (gOpenImagePath != L"") {
			SaveImage(hWnd, gOpenImagePath);
			gIsSaved = TRUE;
			break;
		}
		else {
			//go to save as
		}
	case ID_FILE_SAVEAS:
		path = SaveFileDialog(hWnd);
		if (path != L"") {
			SaveImage(hWnd, path);
			gOpenImagePath = path;
			gIsSaved = TRUE;
		}
		break;
	case ID_FILE_OPEN:
		if (gIsSaved == FALSE) {
			std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
			int choose = MessageBox(hWnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
			if (choose == IDYES) {
				path = SaveFileDialog(hWnd);
				if (path != L"") {
					SaveImage(hWnd, path);
				}
			}else if (choose == IDCANCEL) {
				break;
			}
		}
		gOpenImagePath = OpenFileDialog(hWnd);	
		gDrawShape.Clear();
		break;
	case ID_FILE_NEW:
		if (gIsSaved == FALSE) {
			std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
			int choose = MessageBox(hWnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
			if (choose == IDYES) {
				path = SaveFileDialog(hWnd);
				if (path != L"") {
					SaveImage(hWnd, path);
				}
			}
			else if (choose == IDCANCEL) {
				break;
			}
		}
		gOpenImagePath = L"";
		gDrawShape.Clear();
		gDrawShape.CallReDraw(hWnd);
		gIsSaved = TRUE;
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}
void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	RECT winRect,drawRect;
	HDC          hdcMem;

	HBITMAP      hbmMem;
	HANDLE       hOld;

	int width, height;
	HDC hdc = BeginPaint(hWnd, &ps);

	GetClientRect(hWnd, &winRect);

	HRGN  drawRegion = CreateRectRgn(winRect.left, winRect.top + RIBBON_HEIGHT, winRect.right, winRect.bottom -23);
	SelectClipRgn(hdc, drawRegion);
	GetRgnBox(drawRegion, &drawRect);

	width = winRect.right - winRect.left;
	height = winRect.bottom - winRect.top -22 ;//22 for status bar

	// Create an off-screen DC for double-buffering
	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, width, height);
	hOld = SelectObject(hdcMem, hbmMem);
	
	FillRect(hdcMem, &winRect, (HBRUSH)(COLOR_WINDOW + 1));//tô trắng vùng
	//FillRect(hdcMem, &winRect, HBRUSH(GetBkColor(hdcMem)));

	Graphics* grp = new Graphics(hdcMem);
	grp->SetSmoothingMode(SmoothingModeAntiAlias);

	int R, B, G;
	R = GetRValue(gColor);
	G = GetGValue(gColor);
	B = GetBValue(gColor);
	Pen* pen = new Pen(Color(255, R, G, B), gPenWidth);
	pen->SetDashStyle(DashStyle(gPenStyle));
	Image* img = NULL;
	
	if (gOpenImagePath != L"") {
		img=img->FromFile(gOpenImagePath.c_str());
		grp->DrawImage(img, drawRect.left, drawRect.top);
	}

	int currentPenWidth = pen->GetWidth();
	if (gRightBottom.X < drawRect.left + currentPenWidth)
		gRightBottom.X = drawRect.left + currentPenWidth;
	if (gRightBottom.Y < drawRect.top + currentPenWidth)
		gRightBottom.Y = drawRect.top + currentPenWidth;

	if (gRightBottom.X > drawRect.right - currentPenWidth)
		gRightBottom.X = drawRect.right - currentPenWidth;
	if (gRightBottom.Y > drawRect.bottom - currentPenWidth)
		gRightBottom.Y = drawRect.bottom - currentPenWidth;

	gDrawShape.DrawPreview(grp,pen ,gLeftTop, gRightBottom);
	gDrawShape.ReDrawAll(grp);


	BitBlt(hdc, 0, 0, width, height, hdcMem, 0, 0, SRCCOPY);
	delete pen;

	delete img;
	delete grp;
	DeleteObject(drawRegion);
	SelectObject(hdcMem, hOld);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);
	EndPaint(hWnd, &ps);

}

void OnDestroy(HWND hWnd)
{
	delete gStatusBar;
	DestroyFramework();
	//DeleteObject(gOpenImage);
	//GdiplusShutdown(gdiplusToken);
	PostQuitMessage(0);

}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags)
{
	gRightBottom.X = x;
	gRightBottom.Y = y;
	if (gDrawShape.IsDrawingPreview()) {
		int ctrl = GetKeyState(VK_LSHIFT);
		if (ctrl & 0x8000 ) { // 1: DOWN, 0: UP
			gDrawShape.SetIsSpecialShape(TRUE);
		}
		else {
			gDrawShape.SetIsSpecialShape(FALSE);
		}
		gDrawShape.CallReDraw(hWnd);
	}
	wchar_t buffer[64];
	wsprintf(buffer,L"x:%d,y:%d", x, y);
	gStatusBar->SetText(0, buffer);

}

void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	gLeftTop.X = x;
	gLeftTop.Y = y;
	gDrawShape.SetIsDrawingPreview(TRUE);
	gIsSaved = FALSE;
	SetCapture(hwnd);
}

void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
	//if(gLeftTop.X != gRightBottom.X && gLeftTop.Y != gRightBottom.Y)
		gDrawShape.SavePreview(gLeftTop, gRightBottom);
	gDrawShape.SetIsDrawingPreview(FALSE);
	gDrawShape.CallReDraw(hwnd);
	gDrawShape.SetIsSpecialShape(FALSE);
	ReleaseCapture();

}

void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	int ctrl = GetKeyState(VK_LSHIFT);
	if (ctrl & 0x8000) { // 1: DOWN, 0: UP
		gDrawShape.SetIsSpecialShape(TRUE);
		if (currentCheckedMenu == ID_SHAPE_RECTANGLE) {
			gStatusBar->SetText(1, L"Square");
		}
		if (currentCheckedMenu == ID_SHAPE_ELLIPSE) {
			gStatusBar->SetText(1, L"Circle");
		}
	}
	
	gDrawShape.CallReDraw(hwnd);
}

void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	int ctrl = GetKeyState(VK_LSHIFT);
	if (!(ctrl & 0x8000)) { // 1: DOWN, 0: UP
		gDrawShape.SetIsSpecialShape(FALSE);
		if (currentCheckedMenu == ID_SHAPE_RECTANGLE) {
			gStatusBar->SetText(1, L"Rectangle");
		}
		if (currentCheckedMenu == ID_SHAPE_ELLIPSE) {
			gStatusBar->SetText(1, L"Ellipse");
		}
	}
	gDrawShape.CallReDraw(hwnd);

}

void OnRButtonUp(HWND hwnd, int x, int y, UINT flags)
{
	//MessageBox(0, 0, 0, 0);
	POINT p;
	p.x = x; p.y = y;
	HMENU hMenuPopup = CreatePopupMenu();
	InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_FILE_NEW, L"New..");
	InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_FILE_OPEN, L"Open..");
	InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_FILE_SAVE, L"Save..");

	InsertMenu(hMenuPopup, 0,  MF_UNCHECKED | MF_STRING, ID_SHAPE_LINE, L"Line");
	InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_SHAPE_ELLIPSE, L"Ellipse");
	InsertMenu(hMenuPopup, 0,  MF_UNCHECKED | MF_STRING, ID_SHAPE_RECTANGLE, L"Rectangle");

	CheckMenu(hMenuPopup, currentCheckedMenu);
	ClientToScreen(hwnd, &p);
	TrackPopupMenu(hMenuPopup, TPM_TOPALIGN | TPM_LEFTALIGN, p.x, p.y, 0, hwnd, NULL);
}

void OnClose(HWND hwnd)
{
	if (gIsSaved == FALSE) {
		std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
		int choose = MessageBox(hwnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
		if (choose == IDYES) {
			std::wstring path = SaveFileDialog(hwnd);
			if (path != L"") {
				SaveImage(hwnd, path);
			}
		}else if (choose == IDCANCEL) {
			return;
		}
	}
	DestroyWindow(hwnd);
}


void CheckMenu(HMENU hMenu, int ID)
{
	CheckMenuItem(hMenu, ID, MF_CHECKED);
}

void UncheckMenu(HMENU hMenu, int ID)
{
	CheckMenuItem(hMenu, ID, MF_UNCHECKED);
}

void UnCheckedAllMenu(HMENU hMenu)
{
	for (int i = ID_SHAPE_LINE; i <= ID_SHAPE_LINE + gDrawShape.GetNumOfShapes(); i++) {
		CheckMenuItem(hMenu, i, MF_UNCHECKED);
	}
}

void SaveImage(HWND hwnd,wstring path)
{
	HDC hdc = GetDC(hwnd);
	HDC          hdcMem;
	HBITMAP      hbmMem;
	HANDLE       hOld;
	RECT rect, drawRect;
	int width, height;

	GetClientRect(hwnd, &rect);
	width = rect.right - rect.left;
	height = rect.bottom - rect.top - 22;//22 for status bar


	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, width, height);

	HANDLE oldHandle = SelectObject(hdcMem, hbmMem);




	BitBlt(hdcMem, 0, 0, width, height, hdc, 0, 0, SRCCOPY);

	Bitmap bitmap(hbmMem, NULL);

	int swidth = bitmap.GetWidth();
	int sheight = bitmap.GetHeight()- RIBBON_HEIGHT - 1;

	Bitmap* cloneBitmap = bitmap.Clone(rect.left, RIBBON_HEIGHT, swidth, sheight, PixelFormatDontCare);

	CLSID clsid;
	GetEncoderClsid(GetImageType(path).c_str(), &clsid);
	//bitmap.Save(path.c_str(), &clsid);
	cloneBitmap->Save(path.c_str(), &clsid);

	delete cloneBitmap;
	SelectObject(hdcMem, oldHandle);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);
	ReleaseDC(hwnd, hdc);
}


void OpenImage(HWND hwnd,HDC hdc, wstring path)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int width = rect.right - rect.left;
	int lenght = rect.bottom - rect.top - 22; //22 for status bar
	HBITMAP hbitmap = (HBITMAP)LoadImage(NULL, path.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (hbitmap != NULL) {
		HDC          hdcMem2;
		hdcMem2 = CreateCompatibleDC(hdc);
		SelectObject(hdcMem2, hbitmap);
		BitBlt(hdc, 0, 0, width, lenght, hdcMem2, 0, 0, SRCCOPY);
		DeleteDC(hdcMem2);

	}
}


std::wstring SaveFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 1024;
	wchar_t szFileName[BUFFER_SIZE] = L"";
	wchar_t szFileTitle[128] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"BMP Image(*.bmp)\0*.bmp\0PNG Image(*.png)\0*.png\0JPEG files(*.jpeg)\0*.jpeg\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_NOTESTFILECREATE | OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&ofn) == TRUE)
	{
		std::wstring path(szFileName);
		std::wstring filename(szFileTitle);
		int pos = path.rfind(L"."); //lấy phần extension
		if (pos == wstring::npos) {//ko có extension
			if (ofn.nFilterIndex == 1)
				path.append(L".bmp");
			else if (ofn.nFilterIndex == 2)
				path.append(L".png");
			else if (ofn.nFilterIndex == 3)
				path.append(L".jpeg");
		}
		return path;
	}

	return L"";
}

std::wstring OpenFileDialog(HWND hWnd)
{
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 1024;
	wchar_t szFileName[BUFFER_SIZE] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"BMP Image(*.bmp)\0*.bmp\0PNG Image(*.png)\0*.png\0JPEG files(*.jpeg)\0*.jpeg\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn) == TRUE)
	{
		std::wstring wstr(szFileName);
		return wstr;
	}
	return L"";
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

std::wstring GetImageType(std::wstring path)
{
	int pos = path.rfind(L"."); //lấy phần extension
	path = path.substr(pos + 1, path.size() - pos);
	std::transform(path.begin(), path.end(), path.begin(), ::toupper); //chuyển extension thành chữ hoa

	if (path == L"PNG")
		return L"image/png";
	if(path == L"JPEG")
		return L"image/jpeg";
	return  L"image/bmp";
}
