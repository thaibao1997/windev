#pragma once
#include"1512026Lab06.h"

class StatusBar
{
private:
	HWND handle;
	int noParts;
	
public:
	StatusBar(HWND parentHandle, HINSTANCE hInst);
	~StatusBar();
	void ChangeSize();
	void SetParts(int no, int* size);
	void SetText(int index, wchar_t* text);
};

