#pragma once
#include <windows.h>
#include <ObjIdl.h>
#include<gdiplus.h>
#pragma comment(lib, "gdiplus.lib")
using namespace Gdiplus;

class Shape
{
protected:
	Pen* pen;
	Point leftTop;
	Point rightBottom;
public:
	Shape();
	~Shape();
	Shape(Point lefttop, Point rightbottom);

	virtual void Draw(Graphics* grp,Pen* pen,Point lefttop, Point rightbottom) = 0;
	virtual Shape* Clone()=0;

	void ReDraw(Graphics* grp);
	void SetPoints(Point lefttop, Point rightbottom);
	void SetPen(Pen* _pen);
	Point GetLeftTop();
	Point GetRightBottom();
};

