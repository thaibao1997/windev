#include "stdafx.h"
#include "DrawShape.h"



DrawShape::DrawShape()
{
	prototypes.resize(3);
	prototypes[LINE_PROTOTYPE_INDEX] = new Line();
	prototypes[ELLIPSE_PROTOTYPE_INDEX] = new Ellipse_();
	prototypes[RECTANGLE_PROTOTYPE_INDEX] = new Rectangle_();
	isDrawingPreview = false;
	isSpecialShape = false;
	shapeType = LINE_PROTOTYPE_INDEX;

	/*ULONG_PTR gdiplusToken;
	GdiplusStartupInput gdiplusStartupInput;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	Gdiplus::Pen defaultPen(Color(255, 0, 0, 0), 1);
	for (int i = 0; i < prototypes.size(); i++) {
		prototypes[i]->SetPen(&defaultPen);
	}
	//GdiplusShutdown(gdiplusToken);*/
}


DrawShape::~DrawShape()
{
	for (int i = 0; i < shapes.size(); i++)
		delete shapes[i];
	for (int i = 0; i < prototypes.size(); i++)
		delete prototypes[i];
}

/*void DrawShape::ToggleDrawingPreview()
{
	isDrawingPreview = !isDrawingPreview;
}*/


void DrawShape::SetIsSpecialShape(BOOL val)
{
	isSpecialShape = val;
}

void DrawShape::SetIsDrawingPreview(BOOL val)
{
	isDrawingPreview = val;
}

BOOL DrawShape::IsDrawingPreview()
{
	return this->isDrawingPreview;
}

void DrawShape::ReDrawAll(Graphics* grp)
{
	for (int i = 0; i < shapes.size(); i++)
		shapes[i]->ReDraw(grp);
}

void DrawShape::AddShape(Shape * shape)
{
	this->shapes.push_back(shape);
}

Point DrawShape::DrawPreview(Graphics* grp,Pen* pen ,Point leftTop, Point rightBottom){
	if (this->isDrawingPreview == TRUE) {
		if (isSpecialShape && shapeType != LINE_PROTOTYPE_INDEX) {
			int dx = abs(leftTop.X - rightBottom.X);
			int dy= abs(leftTop.Y - rightBottom.Y);
			if(rightBottom.X >leftTop.X)
				rightBottom.X = leftTop.X + min(dx,dy);
			else if (rightBottom.X <leftTop.X) 
				rightBottom.X = leftTop.X - min(dx, dy);
			if (rightBottom.Y >leftTop.Y)
				rightBottom.Y = leftTop.Y + min(dx, dy);
			else if (rightBottom.Y <leftTop.Y)
				rightBottom.Y = leftTop.Y - min(dx, dy);
		}
		prototypes[shapeType]->Draw(grp,pen ,leftTop, rightBottom);
		return rightBottom;
	}
}

void DrawShape::SavePreview(Point leftTop, Point rightBottom)
{
	if (this->isDrawingPreview == TRUE && (leftTop.X != rightBottom.X || leftTop.Y != rightBottom.Y))
		this->shapes.push_back(prototypes[shapeType]->Clone());
}

void DrawShape::CallReDraw(HWND hwnd)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	/*rect.left = 0;
	rect.top = RIBBON_HEIGHT;*/
	InvalidateRect(hwnd, &rect, FALSE);
}

void DrawShape::SetShapeType(int type)
{
	if (type < LINE_PROTOTYPE_INDEX || type>RECTANGLE_PROTOTYPE_INDEX) return;
	shapeType = type;

}

int DrawShape::GetShapeType()
{
	return this->shapeType;
}

int DrawShape::GetNumOfShapes()
{
	return prototypes.size();
}

void DrawShape::Clear()
{
	this->shapes.clear();
}
