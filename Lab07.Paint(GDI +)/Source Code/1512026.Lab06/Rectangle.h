#pragma once
#include"Shape.h"

class Rectangle_ : public Shape
{
public:
	Rectangle_();
	~Rectangle_();
	void Draw(Graphics* grp, Pen* _pen, Point lefttop, Point rightbottom);
	Shape* Clone();
};

