// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define tabHome 2 
#define tabHome_LabelTitle_RESID 60001
#define groupShape 3 
#define groupShape_LabelTitle_RESID 60002
#define groupPen 4 
#define groupPen_LabelTitle_RESID 60003
#define groupColor 5 
#define groupColor_LabelTitle_RESID 60004
#define ID_BTN_NEW 6 
#define ID_BTN_NEW_LabelTitle_RESID 60005
#define ID_BTN_NEW_LargeImages_RESID 202
#define ID_BTN_OPEN 7 
#define ID_BTN_OPEN_LabelTitle_RESID 60006
#define ID_BTN_OPEN_LargeImages_RESID 60007
#define ID_BTN_SAVE 8 
#define ID_BTN_SAVE_LabelTitle_RESID 60008
#define ID_BTN_SAVE_LargeImages_RESID 201
#define ID_BTN_SAVEAS 9 
#define ID_BTN_SAVEAS_LabelTitle_RESID 60009
#define ID_BTN_SAVEAS_LargeImages_RESID 60010
#define ID_BTN_CLOSE 10 
#define ID_BTN_CLOSE_LabelTitle_RESID 60011
#define ID_BTN_CLOSE_LargeImages_RESID 60012
#define ID_BTN_SHAPE 11 
#define ID_BTN_SHAPE_LabelTitle_RESID 60013
#define ID_BTN_LINE 12 
#define ID_BTN_LINE_LabelTitle_RESID 60014
#define ID_BTN_RECT 13 
#define ID_BTN_RECT_LabelTitle_RESID 60015
#define ID_BTN_ELLIP 14 
#define ID_BTN_ELLIP_LabelTitle_RESID 60016
#define ID_BTN_COLOR 15 
#define ID_BTN_COLOR_LabelTitle_RESID 60017
#define ID_BTN_COLOR_LargeImages_RESID 60018
#define ID_BTN_PENW 16 
#define ID_BTN_PENW_LabelTitle_RESID 60019
#define ID_BTN_PENW_LargeImages_RESID 60020
#define ID_BTN_PENS 17 
#define ID_BTN_PENS_LabelTitle_RESID 60021
#define ID_BTN_PENS_LargeImages_RESID 60022
#define InternalCmd2_LabelTitle_RESID 60023
