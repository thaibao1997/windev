//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512026Lab06.rc
//
#define IDC_MYICON                      2
#define IDD_1512026LAB06_DIALOG         102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512026LAB06                107
#define IDI_SMALL                       108
#define IDC_1512026LAB06                109
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define IDB_BMRECT                      133
#define IDB_BMLINE                      134
#define IDB_BMELLIP                     136
#define IDB_SIZe1                       142
#define IDB_SIZE1                       142
#define IDB_SIZE2                       143
#define IDB_SIZE3                       144
#define IDB_SIZE4                       145
#define IDB_SIZE5                       151
#define ID_SHAPE_LINE                   32771
#define ID_SHAPE_ELLIPSE                32772
#define ID_SHAPE_RECTANGLE              32773
#define ID_FILE_SAVE                    32774
#define ID_FILE_OPEN                    32775
#define ID_FILE_NEW                     32776
#define ID_FILE_SAVEAS                  32777
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
