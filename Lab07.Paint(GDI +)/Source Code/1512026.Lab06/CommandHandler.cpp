// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
#include "stdafx.h"
#include <UIRibbon.h>
#include<Propvarutil.h>
#include<Uiribbon.h>
#include<UIRibbonPropertyHelpers.h>
#include "CommandHandler.h"
#include "RibbonIDs.h"
#include "RibbonFramework.h"
#include"resource.h"
#pragma comment(lib, "Propsys.lib")
// Static method to create an instance of the object.
HRESULT CCommandHandler::CreateInstance(IUICommandHandler **ppCommandHandler)
{
	if (!ppCommandHandler)
	{
		return E_POINTER;
	}

	*ppCommandHandler = NULL;

	HRESULT hr = S_OK;

	CCommandHandler* pCommandHandler = new CCommandHandler();

	if (pCommandHandler != NULL)
	{
		*ppCommandHandler = static_cast<IUICommandHandler *>(pCommandHandler);
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	return hr;
}

// IUnknown method implementations.
STDMETHODIMP_(ULONG) CCommandHandler::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

STDMETHODIMP_(ULONG) CCommandHandler::Release()
{
	LONG cRef = InterlockedDecrement(&m_cRef);
	if (cRef == 0)
	{
		delete this;
	}

	return cRef;
}

STDMETHODIMP CCommandHandler::QueryInterface(REFIID iid, void** ppv)
{
	if (iid == __uuidof(IUnknown))
	{
		*ppv = static_cast<IUnknown*>(this);
	}
	else if (iid == __uuidof(IUICommandHandler))
	{
		*ppv = static_cast<IUICommandHandler*>(this);
	}
	else
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	AddRef();
	return S_OK;
}

//
//  FUNCTION: UpdateProperty()
//
//  PURPOSE: Called by the Ribbon framework when a command property (PKEY) needs to be updated.
//
//  COMMENTS:
//
//    This function is used to provide new command property values, such as labels, icons, or
//    tooltip information, when requested by the Ribbon framework.  
//    
//    In this SimpleRibbon sample, the method is not implemented.  
//
STDMETHODIMP CCommandHandler::UpdateProperty(
	UINT nCmdID,
	REFPROPERTYKEY key,
	const PROPVARIANT* ppropvarCurrentValue,
	PROPVARIANT* ppropvarNewValue)
{
	UNREFERENCED_PARAMETER(nCmdID);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(ppropvarCurrentValue);
	UNREFERENCED_PARAMETER(ppropvarNewValue);
	HRESULT hr = S_FALSE;
	if (key == UI_PKEY_ItemsSource && nCmdID== ID_BTN_PENW)
	{
		IUICollection* pCollection;
		hr = ppropvarCurrentValue->punkVal->QueryInterface(
			IID_PPV_ARGS(&pCollection));
		if (FAILED(hr)) return hr;
		UINT imgIDs[] = { IDB_SIZE1,IDB_SIZE2,IDB_SIZE3,IDB_SIZE4,IDB_SIZE5 };
		for (int i = 0; i < 5; i++) {
			IUIImage* pImg=NULL;
			CreateUIImageFromBitmapResource(MAKEINTRESOURCE(imgIDs[i]), &pImg);
			CPropertySet* pItem;
			wchar_t buffer[8];
			swprintf_s(buffer, L"%d px", i+1);
			hr = CPropertySet::CreateInstance(&pItem);
			pItem->InitializeItemProperties(pImg, buffer, i);
			pCollection->Add(pItem);
			pItem->Release();
		}

		pCollection->Release();
		hr = S_OK;

	}
	else if (key == UI_PKEY_ItemsSource && nCmdID == ID_BTN_PENS)
	{
		IUICollection* pCollection;
		hr = ppropvarCurrentValue->punkVal->QueryInterface(
			IID_PPV_ARGS(&pCollection));
		if (FAILED(hr)) return hr;
		wchar_t* styles[] = { L"Solid",L"Dash" ,L"Dot" ,L"Dash Dot",L"Dash Dot Dot" };
		for (int i = 0; i < 5; i++) {
			CPropertySet* pItem;
			hr = CPropertySet::CreateInstance(&pItem);
			pItem->InitializeItemProperties(NULL, styles[i], i);
			pCollection->Add(pItem);
			pItem->Release();
		}

		pCollection->Release();
		hr = S_OK;
	
	}else if(key == UI_PKEY_ItemsSource && nCmdID == ID_BTN_SHAPE) {
		/**/
		IUICollection* pCollection;
		hr = ppropvarCurrentValue->punkVal->QueryInterface(
			IID_PPV_ARGS(&pCollection));
		if (FAILED(hr)) return hr;

		UINT imgIDs[] = { IDB_BMLINE,IDB_BMRECT,IDB_BMELLIP };
		wchar_t* lables[] = { L"Line\0", L"Rectangle\0",L"Ellipse\0" };
		for (int i = 0; i < sizeof(imgIDs)/sizeof(UINT); i++) {
			IUIImage* pImg;
			CreateUIImageFromBitmapResource(MAKEINTRESOURCE(imgIDs[i]), &pImg);
			CPropertySet* pItem;
			hr = CPropertySet::CreateInstance(&pItem);
			if (FAILED(hr)) return hr;
			pItem->InitializeItemProperties(pImg, lables[i], UI_COLLECTION_INVALIDINDEX);
			pCollection->Add(pItem);
			pItem->Release();
		}
		
		pCollection->Release();
		hr = S_OK;

	}
	
	return hr;
}

//
//  FUNCTION: Execute()
//
//  PURPOSE: Called by the Ribbon framework when a command is executed by the user.  For example, when
//           a button is pressed.
//
STDMETHODIMP CCommandHandler::Execute(
	UINT nCmdID,
	UI_EXECUTIONVERB verb,
	const PROPERTYKEY* key,
	const PROPVARIANT* ppropvarValue,
	IUISimplePropertySet* pCommandExecutionProperties)
{
	UNREFERENCED_PARAMETER(pCommandExecutionProperties);
	UNREFERENCED_PARAMETER(ppropvarValue);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(verb);
	UNREFERENCED_PARAMETER(nCmdID);
	HWND hwnd = GetForegroundWindow();
	PROPVARIANT var, varNew;
	ULONG lg;

	switch (nCmdID)
	{
	case ID_BTN_NEW:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_FILE_NEW, NULL);
		break;
	case ID_BTN_OPEN:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_FILE_OPEN, NULL);
		break;
	case ID_BTN_SAVE:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_FILE_SAVE, NULL);
		break;
	case ID_BTN_SAVEAS:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_FILE_SAVEAS, NULL);
		break;
	case ID_BTN_CLOSE:
		SendMessage(hwnd, WM_CLOSE, NULL, NULL);
		break;
	case ID_BTN_LINE:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_LINE, NULL);
		break;	
	case ID_BTN_RECT:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_RECTANGLE, NULL);
		break;
	case ID_BTN_ELLIP:
		SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_ELLIPSE, 0);
		break;
	case ID_BTN_PENW:
		gPenWidth = ppropvarValue->uintVal + 1;
		break;
	case ID_BTN_PENS:
		gPenStyle = ppropvarValue->uintVal;
		break;
	case ID_BTN_COLOR:
		if(pCommandExecutionProperties != NULL)
			pCommandExecutionProperties->GetValue(UI_PKEY_Color, &var);
		gColor = PropVariantToInt32WithDefault(var, 0);
		//PropVariantToUInt32(*ppropvarValue, &lg);
		break;
	case ID_BTN_SHAPE:
	{
		int selected = ppropvarValue->intVal;
		if (selected == 0) {
			SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_LINE, NULL);
		}
		else if (selected == 1) {
			SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_RECTANGLE, 0);

		}else if (selected == 2) {
			SendMessage(hwnd, WM_COMMAND, (WPARAM)ID_SHAPE_ELLIPSE, 0);

		}

	}
		break;
	default:
		break;
	}
	return S_OK;
}

// Factory method to create IUIImages from resource identifiers. 
void CCommandHandler::CreateUIImageFromBitmapResource(LPCTSTR pszResource,
	IUIImage **ppimg)
{
	IUIImageFromBitmap* m_pifbFactory;

	CoCreateInstance(CLSID_UIRibbonImageFromBitmapFactory,
		NULL, CLSCTX_ALL, IID_PPV_ARGS(&m_pifbFactory));

	// Load the bitmap from the resource file.
	HBITMAP hbm = (HBITMAP)LoadImage(GetModuleHandle(NULL), pszResource,
		IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);

	// Use the factory implemented by the framework to produce an IUIImage.
	m_pifbFactory->CreateImage(hbm, UI_OWNERSHIP_TRANSFER, ppimg);
}