#pragma once
#include "Shape.h"
class Line :
	public Shape
{
public:
	Line();
	~Line();
	void Draw(Graphics* grp, Pen* _pen, Point lefttop, Point rightbottom);
	Shape* Clone();
};

