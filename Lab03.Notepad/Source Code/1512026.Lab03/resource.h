//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 1512026Lab03.rc
//
#define IDC_MYICON                      2
#define IDD_1512026LAB03_DIALOG         102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_1512026LAB03                107
#define IDI_SMALL                       108
#define IDC_1512026LAB03                109
#define ID_EDIT_NOTE                    110
#define MAIN_ICON                       111
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       131
#define IDI_ICON2                       141
#define IDB_BITMAP1                     143
#define ID_FILE_OPENFILE                32771
#define ID_FILE_NEWFILE                 32772
#define ID_FILE_SAVE                    32773
#define ID_FILE_S                       32774
#define ID_EDIT_CUT                     32775
#define ID_EDIT_COPY                    32776
#define ID_EDIT_PASTE                   32777
#define ID_EDIT_DELETE                  32778
#define ID_EDIT_SELECTALL               32779
#define ID_FILE_SAVEAS                  32780
#define ID_EDIT_UNDO                    32781
#define IDM_FILE_SAVE                   32787
#define ID_ACCELERATOR32793             32793
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32795
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           112
#endif
#endif
