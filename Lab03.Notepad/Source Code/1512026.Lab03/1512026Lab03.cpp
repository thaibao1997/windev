﻿// 1512026Lab03.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512026Lab03.h"
#include "windowsx.h"
#include <fstream>
#include <string>
#include <locale>
#include <codecvt>
#include <Commdlg.h>


#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#define MAX_LOADSTRING 100

#define DEFAULT_WIDTH 800
#define DEFAULT_HEIGHT 550
#define HSCROLL_WIDTH 60
#define VSCROLL_WIDTH 16

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hWndMain;


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void OnSize(HWND hwnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hwnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnDestroy(HWND hwnd);
void OnClose(HWND hwnd);

void ReadFromFile(std::wstring file);
void WriteToFile(std::wstring file);
std::wstring OpenFileDialog(HWND hWnd);
std::wstring SaveFileDialog(HWND hWnd);
std::string DetectFileEncoding(std::wstring filename);
int SaveOldFile(HWND hWnd);


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512026LAB03, szWindowClass, MAX_LOADSTRING);


    MyRegisterClass(hInstance);



	
    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512026LAB03));




    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(hWndMain, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512026LAB03);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON2));
	

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWndMain = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, DEFAULT_WIDTH, DEFAULT_HEIGHT, nullptr, nullptr, hInstance, nullptr);

   if (!hWndMain)
   {
      return FALSE;
   }

   ShowWindow(hWndMain, nCmdShow);
   UpdateWindow(hWndMain);
   
   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

HWND editNote;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnSize(HWND hWnd, UINT state, int cx, int cy) {
	SetWindowPos(editNote, NULL, 0, 0, cx  , cy , NULL);
}



BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {


	HFONT hFont = CreateFont(21, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0,L"Times New Roman"); //lấy font Times New Roman
	if (!hFont) { //nếu ko lấy được font thì lấy font hệ thống Lấy font hệ thống
		LOGFONT lf;
		GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
		HFONT hFont = CreateFont(21, 0,
			lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
			lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
			lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
			lf.lfPitchAndFamily, lf.lfFaceName);
	}


	editNote = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | ES_MULTILINE | WS_HSCROLL | WS_VSCROLL
		, 0, 0, DEFAULT_WIDTH - VSCROLL_WIDTH, DEFAULT_HEIGHT - HSCROLL_WIDTH, hWnd, (HMENU)ID_EDIT_NOTE, hInst, NULL);
	SendMessage(editNote, WM_SETFONT, WPARAM(hFont), TRUE);
	SetFocus(editNote);
	return true;
}

BOOL IsSaved = TRUE; //detect if file is saved
std::wstring path = L"";//path to file

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	int size=0;
	WCHAR* buffer = NULL;
	int startPos=0, endPos=0;
	std::wstring filename=L"";
	int choose = 0;


	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case ID_FILE_OPENFILE:
		choose=SaveOldFile(hWnd);
		if (choose != IDCANCEL) {
			filename = OpenFileDialog(hWnd);
			if (filename != L"") {
				ReadFromFile(filename);
				IsSaved = TRUE;
				path = filename;
			}
		}
		break;
	case ID_FILE_NEWFILE:
		choose = SaveOldFile(hWnd);
		if (choose != IDCANCEL) {
			SetWindowText(editNote, L"");
			IsSaved = TRUE;
			path = L"";
		}
		break;
	case ID_FILE_SAVE:
		if (path == L"") {
			filename = SaveFileDialog(hWnd);
			if (filename != L"") {
				WriteToFile(filename);
				path = filename;
			}
		}
		else {
			WriteToFile(path);
		}
		IsSaved = TRUE;

		break;
	case ID_FILE_SAVEAS:
		filename = SaveFileDialog(hWnd);
		if (filename != L"") {
			WriteToFile(filename);
			IsSaved = TRUE;
			path = filename;
		}
		break;
	case ID_EDIT_UNDO:
		if (SendMessage(editNote, EM_CANUNDO, 0, 0))
			SendMessage(editNote, WM_UNDO, 0, 0);
		break;
	case ID_EDIT_CUT:
		SendMessage(editNote, WM_CUT,0, 0);
		break;
	case ID_EDIT_COPY:
		SendMessage(editNote, WM_COPY, 0, 0);
		break;
	case ID_EDIT_PASTE:
		SendMessage(editNote, WM_PASTE, 0, 0);
		break;
	case ID_EDIT_DELETE:
		SendMessage(editNote, WM_CLEAR, 0, 0);

		break;
	case ID_EDIT_SELECTALL:
		SendMessage(editNote, EM_SETSEL, 0, -1);
		break;
	case ID_EDIT_NOTE:
		if (codeNotify == EN_CHANGE) {
			IsSaved = FALSE;
		}
		break;
	case IDM_FILE_SAVE:
		MessageBox(0, 0, 0, 0);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
	//set title
	std::wstring title = L"";
	if (path != L"") {
		if (!IsSaved) 
			title += L" [Unsaved] : ";
		else
			title += L" [Saved] : ";
		title += path;
	
	}
	else {
		title += L"[Unsaved] : United";
	}
	title += L" - Notepad";

	SetWindowText(hWnd, title.c_str());
	if (!buffer)
		delete[] buffer;
}


void ReadFromFile(std::wstring file) {

	std::locale loc;


	std::wifstream savedfile;
	std::string encode_type = DetectFileEncoding(file);
	if (encode_type == "UTF-16LE" || encode_type == "UTF-16BE") {
		std::locale loc_tmp(std::locale(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>); //read utf-16 file
		loc = loc_tmp;
	}
	else {
		std::locale loc_tmp(std::locale(), new std::codecvt_utf8<wchar_t>); //read utf-8 and ansi file
		loc = loc_tmp;
	}
	savedfile.imbue(loc);
	savedfile.open(file);
	std::wstring wstr = L"";
	std::wstring line = L"";

	while (std::getline(savedfile,line)) {
		wstr += line + (wchar_t)13 + (wchar_t)10; //CR/LF : new line
	}
	wstr.pop_back();
	wstr.pop_back(); //omit CR/LF on last line


	SetWindowText(editNote, wstr.c_str());
	savedfile.close();
}

void WriteToFile(std::wstring file) {
	int size = 0;
	WCHAR* buffer = NULL;
	size = GetWindowTextLength(editNote);
	buffer = new WCHAR[size + 1];
	GetWindowText(editNote, buffer, size + 1);
	std::locale loc(std::locale(), new std::codecvt_utf8<wchar_t>);// wirte file as uft8 mode
	//std::locale loc_tmp(std::locale(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>);
	std::wofstream savefile;
	savefile.open(file);
	savefile.imbue(loc);
	std::wstring wstr(buffer);
//	savefile << (wchar_t)0xEFBBBF; // BOM
	savefile << wstr;

	delete[] buffer;
	savefile.close();
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc;
	hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code here...
	EndPaint(hWnd, &ps);
}

void OnClose(HWND hWnd) {
	int choose = SaveOldFile(hWnd);
	if (choose == IDCANCEL) {
		return;
	}
	DestroyWindow(hWnd);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);

}

std::wstring OpenFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"txt";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST  ;
	
	if (GetOpenFileName(&ofn)==TRUE)
	{
		std::wstring wstr(szFileName);
		return wstr;
	}
	return L"";
}

std::wstring SaveFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"Text Files (*.txt)\0*.txt\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"txt";
	ofn.Flags = OFN_PATHMUSTEXIST  |OFN_NOTESTFILECREATE |OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&ofn) == TRUE)
	{
		std::wstring wstr(szFileName);
		return wstr;
	}

	return L"";
}

std::string DetectFileEncoding(std::wstring filename) {
	std::wifstream file(filename, std::ios::binary);
	wchar_t bom[3];
	file.read(bom, 3);
	file.close();
	if (bom[0] == 0xFF && bom[1] == 0xFE) {
		return "UTF-16LE";
	}
	if (bom[0] == 0xFE && bom[1] == 0xFF) {
		return "UTF-16BE";
	}
	if (bom[0] == 0xEF && bom[1] == 0xBB && bom[1] == 0xBF) {
		return "UTF-8";
	}
	return "ANSI";
}

int SaveOldFile(HWND hWnd) {
	std::wstring filename = L"";
	if (!IsSaved) {
		std::wstring message = L"Do You Want To Save The Changes To " + path;
		int choose = MessageBox(hWnd, message.c_str(), L"Notepad", MB_YESNOCANCEL | MB_ICONWARNING);
		if (choose == IDYES) {
			if (path == L"") {
				filename = SaveFileDialog(hWnd);
				if (filename != L"") {
					WriteToFile(filename);
				}
			}
			else {
				WriteToFile(path);
			}
		}
		else{
			return choose;
		}
	
	}
	return -2;
}