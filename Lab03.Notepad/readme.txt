﻿- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com
-Các Chức năng đã làm:
	-Luồng sự kiện chính
		+ Tạo ra giao diện có một edit control để nhập liệu.
		+ Lưu lại dưới dạng file text. Có thể chọn file để lưu.
		+ Mở file text. . Có thể chọn file để mở.
		+Hỗ trợ 3 thao tác Cut - Copy - Paste.
	-Luồng sự kiện Phụ:
		+ Lưu file dưới định dạng UTF-8.
		+ Đọc các file có định dạng ANSI, UTF-8, UTF-16LE ,UTF-16BE.
		+ Hỏi người dùng có lưu file khi thoát hoặc mở file mới hay không.

- Link bitbucket: 
	+ https://bitbucket.org/thaibao1997/windev/src
	+ git clone https://thaibao1997@bitbucket.org/thaibao1997/windev.git