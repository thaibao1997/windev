#pragma once
#include "Shape.h"
class Line :
	public Shape
{
public:
	Line();
	~Line();
	void Draw(HDC hdc, POINT lefttop, POINT rightbottom);
	void ReDraw(HDC hdc);
	Shape* Create();
};

