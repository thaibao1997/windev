#pragma once
#include"Shape.h"

class Rectangle_ : public Shape
{
public:
	Rectangle_();
	~Rectangle_();
	void Draw(HDC hdc, POINT lefttop, POINT rightbottom);
	void ReDraw(HDC hdc);
	Shape* Create();
};

