﻿// 1512026Lab06.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <Unknwn.h> 
#include "1512026Lab06.h"
#include<windowsx.h>
#include"StatusBar.h"
#include"DrawShape.h"
#include<fstream>
#include <Commdlg.h>
#include<string>
using namespace std;
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")


#define MAX_LOADSTRING 100
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
POINT gLeftTop, gRightBottom;
DrawShape gDrawShape;
StatusBar* gStatusBar;
HBITMAP gOpenImage;
int currentCheckedMenu;//id menu dang được check
BOOL gIsSaved = TRUE;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags);
void OnRButtonUp(HWND hwnd, int x, int y, UINT flags);
void OnClose(HWND hwnd);

void CheckMenu(HMENU hMenu, int ID);
void UncheckMenu(HMENU hMenu, int ID);
void UnCheckedAllMenu(HMENU hMenu);
void SaveImage(HWND hwnd,wstring path);
void OpenImage(HWND hwnd, wstring path);

std::wstring SaveFileDialog(HWND hWnd);
std::wstring OpenFileDialog(HWND hWnd);



int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512026LAB06, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512026LAB06));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512026LAB06);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 800, 600, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);
		HANDLE_MSG(hWnd, WM_KEYDOWN, OnKeyDown);
		HANDLE_MSG(hWnd, WM_KEYUP, OnKeyUp);
		HANDLE_MSG(hWnd, WM_RBUTTONUP, OnRButtonUp);
		HANDLE_MSG(hWnd, WM_CLOSE, OnClose);
	case WM_ERASEBKGND:
		return 1;

	case	WM_MOUSELEAVE:
		MessageBox(0, 0, 0, 0);
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void OnSize(HWND hWnd, UINT state, int cx, int cy)
{
	gStatusBar->ChangeSize();
}
 
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	gStatusBar = new StatusBar(hWnd, hInst);
	int SBsize[] = { 100, 250 };
	gStatusBar->SetParts(3, SBsize);

	currentCheckedMenu = ID_SHAPE_LINE;
	HMENU hMenu = GetMenu(hWnd);
	CheckMenu(hMenu, currentCheckedMenu);
	gStatusBar->SetText(1, L"Line");
	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	HMENU hMenu = GetMenu(hWnd);
	wstring path;
	switch (id)
	{
	case ID_SHAPE_LINE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_LINE);
		currentCheckedMenu = ID_SHAPE_LINE;
		gDrawShape.SetShapeType(LINE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Line");

		break;
	case ID_SHAPE_ELLIPSE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_ELLIPSE);
		currentCheckedMenu = ID_SHAPE_ELLIPSE;
		gDrawShape.SetShapeType(ELLIPSE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Ellipse");
		break; 
	case ID_SHAPE_RECTANGLE:
		UncheckMenu(hMenu, currentCheckedMenu);
		CheckMenu(hMenu, ID_SHAPE_RECTANGLE);
		currentCheckedMenu = ID_SHAPE_RECTANGLE;
		gDrawShape.SetShapeType(RECTANGLE_PROTOTYPE_INDEX);
		gStatusBar->SetText(1, L"Rectangle");
		break;
	case IDM_ABOUT:
		//OpenImage(hWnd,L"a.bmp");
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case ID_FILE_SAVE:
		path=SaveFileDialog(hWnd);
		if (path != L"") {
			SaveImage(hWnd, path);
			gIsSaved = TRUE;
		}
		break;
	case ID_FILE_OPEN:
		if (gIsSaved == FALSE) {
			std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
			int choose = MessageBox(hWnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
			if (choose == IDYES) {
				path = SaveFileDialog(hWnd);
				if (path != L"") {
					SaveImage(hWnd, path);
				}
			}else if (choose == IDCANCEL) {
				break;
			}
		}
		path = OpenFileDialog(hWnd);
		if (path != L"") {
			DeleteObject(gOpenImage);
			OpenImage(hWnd, path);
			gDrawShape.Clear();
			gIsSaved = TRUE;
		}		
		break;
	case ID_FILE_NEW:
		if (gIsSaved == FALSE) {
			std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
			int choose = MessageBox(hWnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
			if (choose == IDYES) {
				path = SaveFileDialog(hWnd);
				if (path != L"") {
					SaveImage(hWnd, path);
				}
			}
			else if (choose == IDCANCEL) {
				break;
			}
		}
		path = L"";
		DeleteObject(gOpenImage);
		gOpenImage = NULL;
		gDrawShape.Clear();
		gIsSaved = TRUE;
		gDrawShape.CallReDraw(hWnd);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	RECT rect;
	HDC          hdcMem;

	HBITMAP      hbmMem;
	HANDLE       hOld;
	int width, lenght;
	HDC hdc = BeginPaint(hWnd, &ps);

	GetClientRect(hWnd, &rect);
	width = rect.right - rect.left;
	lenght = rect.bottom - rect.top - 22; //22 for status bar
	// Create an off-screen DC for double-buffering
	hdcMem = CreateCompatibleDC(hdc);
	hbmMem = CreateCompatibleBitmap(hdc, width, lenght);
	hOld = SelectObject(hdcMem, hbmMem);
	SelectObject(hdcMem, GetStockObject(NULL_BRUSH));
	FillRect(hdcMem, &rect, (HBRUSH)(COLOR_WINDOW + 1));//tô trắng vùng


	if (gOpenImage != NULL) {
		HDC          hdcMem2;
		hdcMem2 = CreateCompatibleDC(NULL);
		SelectObject(hdcMem2, gOpenImage);
		BitBlt(hdcMem, 0, 0, width, lenght, hdcMem2, 0, 0, SRCCOPY);
		DeleteDC(hdcMem2);

	}

	if (gRightBottom.x < rect.left)
		gRightBottom.x = rect.left;
	if (gRightBottom.y < rect.top)
		gRightBottom.y = rect.top;
	if (gRightBottom.x > width)
		gRightBottom.x = width;
	if (gRightBottom.y > lenght)
		gRightBottom.y = lenght;

	gRightBottom = gDrawShape.DrawPreview(hdcMem, gLeftTop, gRightBottom);
	gDrawShape.ReDrawAll(hdcMem);


	BitBlt(hdc, 0, 0, width, lenght, hdcMem, 0, 0, SRCCOPY);


	SelectObject(hdcMem, hOld);
	DeleteObject(hbmMem);
	DeleteDC(hdcMem);

	EndPaint(hWnd, &ps);

}

void OnDestroy(HWND hWnd)
{
	delete gStatusBar;
	DeleteObject(gOpenImage);
	PostQuitMessage(0);

}

void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags)
{
	gRightBottom.x = x;
	gRightBottom.y = y;
	if (gDrawShape.IsDrawingPreview()) {
		int ctrl = GetKeyState(VK_LSHIFT);
		if (ctrl & 0x8000 ) { // 1: DOWN, 0: UP
			gDrawShape.SetIsSpecialShape(TRUE);
		}
		else {
			gDrawShape.SetIsSpecialShape(FALSE);
		}
		gDrawShape.CallReDraw(hWnd);
	}
	wchar_t buffer[64];
	wsprintf(buffer,L"x:%d,y:%d", x, y);
	gStatusBar->SetText(0, buffer);

}

void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags)
{
	gLeftTop.x = x;
	gLeftTop.y = y;
	gDrawShape.SetIsDrawingPreview(TRUE);
	gIsSaved = FALSE;
	SetCapture(hwnd);
}

void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags)
{
	gDrawShape.SavePreview(gLeftTop, gRightBottom);
	gDrawShape.SetIsDrawingPreview(FALSE);
	gDrawShape.CallReDraw(hwnd);
	gDrawShape.SetIsSpecialShape(FALSE);
	ReleaseCapture();

}

void OnKeyDown(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	int ctrl = GetKeyState(VK_LSHIFT);
	if (ctrl & 0x8000) { // 1: DOWN, 0: UP
		gDrawShape.SetIsSpecialShape(TRUE);
		if (currentCheckedMenu == ID_SHAPE_RECTANGLE) {
			gStatusBar->SetText(1, L"Square");
		}
		if (currentCheckedMenu == ID_SHAPE_ELLIPSE) {
			gStatusBar->SetText(1, L"Circle");
		}
	}
	
	gDrawShape.CallReDraw(hwnd);
}

void OnKeyUp(HWND hwnd, UINT vk, BOOL fDown, int cRepeat, UINT flags)
{
	int ctrl = GetKeyState(VK_LSHIFT);
	if (!(ctrl & 0x8000)) { // 1: DOWN, 0: UP
		gDrawShape.SetIsSpecialShape(FALSE);
		if (currentCheckedMenu == ID_SHAPE_RECTANGLE) {
			gStatusBar->SetText(1, L"Rectangle");
		}
		if (currentCheckedMenu == ID_SHAPE_ELLIPSE) {
			gStatusBar->SetText(1, L"Ellipse");
		}
	}
	gDrawShape.CallReDraw(hwnd);

}

void OnRButtonUp(HWND hwnd, int x, int y, UINT flags)
{
	//MessageBox(0, 0, 0, 0);
	POINT p;
	p.x = x; p.y = y;
	HMENU hMenuPopup = CreatePopupMenu();
	InsertMenu(hMenuPopup, 0,  MF_UNCHECKED | MF_STRING, ID_SHAPE_LINE, L"Line");
	InsertMenu(hMenuPopup, 0, MF_UNCHECKED | MF_STRING, ID_SHAPE_ELLIPSE, L"Ellipse");
	InsertMenu(hMenuPopup, 0,  MF_UNCHECKED | MF_STRING, ID_SHAPE_RECTANGLE, L"Rectangle");

	CheckMenu(hMenuPopup, currentCheckedMenu);
	ClientToScreen(hwnd, &p);
	TrackPopupMenu(hMenuPopup, TPM_TOPALIGN | TPM_LEFTALIGN, p.x, p.y, 0, hwnd, NULL);
}

void OnClose(HWND hwnd)
{
	if (gIsSaved == FALSE) {
		std::wstring message = L"This Image Have Been Modified. Do You Want To Save The Change?";
		int choose = MessageBox(hwnd, message.c_str(), L"Iamge", MB_YESNOCANCEL | MB_ICONWARNING);
		if (choose == IDYES) {
			std::wstring path = SaveFileDialog(hwnd);
			if (path != L"") {
				SaveImage(hwnd, path);
			}
		}else if (choose == IDCANCEL) {
			return;
		}
	}
	DestroyWindow(hwnd);
}


void CheckMenu(HMENU hMenu, int ID)
{
	CheckMenuItem(hMenu, ID, MF_CHECKED);
}

void UncheckMenu(HMENU hMenu, int ID)
{
	CheckMenuItem(hMenu, ID, MF_UNCHECKED);
}

void UnCheckedAllMenu(HMENU hMenu)
{
	for (int i = ID_SHAPE_LINE; i <= ID_SHAPE_LINE + gDrawShape.GetNumOfShapes(); i++) {
		CheckMenuItem(hMenu, i, MF_UNCHECKED);
	}
}

void SaveImage(HWND hwnd,wstring path)
{
	HDC hdc = GetDC(hwnd);
	HDC          hdcMem;
	HBITMAP      hbmMem;
	HANDLE       hOld;
	RECT rect;
	int width, lenght;

	GetClientRect(hwnd, &rect);
	width = rect.right - rect.left;
	lenght = rect.bottom - rect.top -22;//22 for status bar
	


	ofstream file(path, ios::binary);
	if (!file) return;

	BITMAPFILEHEADER fileHeader;       // bitmap file-header  
	BITMAPINFOHEADER infoHeader;     // bitmap info-header  

	fileHeader.bfType = 0x4d42; //"BM"
	fileHeader.bfSize = 0;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

	infoHeader.biSize = sizeof(infoHeader);
	infoHeader.biWidth = width;
	infoHeader.biHeight = lenght;
	infoHeader.biPlanes = 1;
	infoHeader.biBitCount = 16;
	infoHeader.biCompression = BI_RGB;
	infoHeader.biSizeImage = 0;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biClrImportant = 0;


	BITMAPINFO info;
	info.bmiHeader = infoHeader;

	hdcMem = CreateCompatibleDC(hdc);


	char* memory = 0;
	HBITMAP bitmap = CreateDIBSection(hdc, &info, DIB_RGB_COLORS, (void**)&memory, 0, 0);

	hOld = SelectObject(hdcMem, bitmap);
	BitBlt(hdcMem, 0, 0, width, lenght, hdc, 0, 0, SRCCOPY);
	int bytes = (((16 * width + 31) & (~31)) / 8)*lenght;
	file.write((char*)&fileHeader, sizeof(fileHeader));
	file.write((char*)&infoHeader, sizeof(infoHeader));
	file.write(memory, bytes);

	SelectObject(hdcMem, hOld);
	DeleteDC(hdcMem);
	DeleteObject(bitmap);
	ReleaseDC(hwnd, hdc);
	file.close();
}

void OpenImage(HWND hwnd, wstring path)
{
	gOpenImage = (HBITMAP)LoadImage(NULL, path.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
}

void SaveBeforeExit()
{
}

void OpenImage(HWND hwnd,HDC hdc, wstring path)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	int width = rect.right - rect.left;
	int lenght = rect.bottom - rect.top - 22; //22 for status bar
	HBITMAP hbitmap = (HBITMAP)LoadImage(NULL, path.c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if (hbitmap != NULL) {
		HDC          hdcMem2;
		hdcMem2 = CreateCompatibleDC(hdc);
		SelectObject(hdcMem2, hbitmap);
		BitBlt(hdc, 0, 0, width, lenght, hdcMem2, 0, 0, SRCCOPY);
		DeleteDC(hdcMem2);

	}
}


std::wstring SaveFileDialog(HWND hWnd) {
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"BMP files(*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"bmp";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_NOTESTFILECREATE | OFN_OVERWRITEPROMPT;

	if (GetSaveFileName(&ofn) == TRUE)
	{
		std::wstring wstr(szFileName);
		return wstr;
	}

	return L"";
}

std::wstring OpenFileDialog(HWND hWnd)
{
	OPENFILENAME ofn;
	const int BUFFER_SIZE = 260;
	wchar_t szFileName[MAX_PATH] = L"";

	ZeroMemory(&ofn, sizeof(ofn));

	ofn.lStructSize = sizeof(ofn); // SEE NOTE BELOW
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = BUFFER_SIZE;
	ofn.lpstrFilter = L"BMP files(*.bmp)\0*.bmp\0All Files (*.*)\0*.*\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFileName;
	ofn.lpstrInitialDir = NULL;
	ofn.lpstrDefExt = L"bmp";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	if (GetOpenFileName(&ofn) == TRUE)
	{
		std::wstring wstr(szFileName);
		return wstr;
	}
	return L"";
}
