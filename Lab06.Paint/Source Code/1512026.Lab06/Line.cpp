#include "stdafx.h"
#include "Line.h"


Line::Line()
{
}


Line::~Line()
{
}


void Line::Draw(HDC hdc, POINT lefttop, POINT rightbottom)
{
	this->SetPoints(lefttop, rightbottom);
	MoveToEx(hdc, lefttop.x, lefttop.y, NULL);
	LineTo(hdc, rightbottom.x, rightbottom.y);
}

void Line::ReDraw(HDC hdc)
{
	Draw(hdc, leftTop, rightBottom);
}

Shape * Line::Create()
{
	Line* New = new Line();
	New->SetPoints(this->GetLeftTop(), this->GetRightBottom());
	return New;
}

