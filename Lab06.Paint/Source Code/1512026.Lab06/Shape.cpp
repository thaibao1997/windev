#include "stdafx.h"
#include "Shape.h"

Shape::Shape()
{
}

Shape::Shape(POINT lefttop, POINT rightbottom)
{
	this->leftTop = lefttop;
	this->rightBottom = rightbottom;
}

void Shape::SetPoints(POINT lefttop, POINT rightbottom)
{
	this->leftTop = lefttop;
	this->rightBottom = rightbottom;
}

POINT Shape::GetLeftTop()
{
	return this->leftTop;
}

POINT Shape::GetRightBottom()
{
	return this->rightBottom;
}
