#pragma once
#include <windows.h>


class Shape
{
protected:
	POINT leftTop;
	POINT rightBottom;
public:
	Shape();
	Shape(POINT lefttop, POINT rightbottom);
	virtual void Draw(HDC hdc, POINT lefttop, POINT rightbottom) = 0;
	virtual void ReDraw(HDC hdc) = 0;
	virtual Shape* Create()=0;
	//virtual Shape* Create(POINT lefttop, POINT rightbottom) = 0;
	void SetPoints(POINT lefttop, POINT rightbottom);
	POINT GetLeftTop();
	POINT GetRightBottom();
};

