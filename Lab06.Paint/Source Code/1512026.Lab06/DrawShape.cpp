#include "stdafx.h"
#include "DrawShape.h"



DrawShape::DrawShape()
{
	prototypes.resize(3);
	prototypes[LINE_PROTOTYPE_INDEX] = new Line();
	prototypes[ELLIPSE_PROTOTYPE_INDEX] = new Ellipse_();
	prototypes[RECTANGLE_PROTOTYPE_INDEX] = new Rectangle_();
	isDrawingPreview = false;
	isSpecialShape = false;
	shapeType = LINE_PROTOTYPE_INDEX;
}


DrawShape::~DrawShape()
{
	for (int i = 0; i < shapes.size(); i++)
		delete shapes[i];
	for (int i = 0; i < prototypes.size(); i++)
		delete prototypes[i];
}

/*void DrawShape::ToggleDrawingPreview()
{
	isDrawingPreview = !isDrawingPreview;
}*/


void DrawShape::SetIsSpecialShape(BOOL val)
{
	isSpecialShape = val;
}

void DrawShape::SetIsDrawingPreview(BOOL val)
{
	isDrawingPreview = val;
}

BOOL DrawShape::IsDrawingPreview()
{
	return this->isDrawingPreview;
}

void DrawShape::ReDrawAll(HDC hdc)
{
	for (int i = 0; i < shapes.size(); i++)
		shapes[i]->ReDraw(hdc);
}

void DrawShape::AddShape(Shape * shape)
{
	this->shapes.push_back(shape);
}

POINT DrawShape::DrawPreview(HDC hdc, POINT leftTop, POINT rightBottom){
	if (this->isDrawingPreview == TRUE) {
		if (isSpecialShape && shapeType != LINE_PROTOTYPE_INDEX) {
			int dx = abs(leftTop.x - rightBottom.x);
			int dy= abs(leftTop.y - rightBottom.y);
			if(rightBottom.x >leftTop.x)
				rightBottom.x = leftTop.x + min(dx,dy);
			else if (rightBottom.x <leftTop.x) 
				rightBottom.x = leftTop.x - min(dx, dy);
			if (rightBottom.y >leftTop.y)
				rightBottom.y = leftTop.y + min(dx, dy);
			else if (rightBottom.y <leftTop.y)
				rightBottom.y = leftTop.y - min(dx, dy);
		}
		prototypes[shapeType]->Draw(hdc, leftTop, rightBottom);
		return rightBottom;
	}
}

void DrawShape::SavePreview(POINT leftTop, POINT rightBottom)
{
	if (this->isDrawingPreview == TRUE)
		this->shapes.push_back(prototypes[shapeType]->Create());
}

void DrawShape::CallReDraw(HWND hwnd)
{
	RECT rect;
	GetClientRect(hwnd, &rect);
	InvalidateRect(hwnd, &rect, TRUE);
}

void DrawShape::SetShapeType(int type)
{
	if (type < LINE_PROTOTYPE_INDEX || type>RECTANGLE_PROTOTYPE_INDEX) return;
	shapeType = type;

}

int DrawShape::GetShapeType()
{
	return this->shapeType;
}

int DrawShape::GetNumOfShapes()
{
	return prototypes.size();
}

void DrawShape::Clear()
{
	this->shapes.clear();
}
