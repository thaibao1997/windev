#pragma once
#include"Shape.h"

class Ellipse_ : public Shape
{
public:
	Ellipse_();
	~Ellipse_();
	void Draw(HDC hdc, POINT lefttop, POINT rightbottom);
	void ReDraw(HDC hdc);
	Shape* Create();
};

