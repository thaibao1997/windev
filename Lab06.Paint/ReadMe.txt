﻿- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com
-Các Chức năng đã làm:
	-Luồng sự kiện chính
		+ Vẽ 5 5 loại hình cơ bản: đường thẳng, chữ nhật, vuông, ellipse, tròn.
		+ Tạo Menu cho phép chọn loại hình cần vẽ.
		+ Nhấn shift để vẽ hình tròn nếu đang chọn hình ellipse và hình vuông nếu đang chọn hình chữ nhật
		+ Lưu lại hình đang vẽ dưới dạng file bmp.
		+ Mở file bmp để vẽ
	-Luồng sự kiện Phụ:
		+ Khi người dùng kéo con trỏ ra ngoài màn hình vẽ sẽ tự động giới hạn lại hình.
		+ Khi người dùng thoát hay mở file bmp mà chưa lưu sẽ hỏi người dùng có muốn lưu hay không
-Link Repo: https://bitbucket.org/thaibao1997/windev/
-Link Video Demo: https://youtu.be/wGY01unZqok
