﻿#pragma once
#include <Commctrl.h>
#include <string>
#include "1512026Lab04Explorer.h"
#include <vector>

class ToolBar
{

private:
	HWND addressHandle; //handle thanh địa chỉ
	HWND backButtonHandle;//handle nút back
	HWND forwardButtonHandle;//handle nút forward
	HWND goButtonHandle;//handle nút go
	std::vector<std::wstring> historyAddress;//lưu các địa chỉ trước dùng cho back button
	std::vector<std::wstring> forwardAddress;//lưu các địa chỉ  dùng cho forward button
	std::wstring currentAddress; //address hiện tại trên address bar
public:
	ToolBar(HWND parentHandle, HINSTANCE hInst, HFONT hfont);
	HWND GetAddressBarHandle();
	HWND GetGoButtonHandle();

	void GetAddressBarText(wchar_t* buffer);
	void SetAddressBarText(wchar_t* buffer);

	void StoreCurrentAddress(BOOL onAddressBar=TRUE);//onAddressBar = TRUE -> lưu địa chỉ trên thanh address, FALSE lưu địa chỉ trong this->currentAddress
	void StoreNewAddress(BOOL clearForward=TRUE);//lưu địa chỉ mới clearForward==TRUE xoá forwardAddress
	void StroreForwardAddress();

	void GetBackAddress(wchar_t* buffer);
	void GetForwardAddress(wchar_t* buffer);
	void RestoreAddress();//hồi phục địa chỉ khi nhập ko đúng đường dẫn


	std::wstring GetCurrentAddress();//hồi trả về địa chỉ address bar trước khi nhập trong trg hợp nhập sai



	void SetEnableBackButton();
	void SetEnableForwardButton();

	~ToolBar();
};

