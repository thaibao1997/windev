﻿#pragma once
#include<vector>
#include<string>
#include"ListView.h"
#include"TreeView.h"
#include"StatusBar.h"
#//Dùng để sử dụng hàm StrCpy, StrNCat
#include <shlwapi.h>
#include<iostream>
#include<Shellapi.h>
#include <string>     // std::wstring, std::to_wstring
#include <algorithm>
#include<Shlobj.h>
#pragma comment(lib, "shlwapi.lib")

class Drive
{
private:

public:
	Drive();
	~Drive();
	static std::vector<wchar_t*> bufferAddress;//lưu thông tin địa chỉ đã cấp phát
	static wchar_t* drivesBuffer;//buffer để  lưu thông tin các ổ cứng ổ cứng;
	static std::vector<wchar_t*> GetDrives();


	static void StartUpInital(ListView* lw, TreeView* tw,StatusBar* sb,HINSTANCE hInst);//hiện thị khi chạy chương trình


	static int ViewDriveList(ListView* lw);
	static void LoadDrive(TreeView* tw);

	static void LoadExpanding(TreeView* tw, TVITEM parent);

	static void ShowDriveColumn(ListView* lw);//hiển thị list view cho các ổ cứng
	static void ShowFileColumn(ListView* lw);//hiển thị list view cho các tập tin

	static int ViewFileList(ListView* lw, wchar_t* path);
	static BOOL TryOpenFile(wchar_t* path);
	
	static void SizeConverter(wchar_t* buffer, UINT64 size);//chuyển đổi size bytes sang KB,MB,GB,...
	static BOOL isEmptyDir(wchar_t* path);//kiểm tra xem 1 folder có rỗng hay ko
	static BOOL HasSubDirectory(wchar_t* path);//kiểm tra xem 1 folder có chứa folder con hay không
	static BOOL IsExistPath(wchar_t* path);//kiểm tra đường dẫn có tồn tại hay không
	static BOOL IsFolderPath(wchar_t* path);//kiểm tra đường dẫn có dẫn tới 1 folder không

	static void _GetDiskTotalSize(wchar_t* buffer, wchar_t* drivename);//lấy tổng dung lượnng của ổ cứng
	static void _GetDiskFreeSpace(wchar_t* buffer, wchar_t* drivename);//lấy dung lượnng trống của ổ cứng
	static void _GetDiskLabel(wchar_t* buffer, wchar_t* drivename);

	static UINT64 GetFileSize(const WIN32_FIND_DATA& file);
	static void GetFileSize(wchar_t* buffer, WIN32_FIND_DATA file);
	static UINT64 GetFileSize(wchar_t* path);


	static void GetLastWriteTime(wchar_t* buffer, const WIN32_FIND_DATA& file);
	static void GetFileExtension(wchar_t* buffer, wchar_t*  filename_);

	static void SBViewNoItem(int no,StatusBar* sb);
	static void SBViewNoSelectedItem(int no, StatusBar* sb);
	static void SBViewFilesSize(UINT64 no, StatusBar* sb);


	static void FreeMemory();//giải phóng vùng nhớ lưu trong bufferAddress
};

