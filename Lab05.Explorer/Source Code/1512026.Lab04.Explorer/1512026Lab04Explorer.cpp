﻿// 1512026Lab04Explorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512026Lab04Explorer.h"
#include "windowsx.h"
#include <commctrl.h>
#include "ListView.h"
#include "TreeView.h"
#include <string>
#include"Drive.h"
#include <shlwapi.h>
#include "ToolBar.h"
#include"StatusBar.h"


#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100
#define	LEFT_MINIMUM_WIDTH		50
#define	RIGHT_MINIMUM_WIDTH		100
#define	CONFIG_FILE L"config.ini"
#define	DEFAULT_WINDOW_WIDTH L"900"
#define	DEFAULT_WINDOW_HEIGHT L"600"
#define	DEFAULT_TREEVIEW_WIDTH L"200"

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND editlDir; //thanh địa chỉ
HWND buttonGo; //nút go
ToolBar* g_ToolBar;
ListView* g_ListView; 
TreeView* g_TreeView;
StatusBar* gStatusBar;
int gItemsCount = 0;

HFONT hfont;

int WindowWidth = 1000;
int WindowHeight = 700;

const int viewY = 40;
const int SplitBarWidth = 5;
int viewHeight  = WindowHeight - viewY - 25;;



int TreeViewX = 0;
int TreeViewWidth = 200;

int ListViewX ;
int ListViewWidth ;

double ListViewWidthPercent = 0.25;

int AddressBarWidth = 760;



// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void OnSize(HWND hWnd, UINT state, int cx, int cy);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hWnd);
void OnDestroy(HWND hWnd);
LRESULT OnNotify(HWND hwnd, int idFrom, NMHDR *pnm);
void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags);
void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags);
void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags);
void LoadConfig();
void SaveConfig();

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	LoadConfig();

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_1512026LAB04EXPLORER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_1512026LAB04EXPLORER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON4));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_1512026LAB04EXPLORER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON4));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{


   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      50, 10, WindowWidth, WindowHeight, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}




//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    switch (message)
    {
		HANDLE_MSG(hWnd, WM_SIZE, OnSize);
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
		HANDLE_MSG(hWnd, WM_NOTIFY, OnNotify);
		HANDLE_MSG(hWnd, WM_MOUSEMOVE, OnMouseMove);
		HANDLE_MSG(hWnd, WM_LBUTTONDOWN, OnLButtonDown);
		HANDLE_MSG(hWnd, WM_LBUTTONUP, OnLButtonUp);

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}



BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct) {


	hfont = CreateFont(18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, L"monospace");

	g_TreeView = new TreeView(TreeViewX, viewY, TreeViewWidth, viewHeight, (HMENU)ID_TREEVIEW, hWnd, hInst);

	g_ListView = new ListView(ListViewX, viewY, ListViewWidth, viewHeight, (HMENU)ID_LISTVIEW, hWnd, hInst);//215

	g_ToolBar = new ToolBar(hWnd, hInst,hfont);
	g_ToolBar->SetEnableBackButton();
	g_ToolBar->SetEnableForwardButton();

	gStatusBar = new StatusBar(hWnd, hInst);
	int sizes[] = { 100,250,-1 };
	gStatusBar->SetParts(3, sizes);

	Drive::StartUpInital(g_ListView, g_TreeView,gStatusBar ,hInst);

	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify) {
	wchar_t buffer[512];
	wchar_t buffer2[512];


	switch (id)
	{
	case ID_GO_BUTTON:
		g_ToolBar->GetAddressBarText(buffer);
		if (!StrCmp(buffer, L"This PC")) {
			gItemsCount=Drive::ViewDriveList(g_ListView);
		}
		else if (!Drive::TryOpenFile(buffer)) {
			int itemsCountTmp= gItemsCount;
			gItemsCount = Drive::ViewFileList(g_ListView, buffer);
			if (!gItemsCount) {
				MessageBox(hWnd, L"Đường Dẫn Không Tồn Tại", L"Lỗi", MB_OK | MB_ICONERROR);
				g_ToolBar->RestoreAddress();
				gItemsCount = itemsCountTmp;
			}else if (StrCmp(g_ToolBar->GetCurrentAddress().c_str(), buffer) != 0) {
					g_ToolBar->StoreCurrentAddress(FALSE);
					g_ToolBar->StoreNewAddress();
			}
		}
		else {
			g_ToolBar->RestoreAddress();
		}
		Drive::SBViewNoItem(gItemsCount, gStatusBar);
		break;
	case ID_BACK_BUTTON:
		g_ToolBar->StroreForwardAddress();
		g_ToolBar->GetBackAddress(buffer);
		if (StrCmp(buffer,L"This PC")==0) {
			gItemsCount =Drive::ViewDriveList(g_ListView);
		}
		else {
			gItemsCount =Drive::ViewFileList(g_ListView, buffer);
		}
		Drive::SBViewNoItem(gItemsCount, gStatusBar);
		g_ToolBar->SetAddressBarText(buffer);
		g_ToolBar->StoreNewAddress(FALSE);
		break;
	case ID_FORWARD_BUTTON:
		g_ToolBar->StoreCurrentAddress();
		g_ToolBar->GetForwardAddress(buffer);
		if (StrCmp(buffer, L"This PC") == 0) {
			gItemsCount = Drive::ViewDriveList(g_ListView);
		}
		else {
			gItemsCount = Drive::ViewFileList(g_ListView, buffer);
		}
		Drive::SBViewNoItem(gItemsCount, gStatusBar);
		g_ToolBar->SetAddressBarText(buffer);
		g_ToolBar->StoreNewAddress(FALSE);
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;

	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
//	g_ToolBar->SetEnableBackButton();
}

LRESULT OnNotify(HWND hWnd, int idFrom, NMHDR *pnm) {
	LPNMTREEVIEW lpnmTree = (LPNMTREEVIEW)pnm;
	int nCurSelIndex;
	LPNMTOOLBAR lpnmToolBar = (LPNMTOOLBAR)pnm;

	HTREEITEM  hti;
	TCHAR buffer[128];
	HTREEITEM hSelectedItem;
	TVITEM item;
	int countSelectedItems=0;
	UINT64 filesSize=0;
	bool countSizes=true;

	switch (pnm->code)
	{
	case TVN_ITEMEXPANDING:
		item = lpnmTree->itemNew;
		Drive::LoadExpanding(g_TreeView, item);
		break;
	case TVN_SELCHANGED:
		item = g_TreeView->GetSelectedItem(buffer);
		g_ToolBar->StoreCurrentAddress();
		if (item.lParam == (LPARAM)L"THIS_PC_ROOT") {
			gItemsCount = Drive::ViewDriveList(g_ListView);
			g_ToolBar->SetAddressBarText(L"This PC");
		}
		else {
			gItemsCount = Drive::ViewFileList(g_ListView, (LPWSTR)item.lParam);
			g_ToolBar->SetAddressBarText((LPWSTR)item.lParam);
		}
		g_ToolBar->StoreNewAddress();
		g_ToolBar->SetAddressBarText((LPWSTR)item.lParam);
		Drive::SBViewNoItem(gItemsCount, gStatusBar);

		break;
	case NM_DBLCLK:
		if (idFrom == ID_LISTVIEW) {
			LVITEM item = g_ListView->GetSelectedItem(buffer);
			if (item.pszText == NULL) break;
			if (!Drive::TryOpenFile((LPWSTR)item.lParam)) {
				g_ToolBar->StoreCurrentAddress();
				gItemsCount = Drive::ViewFileList(g_ListView, (LPWSTR)item.lParam);
				Drive::SBViewNoItem(gItemsCount, gStatusBar);
				g_ToolBar->SetAddressBarText((LPWSTR)item.lParam);
				g_ToolBar->StoreNewAddress();
			}


		}
		break;
	case LVN_ITEMCHANGED:
	
		for (int itemInt = -1; (itemInt = SendMessage(g_ListView->getHandle(), LVM_GETNEXTITEM, itemInt, LVNI_SELECTED)) != -1; )
		{
			countSelectedItems++;
			LVITEM lvi = g_ListView->GetItem(itemInt, buffer);
			if ( Drive::IsFolderPath((wchar_t*)lvi.lParam)) {
				countSizes = false;
				continue;
			}
			filesSize += Drive::GetFileSize((wchar_t*)lvi.lParam);
			
		}
		if (countSelectedItems) {
			Drive::SBViewNoSelectedItem(countSelectedItems, gStatusBar);
			if (countSizes)
				Drive::SBViewFilesSize(filesSize, gStatusBar);
			else
				gStatusBar->SetText(2, L"");
		}
		else {
			gStatusBar->SetText(1, L"");
			gStatusBar->SetText(2, L"");
		}


		break;
	default:
		break;
	}
	//
	return 0;

}

void OnPaint(HWND hWnd) {

	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd) {
	if (g_ListView != NULL)
		delete g_ListView;
	if (g_TreeView != NULL)
		delete g_TreeView;
	if(g_ToolBar!=NULL)
		delete g_ToolBar;
	Drive::FreeMemory();
	SaveConfig();
	PostQuitMessage(0);

}




void OnMouseMove(HWND hWnd, int x, int y, UINT keyFlags) {
	if ((x >= TreeViewX + TreeViewWidth - 2) && (x <= TreeViewX + TreeViewWidth + 5) && (y >= viewY) && (y <= viewY + viewHeight )){

		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE)));

	}
	if (keyFlags == MK_LBUTTON) {

		if (x < LEFT_MINIMUM_WIDTH) return;
		if (WindowWidth - x < RIGHT_MINIMUM_WIDTH) return;
		MoveWindow(g_TreeView->getHandle(), TreeViewX, viewY, x - TreeViewX, viewHeight, TRUE);
		TreeViewWidth = x - TreeViewX;
		ListViewX = x + SplitBarWidth;
		ListViewWidth = WindowWidth - ListViewX - 10;
		MoveWindow(g_ListView->getHandle(), ListViewX, viewY, ListViewWidth, viewHeight, TRUE);



	}
	//ReleaseCapture();
//	PostMessage(hWnd, WM_SIZE, 0, 0);

}


void OnLButtonUp(HWND hwnd, int x, int y, UINT keyFlags) {
	ReleaseCapture();
}

void OnLButtonDown(HWND hwnd, BOOL fDoubleClick, int x, int y, UINT keyFlags) {
	if ((x >= TreeViewX + TreeViewWidth - 2) && (x <= TreeViewX + TreeViewWidth + 5) && (y >= viewY) && (y <= viewY + viewHeight)) {
		SetCapture(hwnd);
		SetCursor(LoadCursor(NULL, MAKEINTRESOURCE(IDC_SIZEWE)));
	}
}

void OnSize(HWND hWnd, UINT state, int cx, int cy) {
	
	if(WindowWidth!=0)
		ListViewWidthPercent = (TreeViewX + TreeViewWidth)*1.0 / WindowWidth;//giữ nguyên tỉ lệ màn hình giữa treeview và listview


	WindowWidth = cx;
	WindowHeight = cy;

	TreeViewWidth = cx*ListViewWidthPercent - TreeViewX;

	ListViewX = TreeViewX + TreeViewWidth + SplitBarWidth;
	ListViewWidth = WindowWidth - ListViewX ;
	viewHeight = WindowHeight - viewY - 25;


	MoveWindow(g_TreeView->getHandle(), TreeViewX, viewY, TreeViewWidth, viewHeight, TRUE);
	MoveWindow(g_ListView->getHandle(), ListViewX, viewY, ListViewWidth, viewHeight, TRUE);


	AddressBarWidth = cx - 105;
	MoveWindow(g_ToolBar->GetAddressBarHandle(), 70, 10, AddressBarWidth, 25, TRUE);
	MoveWindow(g_ToolBar->GetGoButtonHandle(), 70 + AddressBarWidth, 10, 35, 25, TRUE);
	

	gStatusBar->ChangeSize();
	//SendMessage(Status, WM_SIZE, NULL, NULL);
}

void LoadConfig() {
	WCHAR curPath[256];
	GetCurrentDirectory(256, curPath);
	StrCat(curPath, L"\\");
	StrCat(curPath, CONFIG_FILE);
	wchar_t buffer[256];
	int tmp = 0;

	GetPrivateProfileString(L"App", L"Window_Width", DEFAULT_WINDOW_WIDTH, buffer, 255, curPath);
	tmp = _wtoi(buffer);
	WindowWidth = tmp && tmp>0 ? tmp + 16 : _wtoi(DEFAULT_WINDOW_WIDTH); //cộng 16 vì hàm InitInstance tính luôn kích thước của thanh tiêu đề trong khi WM_SIZE ko tính

	GetPrivateProfileString(L"App", L"Window_Height", DEFAULT_WINDOW_HEIGHT, buffer, 255, curPath);
	tmp = _wtoi(buffer);
	WindowHeight = tmp&& tmp>0 ? tmp + 59 : _wtoi(DEFAULT_WINDOW_WIDTH);//cộng 59 tương tự trên

	GetPrivateProfileString(L"App", L"TreeView_Width", DEFAULT_TREEVIEW_WIDTH, buffer, 255, curPath);
	tmp = _wtoi(buffer);
	TreeViewWidth = tmp && tmp>0 ? tmp + 4 : _wtoi(DEFAULT_TREEVIEW_WIDTH);//cộng 4 tương tự trên

}

void SaveConfig() {
	WCHAR curPath[256];
	GetCurrentDirectory(256, curPath);
	StrCat(curPath, L"\\");
	StrCat(curPath, CONFIG_FILE);
	WritePrivateProfileString(L"App", L"Window_Width", std::to_wstring(WindowWidth).c_str(), curPath);
	WritePrivateProfileString(L"App", L"Window_Height", std::to_wstring(WindowHeight).c_str(), curPath);
	WritePrivateProfileString(L"App", L"TreeView_Width", std::to_wstring(TreeViewWidth).c_str(), curPath);

}