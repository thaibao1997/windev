﻿#pragma once
#include <Commctrl.h>
#include <string>
#include "1512026Lab04Explorer.h"

class ListView
{
private:
	HWND handle;
	int NumberOfColumn=0; //lưu số lượng colummn
	int NumberOfItem=0; //lưu số lượng item
public:
	ListView(int x,int y,int width,int height,HMENU id,HWND parentHandle,HINSTANCE hInst);
	void AddColumn(LPWSTR name,int width);
	int InsertItem(LPWSTR value,int image=0,LPARAM lparam=NULL);
	void SetItemText(LPWSTR value,int nItem, int column);
	int GetNumberOfItem();
	LVITEM GetSelectedItem(TCHAR* buffer);//buffer để lưu text
	LVITEM GetItem(int index, TCHAR * buffer);
	void Clear();


	HWND getHandle();
	~ListView();
};

