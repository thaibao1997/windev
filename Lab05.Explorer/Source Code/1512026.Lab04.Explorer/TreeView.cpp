#include "stdafx.h"
#include "TreeView.h"


TreeView::TreeView(int x, int y, int width, int height, HMENU id, HWND parentHandle, HINSTANCE hInst)
{
	this->handle = CreateWindowEx(WS_EX_CLIENTEDGE
		, WC_TREEVIEW, L"", WS_CHILD | WS_VISIBLE |WS_HSCROLL |TVS_HASBUTTONS | TVS_HASLINES | TVS_LINESATROOT , x, y, width, height, parentHandle, id, hInst, NULL);
	//TreeView_SetExtendedStyle(this->getHandle(), TVS_EX,NULL);
	
	//this->InitImageLists(hInst);
}

HTREEITEM TreeView::InsertRootNode(LPWSTR value, int image, int selectedImage,LPARAM lparam)
{
	TV_INSERTSTRUCT tvInsert;

	tvInsert.hParent = NULL;
	tvInsert.hInsertAfter = TVI_ROOT;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.item.cChildren = TRUE;
	tvInsert.item.iImage = image;
	tvInsert.item.iSelectedImage = selectedImage;
	tvInsert.item.lParam = lparam;
	tvInsert.item.pszText = value;
	return TreeView_InsertItem(this->getHandle(), &tvInsert);
}

HTREEITEM TreeView::InsertNode(LPWSTR value, HTREEITEM parent, int image, int selectedImage, LPARAM lparam, HTREEITEM InsetPos)
{
	TV_INSERTSTRUCT tvInsert;
	tvInsert.hParent = parent;
	tvInsert.hInsertAfter = InsetPos;
	tvInsert.item.mask = TVIF_TEXT | TVIF_IMAGE | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	tvInsert.item.cChildren = TRUE;
	tvInsert.item.iImage = image;
	tvInsert.item.iSelectedImage = selectedImage;
	tvInsert.item.lParam = lparam;
	tvInsert.item.pszText = value;
	return TreeView_InsertItem(this->getHandle(), &tvInsert);
}

HTREEITEM TreeView::InsertDummy(HTREEITEM parent)
{
	TV_INSERTSTRUCT dummy;
	dummy.item.mask = TVIF_TEXT;
	dummy.item.pszText = L"";
	dummy.hParent = parent;
	return TreeView_InsertItem(this->getHandle(), &dummy);

}

void TreeView::DeleteDummy(HTREEITEM parent)
{
	if (!parent) return;
	HTREEITEM dummy=TreeView_GetChild(this->getHandle(), parent);
	
	wchar_t buffer[128];
	TVITEM item;
	item.hItem = dummy;
	item.mask = TVIF_TEXT;
	item.cchTextMax = 128;
	item.pszText = buffer;
	if (TreeView_GetItem(this->getHandle(), &item))
	{

		if (item.pszText == L"" || lstrlen(buffer) < 1) {
			TreeView_DeleteItem(this->getHandle(), dummy);
		}
	}
}

void TreeView::DeleteAllChild(HTREEITEM parent)
{
	if (!parent) return;
	HTREEITEM item;
	while (item = TreeView_GetChild(this->getHandle(), parent)) {
		TreeView_DeleteItem(this->getHandle(), item);
	}


}

TVITEM TreeView::GetSelectedItem(TCHAR* buffer)
{
	HTREEITEM hSelectedItem = TreeView_GetSelection(this->getHandle());
	TVITEM item;
	item.hItem = hSelectedItem;
	item.mask = TVIF_TEXT | TVIF_PARAM;
	item.cchTextMax = 128;
	item.pszText = buffer;
	if (TreeView_GetItem(this->getHandle(), &item))
	{
		return item;
	}
	return TVITEM();
}

HTREEITEM TreeView::GetSelectedItem()
{
	return TreeView_GetSelection(this->getHandle());
}

HWND TreeView::getHandle()
{
	return this->handle;
}




TreeView::~TreeView()
{
}
