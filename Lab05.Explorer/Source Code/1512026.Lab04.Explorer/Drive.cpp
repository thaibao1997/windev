﻿#include "stdafx.h"
#include "Drive.h"




#define MAX_BUFFER_SIZE 2048
#define DRIVE_ICON_INDEX 0
#define FOLDER_ICON_INDEX 1
#define FILE_ICON_INDEX 2
#define PC_ICON_INDEX 3



wchar_t* Drive::drivesBuffer = new wchar_t[MAX_BUFFER_SIZE];
std::vector<wchar_t*> Drive::bufferAddress = std::vector<wchar_t*>(0);


std::vector<wchar_t*> Drive::GetDrives()
{
	wchar_t* buffer = drivesBuffer;
	GetLogicalDriveStrings(255, buffer);
	std::vector<wchar_t*> vec;

	while (buffer && lstrlen(buffer) > 1) {
		vec.push_back(buffer);
		buffer += lstrlen(buffer) + 1;
	}

	return vec;
}

void Drive::StartUpInital(ListView * lw, TreeView * tw, StatusBar* sb ,HINSTANCE hInst)
{
	LPITEMIDLIST pidl;
	SHFILEINFO sfi;
	//pidl = ILCreateFromPath(L"\\");
	SHGetSpecialFolderLocation(NULL, CSIDL_DRIVES,&pidl);
	SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_ICON);
	HTREEITEM root = tw->InsertRootNode(L"This PC", sfi.iIcon, sfi.iIcon, (LPARAM)L"THIS_PC_ROOT");

	Drive::LoadDrive(tw);
	TreeView_Expand(tw->getHandle(), root, TVE_EXPAND);


	lw->Clear();
	lw->AddColumn(L"Tên", 200);  //tạo sẵn các column
	for (int i = 0; i < 3; i++)
		lw->AddColumn(L"",100);
	

	 Drive::ShowDriveColumn(lw);
	int count= ViewDriveList(lw);
	SBViewNoItem(count, sb);

	HIMAGELIST hImgList, hImgListLV;   // Image list for icon view.
	Shell_GetImageLists(&hImgListLV, &hImgList);
	TreeView_SetImageList(tw->getHandle(), hImgList, TVSIL_NORMAL);
	ListView_SetImageList(lw->getHandle(), hImgListLV, LVSIL_SMALL);

}

int Drive::ViewDriveList(ListView * lw)
{
	std::vector<wchar_t*> drives=GetDrives();
	lw->Clear();
	Drive::ShowDriveColumn(lw);
	for (int i = 0; i < drives.size(); i++) {
		LPITEMIDLIST pidl;
		pidl = ILCreateFromPath(drives[i]);
		SHFILEINFO sfi;
		SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_ICON);


		int iItem = lw->InsertItem(drives[i], sfi.iIcon, (LPARAM)drives[i]);
		wchar_t infoBuffer[30];
		_GetDiskLabel(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 1);
		_GetDiskTotalSize(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 2);
		_GetDiskFreeSpace(infoBuffer, drives[i]);
		lw->SetItemText(infoBuffer, iItem, 3);

	}

	return drives.size();
}

void Drive::LoadDrive(TreeView * tw)
{
	HTREEITEM root = TreeView_GetRoot(tw->getHandle());
	std::vector<wchar_t*> drives = GetDrives();
	for (int i = 0; i < drives.size(); i++) {
		LPITEMIDLIST pidl;
		SHFILEINFO sfi;
		pidl = ILCreateFromPath(drives[i]);
		SHGetFileInfo((LPCTSTR)pidl, -1, &sfi, sizeof(sfi), SHGFI_PIDL| SHGFI_DISPLAYNAME | SHGFI_ICON);
		HTREEITEM justInsert = tw->InsertNode(sfi.szDisplayName, root, sfi.iIcon, sfi.iIcon, (LPARAM)drives[i]);
		if (HasSubDirectory(drives[i]))
			tw->InsertDummy(justInsert);
	}
}


int Drive::ViewFileList(ListView * lw, wchar_t * path)
{
	if (!IsExistPath(path)) return 0;
	int count = 0;
	lw->Clear();
	ShowFileColumn(lw);

	wchar_t Path[MAX_BUFFER_SIZE];
	StrCpy(Path, path);


	LPITEMIDLIST pidl;
	pidl = ILCreateFromPath(Path);
	HRESULT hr = S_OK;
	IShellFolder* pFolder = NULL;
	IShellFolder* pDeskTop = NULL;
	IEnumIDList* pEnumIds = NULL;
	SHGetDesktopFolder(&pDeskTop);
	pDeskTop->BindToObject(pidl,NULL ,IID_IShellFolder, (void **)&pFolder);
	//hr=SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void **)&pFolder);

	if (SUCCEEDED(hr))
	{
		LPITEMIDLIST pidlItems = NULL;
		SHFILEINFO sfi;
		WIN32_FIND_DATA fd;
		STRRET str;
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds); //lấy folder
		while ((hr = pEnumIds->Next(1, &pidlItems,NULL )) == S_OK)
		{
			SHGetFileInfo((LPCTSTR)pidlItems, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME |SHGFI_TYPENAME | SHGFI_ICON);
			wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];
			GetFileExtension(buffer, sfi.szDisplayName);
			//SHGetPathFromIDList(pidlItems, buffer);
			pFolder->GetDisplayNameOf(pidlItems, SHGDN_FORPARSING, &str);
			StrCpy(buffer, str.pOleStr);
			/*StrCpy(buffer, path);
			if (path[lstrlen(path) - 1] != '\\') {
				StrCat(buffer, L"\\");
			}
			StrCat(buffer, sfi.szDisplayName);*/

			int  iItem = lw->InsertItem(sfi.szDisplayName, sfi.iIcon, (LPARAM)buffer);
			lw->SetItemText(sfi.szTypeName, lw->GetNumberOfItem() - 1, 2);
			SHGetDataFromIDList(pFolder, pidlItems, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
			wchar_t infoBuffer[30];
			GetLastWriteTime(infoBuffer, fd);
			lw->SetItemText(infoBuffer, iItem, 3);
			count++;

			bufferAddress.push_back(buffer);

		}

		hr = pFolder->EnumObjects(NULL, SHCONTF_NONFOLDERS, &pEnumIds); //lấy file

		while ((hr = pEnumIds->Next(1, &pidlItems, NULL)) == S_OK)
		{
			SHGetFileInfo((LPCTSTR)pidlItems, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);
			wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];

			pFolder->GetDisplayNameOf(pidlItems, SHGDN_FORPARSING, &str);
			StrCpy(buffer, str.pOleStr);

			int  iItem = lw->InsertItem(sfi.szDisplayName, sfi.iIcon, (LPARAM)buffer);

			lw->SetItemText(sfi.szTypeName, lw->GetNumberOfItem() - 1, 2);

			SHGetDataFromIDList(pFolder, pidlItems, SHGDFIL_FINDDATA, (void*)&fd, sizeof(fd));
			wchar_t infoBuffer[30];
			GetFileSize(infoBuffer, fd);
			lw->SetItemText(infoBuffer, iItem, 1);
			GetLastWriteTime(infoBuffer, fd);
			lw->SetItemText(infoBuffer, iItem, 3);
			count++;
			bufferAddress.push_back(buffer);

		}
	}

	pFolder->Release();
	pDeskTop->Release();
	return count;
	
	
}

BOOL Drive::TryOpenFile(wchar_t * path)
{
	if (!IsExistPath(path) || IsFolderPath(path)) return FALSE;
	SHELLEXECUTEINFO ShExecInfo = { 0 };
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
	ShExecInfo.lpFile = path;
	ShExecInfo.lpParameters = _T("");
	ShExecInfo.lpDirectory = NULL;
	ShExecInfo.nShow = SW_SHOW;
	ShExecInfo.hInstApp = NULL;
	ShellExecuteEx(&ShExecInfo);
	//WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
	return TRUE;
}

void Drive::LoadExpanding(TreeView* tw, TVITEM parent)
{
	tw->DeleteAllChild(parent.hItem);
	if (parent.hItem == TreeView_GetRoot(tw->getHandle())) {
		LoadDrive(tw);
		return;
	}




	wchar_t path[MAX_BUFFER_SIZE];

	StrCpy(path, (PWSTR)parent.lParam);
	
	LPITEMIDLIST pidl;
	pidl = ILCreateFromPath(path);
	HRESULT hr = S_OK;
	IShellFolder* pFolder = NULL;
	IEnumIDList* pEnumIds = NULL;
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void **)&pFolder);
	if (SUCCEEDED(hr))
	{
		LPITEMIDLIST pidlItems = NULL;
		SHFILEINFO sfi;
		STRRET str;
		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds); //lấy folder
		while ((hr = pEnumIds->Next(1, &pidlItems, NULL)) == S_OK)
		{
			SHGetFileInfo((LPCTSTR)pidlItems, -1, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_DISPLAYNAME | SHGFI_TYPENAME | SHGFI_ICON);

			wchar_t* buffer = new wchar_t[MAX_BUFFER_SIZE];
			GetFileExtension(buffer, sfi.szDisplayName);
			if (StrCmp(buffer,L"ZIP") == 0 ) {
				delete[] buffer;
				continue;
			}

			pFolder->GetDisplayNameOf(pidlItems, SHGDN_FORPARSING, &str);
			StrCpy(buffer, str.pOleStr);
		
			HTREEITEM hti = tw->InsertNode(sfi.szDisplayName, parent.hItem, sfi.iIcon, sfi.iIcon, (LPARAM)buffer, TVI_SORT);
			if (HasSubDirectory(buffer))
				tw->InsertDummy(hti);
			bufferAddress.push_back(buffer);

		}

	}

	pFolder->Release();

}

BOOL Drive::isEmptyDir(wchar_t * path)
{
	return PathIsDirectoryEmpty(path);
}

BOOL Drive::HasSubDirectory(wchar_t * path)
{
	LPITEMIDLIST pidl;
	pidl = ILCreateFromPath(path);
	HRESULT hr = S_OK;
	IShellFolder* pFolder = NULL;
	IEnumIDList* pEnumIds = NULL;
	hr = SHBindToObject(NULL, pidl, NULL, IID_IShellFolder, (void **)&pFolder);
	int count = 0;
	if (SUCCEEDED(hr))
	{
		LPITEMIDLIST pidlItems = NULL;

		hr = pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumIds); //lấy folder
		if ((hr = pEnumIds->Next(1, &pidlItems, NULL)) == S_OK)
		{
			pFolder->Release();
			return TRUE;
		}

	}
	pFolder->Release();
	return FALSE;
}

BOOL Drive::IsExistPath(wchar_t * path)
{
	return PathFileExists(path);
}

BOOL Drive::IsFolderPath(wchar_t * path)
{

	wchar_t buffer[MAX_BUFFER_SIZE];
	GetFileExtension(buffer,path);
	if (StrCmp(buffer, L"ZIP") == 0) {
		return false;
	}
	DWORD attr = GetFileAttributes(path);
	return ((attr & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY);
}

void Drive::ShowFileColumn(ListView* lw)
{
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_WIDTH;
	lvCol.cx = 180;
	ListView_SetColumn(lw->getHandle(), 0, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_FMT;
	lvCol.fmt = LVCFMT_RIGHT;
	lvCol.pszText = L"Kích thước";
	ListView_SetColumn(lw->getHandle(), 1, &lvCol);


	lvCol.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_FMT;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 130;
	lvCol.pszText = L"Loại tập tin";
	ListView_SetColumn(lw->getHandle(), 2, &lvCol);

	lvCol.pszText = L"Ngày chỉnh sửa";
	ListView_SetColumn(lw->getHandle(), 3, &lvCol);
}

UINT64 Drive::GetFileSize(const WIN32_FIND_DATA & file)
{
	_LARGE_INTEGER   size;
	size.HighPart = file.nFileSizeHigh;
	size.LowPart = file.nFileSizeLow;
	return size.QuadPart;
}


void Drive::GetFileSize(wchar_t* buffer, WIN32_FIND_DATA file)
{
	UINT64 size = GetFileSize(file);
	SizeConverter(buffer, size);
}

UINT64 Drive::GetFileSize(wchar_t * path)
{
	if (!IsExistPath(path)) return 0;
	WIN32_FIND_DATA file;
	FindFirstFile(path,&file);
	return GetFileSize(file);
}

void Drive::GetLastWriteTime(wchar_t * buffer, const WIN32_FIND_DATA & file)
{
	SYSTEMTIME stUTC, stLocal;
	FileTimeToSystemTime(&file.ftLastWriteTime, &stUTC);
	SystemTimeToTzSpecificLocalTime(NULL, &stUTC, &stLocal);

	wsprintf(buffer, L"%02d/%02d/%04d %02d:%02d %s",
		stLocal.wDay, stLocal.wMonth, stLocal.wYear,
		(stLocal.wHour > 12) ? (stLocal.wHour / 12) : (stLocal.wHour),
		stLocal.wMinute,
		(stLocal.wHour > 12) ? (L"PM") : (L"AM"));
}

void Drive::GetFileExtension(wchar_t * buffer, wchar_t* filename_)
{
	std::wstring filename(filename_);
	int pos = filename.rfind(L"."); //lấy phần extension
	filename = filename.substr(pos + 1, filename.size() - pos);
	std::transform(filename.begin(), filename.end(), filename.begin(), ::toupper); //chuyển extension thành chữ hoa
	StrCpy(buffer, filename.c_str());

}

void Drive::SBViewNoItem(int no, StatusBar * sb)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	wsprintf(buffer, L" %d%s", no,L" Items");
	sb->SetText(0, buffer);
}

void Drive::SBViewNoSelectedItem(int no, StatusBar * sb)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	wsprintf(buffer, L" %d%s", no, L" Items Selected");
	sb->SetText(1, buffer);
}

void Drive::SBViewFilesSize(UINT64 no, StatusBar * sb)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	SizeConverter(buffer, no);
	sb->SetText(2, buffer);


}

void Drive::FreeMemory()
{
	for (int i = 0; i < bufferAddress.size(); i++){
		delete[] bufferAddress[i];
	}
	delete[] drivesBuffer;

}

void Drive::ShowDriveColumn(ListView* lw)
{
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_TEXT | LVCF_FMT  | LVCF_WIDTH;

	lvCol.fmt = LVCFMT_LEFT ;
	lvCol.cx = 100;
	lvCol.pszText = L"Label";
	ListView_SetColumn(lw->getHandle(), 1, &lvCol);

	lvCol.fmt = LVCFMT_RIGHT ;
	lvCol.cx = 150;
	lvCol.pszText = L"Tổng dung lượng";
	ListView_SetColumn(lw->getHandle(), 2, &lvCol);

	lvCol.cx = 150;
	lvCol.pszText = L"Dung lượng trống";
	ListView_SetColumn(lw->getHandle(), 3, &lvCol);
}



void Drive::SizeConverter(wchar_t* buffer, UINT64 size) {
	wchar_t* DV[] = { L" Bytes",L" KB",L" MB", L" GB" };
	int count = 0;
	double sizeD = size;
	while ((size / 1024) != 0 && count < 4) {
		sizeD /= 1024;
		size /= 1024;
		count++;
	}
	wsprintf(buffer, L"%d.%d", int(sizeD), int((sizeD - int(sizeD)) * 100));//in ra 2 số sau dấu thập phân
	StrCat(buffer, DV[count]);
}

void Drive::_GetDiskTotalSize(wchar_t * buffer, wchar_t * drivename)
{
	ULARGE_INTEGER totalsize;
	SHGetDiskFreeSpaceEx(drivename, NULL,&totalsize, NULL);
	UINT64 size = totalsize.QuadPart;
	SizeConverter(buffer, size);

}

void Drive::_GetDiskFreeSpace(wchar_t * buffer, wchar_t * drivename)
{
	ULARGE_INTEGER freeSpace;
	SHGetDiskFreeSpaceEx(drivename, NULL, NULL, &freeSpace);
	UINT64 size = freeSpace.QuadPart;
	SizeConverter(buffer, size);
}

void Drive::_GetDiskLabel(wchar_t * buffer, wchar_t * drivename)
{
	GetVolumeInformation(drivename, buffer, 30, NULL, NULL, NULL, NULL, NULL);
}




Drive::Drive()
{
}


Drive::~Drive()
{
}
