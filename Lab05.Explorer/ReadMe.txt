﻿- Info:
 	+ id	: 1512026
 	+ name	: Lương Thái Bảo
 	+ email	: thaibao1997@gmail.com
-Các Chức năng đã làm:
	+ Tạo node gốc là Thic PC 
	+ Khi click vào node trên tree view sẽ hiện thị bên list view
	+ Double Click vào list view nếu là thư mục sẽ hiện thị nội dung các thư mục đó nếu là tập tin sẽ chạy tập tin
	+ Hiển thị các cột: Tên, Thời gian chỉnh sửa, Dung lượng và loại tập tin
	+ Các nút back để quay lại thư mục vừa truy cập và nút forward tương tự.
	+ Thanh Address bar để hiển thị đường dẫn hiện tại 
	+ Nút Go (>) cho phép đi đến đường dẫn người dùng nhập trên thanh address bar
	+ Thay đổi kích thước chiều ngang listview, treeview
	+ Lưu lại kích thước cữa sổ trước khi thoát để lần sau chạy lên lại
-Luồng sự kiện chính:
	+ Chạy chương trình lên, hiển thị node This PC trên TreeView bên trái ở trạng thái collapse (thu gọn). Bấm vào sẽ xổ xuống các node con là danh sách ổ đĩa.
	+ Chạy chương trình lên, List view sẽ hiện thị thông tin các ổ đĩa double click vào sẽ hiện ra các thư mục và tập tin bên trong
-Các luồng sự kiện phụ:
	+ Người dùng nhập sai đường dẫn trên thanh address bar và bấm nút go sẽ thông báo lỗi và khôi phục lại địa chỉ trước khi nhập
-Link Repo: https://bitbucket.org/thaibao1997/windev/
-Link Video Demo: https://youtu.be/3HqUs7rkQc4

